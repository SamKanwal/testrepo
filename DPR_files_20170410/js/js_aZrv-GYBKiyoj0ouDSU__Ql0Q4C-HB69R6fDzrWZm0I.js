var order = (function ($) {
	return {
		init:function (options) {

			this._bindings();
			
		},

		_bindings:function(){

			var self = this;

			$('div[id^="ticket_row_info_"]').on({
				'show.bs.collapse': function () {
				  orderArrow($(this).attr('id'));
				},
				'hide.bs.collapse': function () {
				  orderArrow($(this).attr('id'));
				}
			});

			$(document).on('click','.ticket_info_frame .arrow', function() {
				$(this).closest('.collapse').collapse('toggle');
				orderArrow($(this).closest('.collapse').attr('id'));
			});

			$("#sendwishlistModalSuccess .inv").off("click")
											   .on("click", function(){
											   		jQuery("#sendwishlistModalSuccess").modal("hide");
											   })

			// $(".icm-mail").off("click")
			// 			  .on("click", function(){
			// 			  		self._sendMail($(this));
			// 			  });
			//
			function orderArrow(collapseId) {
				$('[data-target="#' + collapseId + '"]').closest(".user-tickets")
														.find('.price-info')
														.toggleClass('collapse-open');
			}
		},

		_sendMail: function(el){
			// // send the tickets via mail
			// var id, html, url, mailMsg;
			//
			// id = el.closest(".row_ticket_container").data("target");
			// html = $(id).html();
			// //chiamata al servizio
			// url = "";
			// jsFunctions.toggleSpinner();
       //      $.ajax({
       //          type: 'POST',
       //          url: url,
       //          data: {
       //              'html': name
       //          },
       //          success: function() {
       //              mailMsg = Drupal.t('Your request has been sent successfully!');
       //          },
       //          error: function() {
       //              mailMsg = Drupal.t('Sorry, something went wrong. Please try again.');
       //          },
       //          complete: function(modalObject) {
       //          	jsFunctions.toggleSpinner();
       //              var $modal = $("#sendwishlistModalSuccess");
       //              $modal.children('h5').html(mailMsg);
       //              $modal.modal('show');
       //          }
       //      });
		}
	}
	

})(jQuery);
