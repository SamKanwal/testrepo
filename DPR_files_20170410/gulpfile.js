// include gulp
var gulp = require('gulp');

// include plug-ins
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var autoprefix = require('gulp-autoprefixer');
var minifyCSS = require('gulp-minify-css');
var concat = require('gulp-concat');
var stripDebug = require('gulp-strip-debug');
var uglify = require('gulp-uglify');
var combineMq = require('gulp-combine-mq');
var imagemin = require('gulp-imagemin');
// Ottimizzazione immagini
var imageop = require('gulp-image-optimization');
var print = require('gulp-print');
var count = require('gulp-count');

'use strict';

gulp.task('images', function(cb) {
    gulp.src(['../../../default/files/images/**/*.png','../../../default/files/**/*.jpg','../../../default/files/**/*.gif','./images/**/*.jpeg']).pipe(imageop({
        optimizationLevel: 10,
        progressive: true,
        interlaced: true
    })).pipe(gulp.dest('../../../default/files/images-opt')).on('end', cb).on('error', cb);
});




gulp.task('sass', function () {
    gulp.src('./scss/**/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefix('last 10 versions'))
        .pipe(sourcemaps.write('./maps'))
        .pipe(gulp.dest('./css'))
});


gulp.task('combineMq', function () {
    return gulp.src('./css/dprs.css')
        .pipe(combineMq({
            beautify: false
        }))
        .pipe(minifyCSS())
        .pipe(gulp.dest('./css/'));
});


gulp.task('minifyCSS', function() {
    return gulp.src('./css/dprs.css')
        .pipe(combineMq({
            beautify: false
        }))
        .pipe(minifyCSS())
        .pipe(gulp.dest('./css/prod/'))
});

gulp.task('sass:watch', function () {
    gulp.watch('./scss/**/*.scss', ['sass']);
});

gulp.task('sass:watch:minify', function () {
    gulp.watch('./css/dprs.css', ['minifyCSS']);
});

// JS concat, strip debugging and minify
gulp.task('ClassesScript', function() {
    gulp.src(['./js/js_classes/*'])
        .pipe(concat('classes.js'))
        .pipe(gulp.dest('./js/'));
});

gulp.task('compressJs', function() {
    return gulp.src('./js/**/*.js')
    // .pipe(concat('main.min.js'))
        .pipe(uglify({
            mangle:false
        }))
        .pipe(gulp.dest('./js/prod/'));
});

gulp.task('createProdJS', function () {
    var paths = [
        //This list is an extract of sites/all/themes/dprs_common/dprs_common.info js inclusions
        'js/vendor/dictionary.js',
        'js/vendor/TweenMax.min.js',
        'js/vendor/ScrollToPlugin.min.js',
        'js/vendor/slick.min.js',
        'js/vendor/glide.min.js',
        'js/vendor/telPrefix.js',
        'js/vendor/select2.full.js',
        'js/vendor/jScrollPane.js',
        'js/vendor/handlebars.js',
        'js/vendor/countryRegionList.js',
        'js/vendor/countryRegionListAR.js',
        'js/vendor/crs.js',
        'js/vendor/jquery.mousewheel.min.js',
        'js/vendor/jquery.ui.touch-punch.min.js',
        'js/vendor/moment-with-locales.js',
        'js/vendor/jquery.mask.min.js',
        'js/vendor/bootstrap-select.min.js',
        'js/vendor/bootstrap-datepicker.js',
        'js/vendor/jquery.ui.slider-rtl.min.js',
        'js/vendor/underscore-min.js',
        'js/vendor/jquery.payment.min.js',
        'js/vendor/ScrollMagic.js',
        'js/vendor/plugins/animation.gsap.js',
        'js/vendor/inputmask.js',
        'js/vendor/inputmask.regex.extensions.js',
        'js/vendor/jquery.inputmask.js',
        'js/vendor/jquery.visible.min.js',
        'js/prod/main.js',
        'js/prod/yt.js',
        'js/prod/classes.js',
        '../../modules/dpr/dpr_seat_selection/src/scripts/prod/cookie-modal.js'
    ];
    console.log('Following files and dirs will be included (Total %s): ', paths.length);
    return gulp.src(paths, {base: './js'})
        .pipe(count('## assets selected'))
        .pipe(print())
        .pipe(concat('dpr-theme.min.js'))
        .pipe(uglify({
            mangle:false
        }))
        .pipe(gulp.dest('./js/minified/'));

});

gulp.task('createProdCSS', function () {
    var paths = [
        //This list is an extract of sites/all/themes/dprs_common/dprs_common.info js inclusions
        'bootstrap/dist/css/bootstrap-theme.css',
        'bootstrap/dist/css/bootstrap.css',
        'css/jquery-ui.css',
        'css/font-face.css',
        'css/iconset.css',
        'css/chosen-bootstrap.css',
        'css/style.css',
        'css/page.css',
        'css/block.css',
        'css/views.css',
        'css/gmap.css',
        'css/loader_dpr.css',
        'css/responsive.css',
        'css/rtl.css',
        'css/glide.core.min.css',
        'css/glide.theme.min.css',
        'css/jquery.jscrollpane.css',
        'css/select2.css',
        'css/bootstrap-select.css',
        'css/jquery.ui.slider-rtl.css',
        'css/dprs.css',
        '../../modules/dpr/dpr_seat_selection/src/css/counter.css'
    ];
    console.log('Following files and dirs will be included (Total %s): ', paths.length);
    return gulp.src(paths, {base: './css'})
        .pipe(count('## assets selected'))
        .pipe(print())
        .pipe(concat('dpr-theme.min.css'))
        .pipe(minifyCSS())
        .pipe(gulp.dest('./css/'));

});

gulp.task('createProdModuleJS', function () {
    var _module = '../../modules/dpr/';

    var moduleName = 'dpr_booking_flow'; //CHANGE THIS AND RUN THE SCRIPT TO PROCESS DIFFERENT MODULES
    var scriptsPath = '/src/scripts';   //SPECIFY A PROPER SCRIPT PATH ALSO.

    var path = _module + moduleName + scriptsPath;

    console.log('Following files and dirs will be included: ');
    return gulp.src(path + '/*.js', {base: path})
        .pipe(count('## assets selected'))
        .pipe(print())
        .pipe(uglify({
            mangle:false
        }))
        .pipe(gulp.dest(path + '/prod'));

});

gulp.task('scripts:watch', function () {
    gulp.watch('./js/js_classes/*js', ['ClassesScript']);
});

gulp.task('scripts:watch:compress', function () {
    gulp.watch('./js/main.js', ['compressJs']);
});


// JS concat, strip debugging and minify
gulp.task('processJs', function() {
    gulp.src(['./js/js_classes/*'])
        .pipe(concat('classes.js'))
        .pipe(gulp.dest('./js/'));
});

gulp.task('processJs', function() {
    return gulp.src('./js/**/*.js')
        .pipe(concat('main.min.js'))
        .pipe(uglify({
            mangle:false
        }))
        .pipe(gulp.dest('./js/min/'));
});

gulp.task('default', [
        'sass',
        'minifyCSS',
        'ClassesScript',
        'compressJs',
        'sass:watch',
        'scripts:watch',
        'scripts:watch:compress',
        'sass:watch:minify'
    ]
);
// da decommentare per attivare estensione chrome di gulp nell'inspector
// module.exports = gulp;