<?php
/**
 * ticket summary flow
 */
global $base_url, $language, $base_path;

$content  = array_values(node_load_by_type("addons", 1, 0))[0];

$modaltitle = isset($content -> field_seat_sel_modal_title) ? $content -> field_seat_sel_modal_title['und'][0]['value'] : '';
$modaltext = isset($content -> field_seat_sel_modal_subtitle) ? $content -> field_seat_sel_modal_subtitle['und'][0]['value'] : '';
$modaltitleBlue = isset($content -> field_modal_subtitle_blue) ? $content -> field_modal_subtitle_blue['und'][0]['value'] : '';
$buttonCta = isset($content -> field_button_cta) ? $content -> field_button_cta['und'][0]['value'] : '';
if (isset($content->field_header_image['und'][0])) {
$headerImgUri = str_replace("public://", "", $content->field_header_image['und'][0]['uri']);
$headerImgUri = "/sites/default/files/".$headerImgUri;
} else {
$headerImgUri = "";    
}

$titleStandalone = isset($content -> field_title_standalone) ? $content -> field_title_standalone['und'][0]['value'] : '';

$titleHeader = isset($content -> field_header_title) ? $content -> field_header_title['und'][0]['value'] : '';

$titleNormal = isset($content -> field_title_normal) ? $content -> field_title_normal['und'][0]['value'] : '';

$reqUrl = explode("/", $_SERVER["REQUEST_URI"]);

if($reqUrl[count($reqUrl)-1]=="addon"){
    //stadalone version
?>
<script>
  addOnSearch = {
    type : "STANDALONE"
  };
</script>
<?php }else{ ?>
<script>
  addOnSearch = {
    type : "EXTRA"
  };
</script>
<?php 
print_r($reqUrl);
} ?>

<div class="wrapper-page qextra-wrapper">
    <section class="top-page">
        <!-- polymer component -->
    </section>
    <fieldset class="content-page">
<?php if( $reqUrl[count($reqUrl)-1] !="addon"):?>
        <div class="booking-flow-breadcrumb">
            <div class="booking-flow-breadcrumb-table container">
                <div class="booking-flow-breadcrumb-row">
                    <div class="bf-breadcrumb-item prev-next">
                        <a href="javascript:javascript:history.go(-1)">
                            <div class="bf-breadcrumb-icon bf-breadcrumb-prev"></div>
                        </a>
                    </div>
                    <div class="bf-breadcrumb-item active current">
                        <div class="bf-breadcrumb-icon icon-DPR_Icon1_34CreditCard icm-list" id="bf_billing_icon"></div>
                        <div
                          class="bf-breadcrumb-title"><?php print t('Other Products'); ?></div>
                    </div>
                    <div class="bf-breadcrumb-item">
                        <div class="bf-breadcrumb-icon icon-DPR_Icon1_34CreditCard icm-billing" id="bf_billing_icon"></div>
                        <div
                            class="bf-breadcrumb-title"><?php print t('Billing'); ?></div>
                    </div>
                    <div class="bf-breadcrumb-item">
                        <div class="bf-breadcrumb-icon icon-DPR_Icon1_34CreditCard" id="bf_billing_icon"></div>
                        <div
                            class="bf-breadcrumb-title"><?php print t('Payment'); ?></div>
                    </div>
                    <div class="bf-breadcrumb-item ">
                        <div class="bf-breadcrumb-icon icon-DPR_Icon1_69Confirm" id="bf_confirmation_icon"></div>
                        <div class="bf-breadcrumb-title"><?php print t('Confirmation'); ?></div>
                    </div>
                </div>
            </div>
        </div>
        <?php else: ?>
        <figure class="banner">
          <img src="<?= $headerImgUri; ?>" class="img-responsive">  
          <figcaption><?php print $titleHeader; ?></figcaption>
        </figure>
        <?php endif; ?>
        <!-- CART APOCALYPSE include -->
        <?php module_load_include('php', 'dpr_cart_apocalypse', '/templates/entry-point.tpl'); ?>
         <?php if (in_array('administrator', array_values($user->roles))) print "<div class='col-md-12 wish_header'>".l(t('Edit settings for this section'),'node/'.$content->nid.'/edit' )."</div>";?>
       
        <div class="bf_header">
            <h1><?php if( $reqUrl[count($reqUrl)-1] !="addon") { print $titleStandalone; } else { print $titleNormal; } ?></h1>
        </div>
        <div id="q-extras">
            <div class="container">

                <div class="q-pass" data-role="template" >
                    <div class="block-wrapper">
                        <p class="title">
                            <span class="park">Bollywood 1 Day Dated Standard Q-Fast</span>
                            <br>
                            <!--                        <span class="tag">--><?//= t("Qfast"); ?><!--</span>-->
                        </p>
<!--                        <div class="type">standard</div>-->
                        <p class="description">
                            Cur extum studere? Hibridas persuadere, tanquam bi-color valebat.
                        </p>
                        <div class="price">
                            <span class="currency">AED</span>
                            <span class="total">100</span>
                        </div>
                        <div class="block-footer">
                            <button class="cta choose-btn"><?= $buttonCta  ?></button>
                        </div>
                    </div>

                </div>
                <div class="pass-selection">
                    <div class="qfast-collapser-wrapper">
                        <div class="tail-container">
                            <div class="tail"></div>
                        </div>
                        <form>
                            <input type="hidden" name="selectedQpass" value="test">
                            <div class="row">
                                <div class="is-block col-md-4 col-sm-6 col-xs-12">
                                    <div class="select_guests">
                                        <div class="dropdown select_guest_dropdown">
                                            <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">
                                                <span class="dropdown-title" id="guestsIn"><?php print t('Select Quantity'); ?></span>
                                                <span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>
                                                <span class="glyphicon glyphicon-menu-up" aria-hidden="true"></span>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li class="adult_row">
                                                    <div class="guest-title"><?php print t('Guests'); ?></div>
                                                    <div class="guest-table">
                                                        <div class="guest-cell">
                                                            <a href="#" class="minus" data-guest="adult"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span></a>
                                                        </div>
                                                        <div class="guest-cell number">
                                                            <span class="icm-guest_adult iconic"></span><span>x</span><span class="guest-number">0</span>
                                                        </div>
                                                        <div class="guest-cell">
                                                            <a href="#" class="plus" data-guest="adult"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a>
                                                        </div>
                                                    </div>
<!--                                                    <div class="guest-price text-center">-->
<!--                                                        <span class="cur_price">--><?php //print $_SESSION['rdps']['currency']; ?><!--</span> <span>500</span>-->
<!--                                                    </div>-->
                                                </li>
                                                <li class="dropdown-ok">
                                                    <a href="#" data-toggle="dropdown"><div><?php print t('ok'); ?></div></a>
                                                </li>
                                            </ul>
                                        </div>
                                        <input type="hidden" name="adults" class="guest_number_input" value="0"/>
                                        <input type="hidden" name="children" class="guest_number_input" value="0"/>
                                        <input type="hidden" name="seniors" class="guest_number_input" value="0"/>
                                    </div>
                                </div>
                                <div class="is-block col-md-4 col-sm-6 col-xs-12">
                                    <div class="ticket_form">
                                        <input type="text" class="datepicker" name="date_booking" placeholder="<?php print t('Select Date'); ?>" readonly="true">
                                        <input type="hidden" name="dateStartTicket" id="date_start_ticket" class="date-start-ticket">
                                    </div>
                                </div>
                                <div class="is-block col-md-4 col-sm-12 col-xs-12">
                                    <button class="cta add-to-cart disabled"><?= t("Add to cart"); ?></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </fieldset>
</div>

<!-- Modal for messages -->


<div id="no-addons" class="modal fade" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <i class="icm-negative_feedback iconic"></i>
                <p class="title"><?= $modaltitle ?></p>
<!--                <p class="subtitle">--><?//= $modaltext ?><!--</p>-->
                <p class="subtitle"><?= $modaltext ?></p>
                <div class="body"><?= $modaltitleBlue ?><span class="reason"></span></div>
            </div>
        </div>
    </div>
</div>