<?php

/**
 * @file
 * template.php
 */

require_once dirname(__FILE__) . '/preprocess/page.preprocess.inc';
require_once dirname(__FILE__) . '/preprocess/node.preprocess.inc';
require_once dirname(__FILE__) . '/preprocess/menu_tree.preprocess.inc';
require_once dirname(__FILE__) . '/preprocess/field.preprocess.inc';
require_once dirname(__FILE__) . '/preprocess/form.preprocess.inc';

drupal_add_library('system', 'ui');
/**
 * Implements template_preprocess_entity().
 */
function dprs_common_preprocess_entity(&$variables, $hook) {
    $function = 'dprs_common_preprocess_' . $variables['entity_type'];
    if (function_exists($function)) {
        $function($variables, $hook);
    }
}

/**
 * Implements template_links
 */
function dprs_common_links__locale_block(&$variables) {

    global $language;

    $output = '<div class="dropdown">
            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownLang" data-toggle="dropdown" >
            <span class="lang-title">' . $language->native . '</span>
            <span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>
            </button>';

    $dd_class = 'dropdown-menu';

    if ($language->direction == '1') {
        $dd_class = 'dropdown-menu dropdown-menu-right';
    }

    $output .= '<ul class="' . $dd_class . '" aria-labelledby="dropdownLang">';

    // an array of list items
    foreach($variables['links'] as $lang => $info) {

        if ($lang != $language->language) {

            $name = $info['language']->native;
            $href = isset($info['href']) ? $info['href'] : '';

            $link_classes = array('lang-link');
            $options = array('attributes' => array(
                'class'    => $link_classes),
                'language' => $info['language'],
                'html'     => true
            );

            $link = l($name, $href, $options);

            $output .= '<li>' . $link . '</li>';
        }
    }

    $output .= '</ul>
              </div>';

    return $output;
}

function dprs_common_visual_sitemap(&$variables) {
    global $language;
    $sitemap = xmlsitemap_sitemap_load_by_context(array(
        'language' => $language->language,
    ));
    $file = drupal_realpath(xmlsitemap_sitemap_get_file($sitemap, 1));
    $xml = simplexml_load_file($file);
    $node_types = node_type_get_types();
    $links = array();
    foreach ($xml->url as $url) {
        $path = str_replace($sitemap->uri['options']['base_url'], '', $url->loc);
        $path = urldecode($path);
        $path = ltrim($path, '/');
        $path = drupal_get_normal_path($path);
        $path = substr($path,3);
        $path = drupal_lookup_path("source", $path);
        $node = menu_get_object('node', 1, $path);
        if ($node) {
            $node_type = $node->type;
            $section = $node_types[$node_type]->name;
            if (!isset($links[$section])) {
                $links[$section] = array();
            }
            $links[$section][] = $node;
        }
    }

    $variables['sitemap'] = $links;
}

function node_load_by_type($type, $limit = 15, $offset = 0) {
    global $language;
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
        ->entityCondition('bundle', $type)
        ->propertyCondition('language', $language->language, '=')
        ->propertyCondition('status', NODE_PUBLISHED)
        ->propertyOrderBy('created', 'DESC')
        ->range($offset, $limit);
    $results = $query->execute();
    return node_load_multiple(array_keys($results['node']));
}

function encodeCookie($data){

    $cookie = json_encode($data);

    $dictionary = [
        "ANNPASS" => "A",
        "1DAY" => "Q",
        "2DAY" => "E",
        "3DAY" => "R",
        "4DAY" => "B",
        "ndt" => "g",
        "total" => "t",
        "adult" => "a",
        "child" => "c",
        "senior" => "s",
        "number" => "n",
        "price" => "p",
        "code" => "y",
        "ticketName" => "x",
        "ticketEventName" => "w",
        "datedTicket" => "z",
        "date1" => "d",
        "raw_date1" => "l",
        "ticketPrice" => "o",
        "currency" => "h",
        "ticketEntitlement" => "e",
        "startingPriceCurrency" => "u",
        "_mode_flag" => "f",
        "PriceMode" => "j",
        "ticketReference" => "k",
        "Currency" => "v",
        "startingPrice" => "b",
        "Prices" => "P",
        "Amount" => "m",
        "type" => "G",
        "qty" => "T",
        "cart" => "Y",
        "performanceId" => "U",
        "productId" => "I",
        "bookingUrl" => "O",
        "name" => "S",
        "walletDeposit" => "D",
        "mediaCode" => "F",
        "shopCartId" => "H",
        "discount" => "K",
        "portfolioId" => "L",
        "seatCoord" => "W",
        "catName" => "X"
    ];

    foreach ($dictionary as $k => $v){
        $cookie = str_replace('"'.$k.'"', '"'.$v.'"', $cookie);
    }

    return str_replace(' ', '+', $cookie);
}

function decodeCookie($id){

    $cookie = isset($_COOKIE[$id]) ? $_COOKIE[$id] : '{}';

    $dictionary = [
        "A"=> 'ANNPASS',
        "Q"=> '1DAY',
        "E"=> '2DAY',
        "R"=> '3DAY',
        "B"=> '4DAY',
        "g"=> 'ndt',
        "t"=> "total",
        "a"=> "adult",
        "c"=> "child",
        "s"=> "senior",
        "n"=> "number",
        "p"=> "price",
        "y"=> "code",
        "x"=> "ticketName",
        "w"=> "ticketEventName",
        "z"=> "datedTicket",
        "d"=> "date1",
        "l"=> "raw_date1",
        "o"=> "ticketPrice",
        "h"=> "currency",
        "e" => "ticketEntitlement",
        "u" => "startingPriceCurrency",
        "f" => "_mode_flag",
        "j" => "PriceMode",
        "k" => "ticketReference",
        "v" => "Currency",
        "b" => "startingPrice",
        "P" => "Prices",
        "m" => "Amount",
        "G" => "type",
        "T" => "qty",
        "Y" => "cart",
        "U" => "performanceId",
        "I" => "productId",
        "O" => "bookingUrl",
        "S" => "name",
        "D" => "walletDeposit",
        "F" => "mediaCode",
        "H" => "shopCartId",
        "K" => "discount",
        "L" => "portfolioId",
        "X" => "catName",
        "W" => "seatCoord"


    ];

    foreach ($dictionary as $k => $v){
        $cookie = str_replace('"'.$k.'"', '"'.$v.'"', $cookie);
    }
    $cookie = str_replace('+', ' ', $cookie);

    return json_decode($cookie);
}

function formatPrice($price){
    $price = intval($price);
    $ret = strval($price);

    if($price > 999){
        $ret = substr_replace($ret, ",", 1, 0);
    }

    return $ret;
}

function checkCurrencyConsistency(){
    global $language;
    
    if($language -> prefix == 'en' && isset($_SESSION['rdps']) && isNotAscii($_SESSION['rdps']['currency'])){
        $_SESSION['rdps']['currency'] = 'AED';
    }
}

function isNotAscii($str){
    return !preg_match('/^[\x00-\x7F]*$/', $str);
}

$GLOBALS['is_mobile'] = false;
$useragent=$_SERVER['HTTP_USER_AGENT'];
if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od|ad)|iris|kindle|lge |android|maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|android|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))){
    $GLOBALS['is_mobile'] = true;
}

// acapiacecess_getProducts_annualPass('DPR', '', false);
checkCurrencyConsistency();
