/**
* @file
* Javascript, modifications of DOM.
*
* Manipulates links to include jquery load funciton
*/

(function ($) {
  Drupal.behaviors.jquery_ajax_load = {
    attach: function (context, settings) {
      jQuery.ajaxSetup ({
      // Disable caching of AJAX responses
        cache: false
      });

      var trigger = Drupal.settings.jquery_ajax_load.trigger;
      var target = Drupal.settings.jquery_ajax_load.target;
      // Puede ser más de un valor, hay que usar foreach()
      $(trigger).once(function() {
        var html_string = $(this).attr( 'href' );
        // Hay que validar si la ruta trae la URL del sitio
        $(this).attr( 'href' , target );
        var data_target = $(this).attr( 'data-target' );
        if (typeof data_target === 'undefined' ) {
          data_target = target;
        }
        else {
          data_target = '#' + data_target;
        }
        $(this).click(function(evt) {
          evt.preventDefault();
          jquery_ajax_load_load($(this), data_target, html_string);
        });
      });
      $(trigger).removeClass(trigger);
    }
  };  

// Handles link calls
  function jquery_ajax_load_load(el, target, url) {
    var module_path = Drupal.settings.jquery_ajax_load.module_path;
    var toggle = Drupal.settings.jquery_ajax_load.toggle;
    var base_path = Drupal.settings.jquery_ajax_load.base_path;
    var animation = Drupal.settings.jquery_ajax_load.animation;
    if( toggle && $(el).hasClass( "jquery_ajax_load_open" ) ) {
      $(el).removeClass( "jquery_ajax_load_open" );
      if ( animation ) {
        $(target).hide('slow', function() {
          $(target).empty();
        });
      }
      else {
        $(target).empty();
      }
    }
    else {
      var loading_html = Drupal.t('Loading'); 
      loading_html += '... <img src="/';
      loading_html += module_path;
      loading_html += '/jquery_ajax_load_loading.gif">';
      $(target).html(loading_html);
      $(target).load(base_path + 'jquery_ajax_load/get' + url, function( response, status, xhr ) {
        if ( status == "error" ) {
          var msg = "Sorry but there was an error: ";
          $(target).html( msg + xhr.status + " " + xhr.statusText );
        }
        else {
          if ( animation ) {
            $(target).hide();
            $(target).show('slow')
          }
//        Drupal.attachBehaviors(target);
        }
      });
      $(el).addClass( "jquery_ajax_load_open" );
    }
  }
}(jQuery));
;/**/
/*!
 * jQuery Validation Plugin v1.14.0
 *
 * http://jqueryvalidation.org/
 *
 * Copyright (c) 2015 Jörn Zaefferer
 * Released under the MIT license
 */
(function( factory ) {
	if ( typeof define === "function" && define.amd ) {
		define( ["jquery"], factory );
	} else {
		factory( jQuery );
	}
}(function( $ ) {

$.extend($.fn, {
	// http://jqueryvalidation.org/validate/
	validate: function( options ) {

		// if nothing is selected, return nothing; can't chain anyway
		if ( !this.length ) {
			if ( options && options.debug && window.console ) {
				console.warn( "Nothing selected, can't validate, returning nothing." );
			}
			return;
		}

		// check if a validator for this form was already created
		var validator = $.data( this[ 0 ], "validator" );
		if ( validator ) {
			return validator;
		}

		// Add novalidate tag if HTML5.
		this.attr( "novalidate", "novalidate" );

		validator = new $.validator( options, this[ 0 ] );
		$.data( this[ 0 ], "validator", validator );

		if ( validator.settings.onsubmit ) {

			this.on( "click.validate", ":submit", function( event ) {
				if ( validator.settings.submitHandler ) {
					validator.submitButton = event.target;
				}

				// allow suppressing validation by adding a cancel class to the submit button
				if ( $( this ).hasClass( "cancel" ) ) {
					validator.cancelSubmit = true;
				}

				// allow suppressing validation by adding the html5 formnovalidate attribute to the submit button
				if ( $( this ).attr( "formnovalidate" ) !== undefined ) {
					validator.cancelSubmit = true;
				}
			});

			// validate the form on submit
			this.on( "submit.validate", function( event ) {
				if ( validator.settings.debug ) {
					// prevent form submit to be able to see console output
					event.preventDefault();
				}
				function handle() {
					var hidden, result;
					if ( validator.settings.submitHandler ) {
						if ( validator.submitButton ) {
							// insert a hidden input as a replacement for the missing submit button
							hidden = $( "<input type='hidden'/>" )
								.attr( "name", validator.submitButton.name )
								.val( $( validator.submitButton ).val() )
								.appendTo( validator.currentForm );
						}
						result = validator.settings.submitHandler.call( validator, validator.currentForm, event );
						if ( validator.submitButton ) {
							// and clean up afterwards; thanks to no-block-scope, hidden can be referenced
							hidden.remove();
						}
						if ( result !== undefined ) {
							return result;
						}
						return false;
					}
					return true;
				}

				// prevent submit for invalid forms or custom submit handlers
				if ( validator.cancelSubmit ) {
					validator.cancelSubmit = false;
					return handle();
				}
				if ( validator.form() ) {
					if ( validator.pendingRequest ) {
						validator.formSubmitted = true;
						return false;
					}
					return handle();
				} else {
					validator.focusInvalid();
					return false;
				}
			});
		}

		return validator;
	},
	// http://jqueryvalidation.org/valid/
	valid: function() {
		var valid, validator, errorList;

		if ( $( this[ 0 ] ).is( "form" ) ) {
			valid = this.validate().form();
		} else {
			errorList = [];
			valid = true;
			validator = $( this[ 0 ].form ).validate();
			this.each( function() {
				valid = validator.element( this ) && valid;
				errorList = errorList.concat( validator.errorList );
			});
			validator.errorList = errorList;
		}
		return valid;
	},

	// http://jqueryvalidation.org/rules/
	rules: function( command, argument ) {
		var element = this[ 0 ],
			settings, staticRules, existingRules, data, param, filtered;

		if ( command ) {
			settings = $.data( element.form, "validator" ).settings;
			staticRules = settings.rules;
			existingRules = $.validator.staticRules( element );
			switch ( command ) {
			case "add":
				$.extend( existingRules, $.validator.normalizeRule( argument ) );
				// remove messages from rules, but allow them to be set separately
				delete existingRules.messages;
				staticRules[ element.name ] = existingRules;
				if ( argument.messages ) {
					settings.messages[ element.name ] = $.extend( settings.messages[ element.name ], argument.messages );
				}
				break;
			case "remove":
				if ( !argument ) {
					delete staticRules[ element.name ];
					return existingRules;
				}
				filtered = {};
				$.each( argument.split( /\s/ ), function( index, method ) {
					filtered[ method ] = existingRules[ method ];
					delete existingRules[ method ];
					if ( method === "required" ) {
						$( element ).removeAttr( "aria-required" );
					}
				});
				return filtered;
			}
		}

		data = $.validator.normalizeRules(
		$.extend(
			{},
			$.validator.classRules( element ),
			$.validator.attributeRules( element ),
			$.validator.dataRules( element ),
			$.validator.staticRules( element )
		), element );

		// make sure required is at front
		if ( data.required ) {
			param = data.required;
			delete data.required;
			data = $.extend( { required: param }, data );
			$( element ).attr( "aria-required", "true" );
		}

		// make sure remote is at back
		if ( data.remote ) {
			param = data.remote;
			delete data.remote;
			data = $.extend( data, { remote: param });
		}

		return data;
	}
});

// Custom selectors
$.extend( $.expr[ ":" ], {
	// http://jqueryvalidation.org/blank-selector/
	blank: function( a ) {
		return !$.trim( "" + $( a ).val() );
	},
	// http://jqueryvalidation.org/filled-selector/
	filled: function( a ) {
		return !!$.trim( "" + $( a ).val() );
	},
	// http://jqueryvalidation.org/unchecked-selector/
	unchecked: function( a ) {
		return !$( a ).prop( "checked" );
	}
});

// constructor for validator
$.validator = function( options, form ) {
	this.settings = $.extend( true, {}, $.validator.defaults, options );
	this.currentForm = form;
	this.init();
};

// http://jqueryvalidation.org/jQuery.validator.format/
$.validator.format = function( source, params ) {
	if ( arguments.length === 1 ) {
		return function() {
			var args = $.makeArray( arguments );
			args.unshift( source );
			return $.validator.format.apply( this, args );
		};
	}
	if ( arguments.length > 2 && params.constructor !== Array  ) {
		params = $.makeArray( arguments ).slice( 1 );
	}
	if ( params.constructor !== Array ) {
		params = [ params ];
	}
	$.each( params, function( i, n ) {
		source = source.replace( new RegExp( "\\{" + i + "\\}", "g" ), function() {
			return n;
		});
	});
	return source;
};

$.extend( $.validator, {

	defaults: {
		messages: {},
		groups: {},
		rules: {},
		errorClass: "error",
		validClass: "valid",
		errorElement: "label",
		focusCleanup: false,
		focusInvalid: true,
		errorContainer: $( [] ),
		errorLabelContainer: $( [] ),
		onsubmit: true,
		ignore: ":hidden",
		ignoreTitle: false,
		onfocusin: function( element ) {
			this.lastActive = element;

			// Hide error label and remove error class on focus if enabled
			if ( this.settings.focusCleanup ) {
				if ( this.settings.unhighlight ) {
					this.settings.unhighlight.call( this, element, this.settings.errorClass, this.settings.validClass );
				}
				this.hideThese( this.errorsFor( element ) );
			}
		},
		onfocusout: function( element ) {
			if ( !this.checkable( element ) && ( element.name in this.submitted || !this.optional( element ) ) ) {
				this.element( element );
			}
		},
		onkeyup: function( element, event ) {
			// Avoid revalidate the field when pressing one of the following keys
			// Shift       => 16
			// Ctrl        => 17
			// Alt         => 18
			// Caps lock   => 20
			// End         => 35
			// Home        => 36
			// Left arrow  => 37
			// Up arrow    => 38
			// Right arrow => 39
			// Down arrow  => 40
			// Insert      => 45
			// Num lock    => 144
			// AltGr key   => 225
			var excludedKeys = [
				16, 17, 18, 20, 35, 36, 37,
				38, 39, 40, 45, 144, 225
			];

			if ( event.which === 9 && this.elementValue( element ) === "" || $.inArray( event.keyCode, excludedKeys ) !== -1 ) {
				return;
			} else if ( element.name in this.submitted || element === this.lastElement ) {
				this.element( element );
			}
		},
		onclick: function( element ) {
			// click on selects, radiobuttons and checkboxes
			if ( element.name in this.submitted ) {
				this.element( element );

			// or option elements, check parent select in that case
			} else if ( element.parentNode.name in this.submitted ) {
				this.element( element.parentNode );
			}
		},
		highlight: function( element, errorClass, validClass ) {
			if ( element.type === "radio" ) {
				this.findByName( element.name ).addClass( errorClass ).removeClass( validClass );
			} else {
				$( element ).addClass( errorClass ).removeClass( validClass );
			}
		},
		unhighlight: function( element, errorClass, validClass ) {
			if ( element.type === "radio" ) {
				this.findByName( element.name ).removeClass( errorClass ).addClass( validClass );
			} else {
				$( element ).removeClass( errorClass ).addClass( validClass );
			}
		}
	},

	// http://jqueryvalidation.org/jQuery.validator.setDefaults/
	setDefaults: function( settings ) {
		$.extend( $.validator.defaults, settings );
	},

	messages: {
		required: "This field is required.",
		remote: "Please fix this field.",
		email: "Please enter a valid email address.",
		url: "Please enter a valid URL.",
		date: "Please enter a valid date.",
		dateISO: "Please enter a valid date ( ISO ).",
		number: "Please enter a valid number.",
		digits: "Please enter only digits.",
		creditcard: "Please enter a valid credit card number.",
		equalTo: "Please enter the same value again.",
		maxlength: $.validator.format( "Please enter no more than {0} characters." ),
		minlength: $.validator.format( "Please enter at least {0} characters." ),
		rangelength: $.validator.format( "Please enter a value between {0} and {1} characters long." ),
		range: $.validator.format( "Please enter a value between {0} and {1}." ),
		max: $.validator.format( "Please enter a value less than or equal to {0}." ),
		min: $.validator.format( "Please enter a value greater than or equal to {0}." )
	},

	autoCreateRanges: false,

	prototype: {

		init: function() {
			this.labelContainer = $( this.settings.errorLabelContainer );
			this.errorContext = this.labelContainer.length && this.labelContainer || $( this.currentForm );
			this.containers = $( this.settings.errorContainer ).add( this.settings.errorLabelContainer );
			this.submitted = {};
			this.valueCache = {};
			this.pendingRequest = 0;
			this.pending = {};
			this.invalid = {};
			this.reset();

			var groups = ( this.groups = {} ),
				rules;
			$.each( this.settings.groups, function( key, value ) {
				if ( typeof value === "string" ) {
					value = value.split( /\s/ );
				}
				$.each( value, function( index, name ) {
					groups[ name ] = key;
				});
			});
			rules = this.settings.rules;
			$.each( rules, function( key, value ) {
				rules[ key ] = $.validator.normalizeRule( value );
			});

			function delegate( event ) {
				var validator = $.data( this.form, "validator" ),
					eventType = "on" + event.type.replace( /^validate/, "" ),
					settings = validator.settings;
				if ( settings[ eventType ] && !$( this ).is( settings.ignore ) ) {
					settings[ eventType ].call( validator, this, event );
				}
			}

			$( this.currentForm )
				.on( "focusin.validate focusout.validate keyup.validate",
					":text, [type='password'], [type='file'], select, textarea, [type='number'], [type='search'], " +
					"[type='tel'], [type='url'], [type='email'], [type='datetime'], [type='date'], [type='month'], " +
					"[type='week'], [type='time'], [type='datetime-local'], [type='range'], [type='color'], " +
					"[type='radio'], [type='checkbox']", delegate)
				// Support: Chrome, oldIE
				// "select" is provided as event.target when clicking a option
				.on("click.validate", "select, option, [type='radio'], [type='checkbox']", delegate);

			if ( this.settings.invalidHandler ) {
				$( this.currentForm ).on( "invalid-form.validate", this.settings.invalidHandler );
			}

			// Add aria-required to any Static/Data/Class required fields before first validation
			// Screen readers require this attribute to be present before the initial submission http://www.w3.org/TR/WCAG-TECHS/ARIA2.html
			$( this.currentForm ).find( "[required], [data-rule-required], .required" ).attr( "aria-required", "true" );
		},

		// http://jqueryvalidation.org/Validator.form/
		form: function() {
			this.checkForm();
			$.extend( this.submitted, this.errorMap );
			this.invalid = $.extend({}, this.errorMap );
			if ( !this.valid() ) {
				$( this.currentForm ).triggerHandler( "invalid-form", [ this ]);
			}
			this.showErrors();
			return this.valid();
		},

		checkForm: function() {
			this.prepareForm();
			for ( var i = 0, elements = ( this.currentElements = this.elements() ); elements[ i ]; i++ ) {
				this.check( elements[ i ] );
			}
			return this.valid();
		},

		// http://jqueryvalidation.org/Validator.element/
		element: function( element ) {
			var cleanElement = this.clean( element ),
				checkElement = this.validationTargetFor( cleanElement ),
				result = true;

			this.lastElement = checkElement;

			if ( checkElement === undefined ) {
				delete this.invalid[ cleanElement.name ];
			} else {
				this.prepareElement( checkElement );
				this.currentElements = $( checkElement );

				result = this.check( checkElement ) !== false;
				if ( result ) {
					delete this.invalid[ checkElement.name ];
				} else {
					this.invalid[ checkElement.name ] = true;
				}
			}
			// Add aria-invalid status for screen readers
			$( element ).attr( "aria-invalid", !result );

			if ( !this.numberOfInvalids() ) {
				// Hide error containers on last error
				this.toHide = this.toHide.add( this.containers );
			}
			this.showErrors();
			return result;
		},

		// http://jqueryvalidation.org/Validator.showErrors/
		showErrors: function( errors ) {
			if ( errors ) {
				// add items to error list and map
				$.extend( this.errorMap, errors );
				this.errorList = [];
				for ( var name in errors ) {
					this.errorList.push({
						message: errors[ name ],
						element: this.findByName( name )[ 0 ]
					});
				}
				// remove items from success list
				this.successList = $.grep( this.successList, function( element ) {
					return !( element.name in errors );
				});
			}
			if ( this.settings.showErrors ) {
				this.settings.showErrors.call( this, this.errorMap, this.errorList );
			} else {
				this.defaultShowErrors();
			}
		},

		// http://jqueryvalidation.org/Validator.resetForm/
		resetForm: function() {
			if ( $.fn.resetForm ) {
				$( this.currentForm ).resetForm();
			}
			this.submitted = {};
			this.lastElement = null;
			this.prepareForm();
			this.hideErrors();
			var i, elements = this.elements()
				.removeData( "previousValue" )
				.removeAttr( "aria-invalid" );

			if ( this.settings.unhighlight ) {
				for ( i = 0; elements[ i ]; i++ ) {
					this.settings.unhighlight.call( this, elements[ i ],
						this.settings.errorClass, "" );
				}
			} else {
				elements.removeClass( this.settings.errorClass );
			}
		},

		numberOfInvalids: function() {
			return this.objectLength( this.invalid );
		},

		objectLength: function( obj ) {
			/* jshint unused: false */
			var count = 0,
				i;
			for ( i in obj ) {
				count++;
			}
			return count;
		},

		hideErrors: function() {
			this.hideThese( this.toHide );
		},

		hideThese: function( errors ) {
			errors.not( this.containers ).text( "" );
			this.addWrapper( errors ).hide();
		},

		valid: function() {
			return this.size() === 0;
		},

		size: function() {
			return this.errorList.length;
		},

		focusInvalid: function() {
			if ( this.settings.focusInvalid ) {
				try {
					$( this.findLastActive() || this.errorList.length && this.errorList[ 0 ].element || [])
					.filter( ":visible" )
					.focus()
					// manually trigger focusin event; without it, focusin handler isn't called, findLastActive won't have anything to find
					.trigger( "focusin" );
				} catch ( e ) {
					// ignore IE throwing errors when focusing hidden elements
				}
			}
		},

		findLastActive: function() {
			var lastActive = this.lastActive;
			return lastActive && $.grep( this.errorList, function( n ) {
				return n.element.name === lastActive.name;
			}).length === 1 && lastActive;
		},

		elements: function() {
			var validator = this,
				rulesCache = {};

			// select all valid inputs inside the form (no submit or reset buttons)
			return $( this.currentForm )
			.find( "input, select, textarea" )
			.not( ":submit, :reset, :image, :disabled" )
			.not( this.settings.ignore )
			.filter( function() {
				if ( !this.name && validator.settings.debug && window.console ) {
					console.error( "%o has no name assigned", this );
				}

				// select only the first element for each name, and only those with rules specified
				if ( this.name in rulesCache || !validator.objectLength( $( this ).rules() ) ) {
					return false;
				}

				rulesCache[ this.name ] = true;
				return true;
			});
		},

		clean: function( selector ) {
			return $( selector )[ 0 ];
		},

		errors: function() {
			var errorClass = this.settings.errorClass.split( " " ).join( "." );
			return $( this.settings.errorElement + "." + errorClass, this.errorContext );
		},

		reset: function() {
			this.successList = [];
			this.errorList = [];
			this.errorMap = {};
			this.toShow = $( [] );
			this.toHide = $( [] );
			this.currentElements = $( [] );
		},

		prepareForm: function() {
			this.reset();
			this.toHide = this.errors().add( this.containers );
		},

		prepareElement: function( element ) {
			this.reset();
			this.toHide = this.errorsFor( element );
		},

		elementValue: function( element ) {
			var val,
				$element = $( element ),
				type = element.type;

			if ( type === "radio" || type === "checkbox" ) {
				return this.findByName( element.name ).filter(":checked").val();
			} else if ( type === "number" && typeof element.validity !== "undefined" ) {
				return element.validity.badInput ? false : $element.val();
			}

			val = $element.val();
			if ( typeof val === "string" ) {
				return val.replace(/\r/g, "" );
			}
			return val;
		},

		check: function( element ) {
			element = this.validationTargetFor( this.clean( element ) );

			var rules = $( element ).rules(),
				rulesCount = $.map( rules, function( n, i ) {
					return i;
				}).length,
				dependencyMismatch = false,
				val = this.elementValue( element ),
				result, method, rule;

			for ( method in rules ) {
				rule = { method: method, parameters: rules[ method ] };
				try {

					result = $.validator.methods[ method ].call( this, val, element, rule.parameters );

					// if a method indicates that the field is optional and therefore valid,
					// don't mark it as valid when there are no other rules
					if ( result === "dependency-mismatch" && rulesCount === 1 ) {
						dependencyMismatch = true;
						continue;
					}
					dependencyMismatch = false;

					if ( result === "pending" ) {
						this.toHide = this.toHide.not( this.errorsFor( element ) );
						return;
					}

					if ( !result ) {
						this.formatAndAdd( element, rule );
						return false;
					}
				} catch ( e ) {
					if ( this.settings.debug && window.console ) {
						console.log( "Exception occurred when checking element " + element.id + ", check the '" + rule.method + "' method.", e );
					}
					if ( e instanceof TypeError ) {
						e.message += ".  Exception occurred when checking element " + element.id + ", check the '" + rule.method + "' method.";
					}

					throw e;
				}
			}
			if ( dependencyMismatch ) {
				return;
			}
			if ( this.objectLength( rules ) ) {
				this.successList.push( element );
			}
			return true;
		},

		// return the custom message for the given element and validation method
		// specified in the element's HTML5 data attribute
		// return the generic message if present and no method specific message is present
		customDataMessage: function( element, method ) {
			return $( element ).data( "msg" + method.charAt( 0 ).toUpperCase() +
				method.substring( 1 ).toLowerCase() ) || $( element ).data( "msg" );
		},

		// return the custom message for the given element name and validation method
		customMessage: function( name, method ) {
			var m = this.settings.messages[ name ];
			return m && ( m.constructor === String ? m : m[ method ]);
		},

		// return the first defined argument, allowing empty strings
		findDefined: function() {
			for ( var i = 0; i < arguments.length; i++) {
				if ( arguments[ i ] !== undefined ) {
					return arguments[ i ];
				}
			}
			return undefined;
		},

		defaultMessage: function( element, method ) {
			return this.findDefined(
				this.customMessage( element.name, method ),
				this.customDataMessage( element, method ),
				// title is never undefined, so handle empty string as undefined
				!this.settings.ignoreTitle && element.title || undefined,
				$.validator.messages[ method ],
				"<strong>Warning: No message defined for " + element.name + "</strong>"
			);
		},

		formatAndAdd: function( element, rule ) {
			var message = this.defaultMessage( element, rule.method ),
				theregex = /\$?\{(\d+)\}/g;
			if ( typeof message === "function" ) {
				message = message.call( this, rule.parameters, element );
			} else if ( theregex.test( message ) ) {
				message = $.validator.format( message.replace( theregex, "{$1}" ), rule.parameters );
			}
			this.errorList.push({
				message: message,
				element: element,
				method: rule.method
			});

			this.errorMap[ element.name ] = message;
			this.submitted[ element.name ] = message;
		},

		addWrapper: function( toToggle ) {
			if ( this.settings.wrapper ) {
				toToggle = toToggle.add( toToggle.parent( this.settings.wrapper ) );
			}
			return toToggle;
		},

		defaultShowErrors: function() {
			var i, elements, error;
			for ( i = 0; this.errorList[ i ]; i++ ) {
				error = this.errorList[ i ];
				if ( this.settings.highlight ) {
					this.settings.highlight.call( this, error.element, this.settings.errorClass, this.settings.validClass );
				}
				this.showLabel( error.element, error.message );
			}
			if ( this.errorList.length ) {
				this.toShow = this.toShow.add( this.containers );
			}
			if ( this.settings.success ) {
				for ( i = 0; this.successList[ i ]; i++ ) {
					this.showLabel( this.successList[ i ] );
				}
			}
			if ( this.settings.unhighlight ) {
				for ( i = 0, elements = this.validElements(); elements[ i ]; i++ ) {
					this.settings.unhighlight.call( this, elements[ i ], this.settings.errorClass, this.settings.validClass );
				}
			}
			this.toHide = this.toHide.not( this.toShow );
			this.hideErrors();
			this.addWrapper( this.toShow ).show();
		},

		validElements: function() {
			return this.currentElements.not( this.invalidElements() );
		},

		invalidElements: function() {
			return $( this.errorList ).map(function() {
				return this.element;
			});
		},

		showLabel: function( element, message ) {
			var place, group, errorID,
				error = this.errorsFor( element ),
				elementID = this.idOrName( element ),
				describedBy = $( element ).attr( "aria-describedby" );
			if ( error.length ) {
				// refresh error/success class
				error.removeClass( this.settings.validClass ).addClass( this.settings.errorClass );
				// replace message on existing label
				error.html( message );
			} else {
				// create error element
				error = $( "<" + this.settings.errorElement + ">" )
					.attr( "id", elementID + "-error" )
					.addClass( this.settings.errorClass )
					.html( message || "" );

				// Maintain reference to the element to be placed into the DOM
				place = error;
				if ( this.settings.wrapper ) {
					// make sure the element is visible, even in IE
					// actually showing the wrapped element is handled elsewhere
					place = error.hide().show().wrap( "<" + this.settings.wrapper + "/>" ).parent();
				}
				if ( this.labelContainer.length ) {
					this.labelContainer.append( place );
				} else if ( this.settings.errorPlacement ) {
					this.settings.errorPlacement( place, $( element ) );
				} else {
					place.insertAfter( element );
				}

				// Link error back to the element
				if ( error.is( "label" ) ) {
					// If the error is a label, then associate using 'for'
					error.attr( "for", elementID );
				} else if ( error.parents( "label[for='" + elementID + "']" ).length === 0 ) {
					// If the element is not a child of an associated label, then it's necessary
					// to explicitly apply aria-describedby

					errorID = error.attr( "id" ).replace( /(:|\.|\[|\]|\$)/g, "\\$1");
					// Respect existing non-error aria-describedby
					if ( !describedBy ) {
						describedBy = errorID;
					} else if ( !describedBy.match( new RegExp( "\\b" + errorID + "\\b" ) ) ) {
						// Add to end of list if not already present
						describedBy += " " + errorID;
					}
					$( element ).attr( "aria-describedby", describedBy );

					// If this element is grouped, then assign to all elements in the same group
					group = this.groups[ element.name ];
					if ( group ) {
						$.each( this.groups, function( name, testgroup ) {
							if ( testgroup === group ) {
								$( "[name='" + name + "']", this.currentForm )
									.attr( "aria-describedby", error.attr( "id" ) );
							}
						});
					}
				}
			}
			if ( !message && this.settings.success ) {
				error.text( "" );
				if ( typeof this.settings.success === "string" ) {
					error.addClass( this.settings.success );
				} else {
					this.settings.success( error, element );
				}
			}
			this.toShow = this.toShow.add( error );
		},

		errorsFor: function( element ) {
			var name = this.idOrName( element ),
				describer = $( element ).attr( "aria-describedby" ),
				selector = "label[for='" + name + "'], label[for='" + name + "'] *";

			// aria-describedby should directly reference the error element
			if ( describer ) {
				selector = selector + ", #" + describer.replace( /\s+/g, ", #" );
			}
			return this
				.errors()
				.filter( selector );
		},

		idOrName: function( element ) {
			return this.groups[ element.name ] || ( this.checkable( element ) ? element.name : element.id || element.name );
		},

		validationTargetFor: function( element ) {

			// If radio/checkbox, validate first element in group instead
			if ( this.checkable( element ) ) {
				element = this.findByName( element.name );
			}

			// Always apply ignore filter
			return $( element ).not( this.settings.ignore )[ 0 ];
		},

		checkable: function( element ) {
			return ( /radio|checkbox/i ).test( element.type );
		},

		findByName: function( name ) {
			return $( this.currentForm ).find( "[name='" + name + "']" );
		},

		getLength: function( value, element ) {
			switch ( element.nodeName.toLowerCase() ) {
			case "select":
				return $( "option:selected", element ).length;
			case "input":
				if ( this.checkable( element ) ) {
					return this.findByName( element.name ).filter( ":checked" ).length;
				}
			}
			return value.length;
		},

		depend: function( param, element ) {
			return this.dependTypes[typeof param] ? this.dependTypes[typeof param]( param, element ) : true;
		},

		dependTypes: {
			"boolean": function( param ) {
				return param;
			},
			"string": function( param, element ) {
				return !!$( param, element.form ).length;
			},
			"function": function( param, element ) {
				return param( element );
			}
		},

		optional: function( element ) {
			var val = this.elementValue( element );
			return !$.validator.methods.required.call( this, val, element ) && "dependency-mismatch";
		},

		startRequest: function( element ) {
			if ( !this.pending[ element.name ] ) {
				this.pendingRequest++;
				this.pending[ element.name ] = true;
			}
		},

		stopRequest: function( element, valid ) {
			this.pendingRequest--;
			// sometimes synchronization fails, make sure pendingRequest is never < 0
			if ( this.pendingRequest < 0 ) {
				this.pendingRequest = 0;
			}
			delete this.pending[ element.name ];
			if ( valid && this.pendingRequest === 0 && this.formSubmitted && this.form() ) {
				$( this.currentForm ).submit();
				this.formSubmitted = false;
			} else if (!valid && this.pendingRequest === 0 && this.formSubmitted ) {
				$( this.currentForm ).triggerHandler( "invalid-form", [ this ]);
				this.formSubmitted = false;
			}
		},

		previousValue: function( element ) {
			return $.data( element, "previousValue" ) || $.data( element, "previousValue", {
				old: null,
				valid: true,
				message: this.defaultMessage( element, "remote" )
			});
		},

		// cleans up all forms and elements, removes validator-specific events
		destroy: function() {
			this.resetForm();

			$( this.currentForm )
				.off( ".validate" )
				.removeData( "validator" );
		}

	},

	classRuleSettings: {
		required: { required: true },
		email: { email: true },
		url: { url: true },
		date: { date: true },
		dateISO: { dateISO: true },
		number: { number: true },
		digits: { digits: true },
		creditcard: { creditcard: true }
	},

	addClassRules: function( className, rules ) {
		if ( className.constructor === String ) {
			this.classRuleSettings[ className ] = rules;
		} else {
			$.extend( this.classRuleSettings, className );
		}
	},

	classRules: function( element ) {
		var rules = {},
			classes = $( element ).attr( "class" );

		if ( classes ) {
			$.each( classes.split( " " ), function() {
				if ( this in $.validator.classRuleSettings ) {
					$.extend( rules, $.validator.classRuleSettings[ this ]);
				}
			});
		}
		return rules;
	},

	normalizeAttributeRule: function( rules, type, method, value ) {

		// convert the value to a number for number inputs, and for text for backwards compability
		// allows type="date" and others to be compared as strings
		if ( /min|max/.test( method ) && ( type === null || /number|range|text/.test( type ) ) ) {
			value = Number( value );

			// Support Opera Mini, which returns NaN for undefined minlength
			if ( isNaN( value ) ) {
				value = undefined;
			}
		}

		if ( value || value === 0 ) {
			rules[ method ] = value;
		} else if ( type === method && type !== "range" ) {

			// exception: the jquery validate 'range' method
			// does not test for the html5 'range' type
			rules[ method ] = true;
		}
	},

	attributeRules: function( element ) {
		var rules = {},
			$element = $( element ),
			type = element.getAttribute( "type" ),
			method, value;

		for ( method in $.validator.methods ) {

			// support for <input required> in both html5 and older browsers
			if ( method === "required" ) {
				value = element.getAttribute( method );

				// Some browsers return an empty string for the required attribute
				// and non-HTML5 browsers might have required="" markup
				if ( value === "" ) {
					value = true;
				}

				// force non-HTML5 browsers to return bool
				value = !!value;
			} else {
				value = $element.attr( method );
			}

			this.normalizeAttributeRule( rules, type, method, value );
		}

		// maxlength may be returned as -1, 2147483647 ( IE ) and 524288 ( safari ) for text inputs
		if ( rules.maxlength && /-1|2147483647|524288/.test( rules.maxlength ) ) {
			delete rules.maxlength;
		}

		return rules;
	},

	dataRules: function( element ) {
		var rules = {},
			$element = $( element ),
			type = element.getAttribute( "type" ),
			method, value;

		for ( method in $.validator.methods ) {
			value = $element.data( "rule" + method.charAt( 0 ).toUpperCase() + method.substring( 1 ).toLowerCase() );
			this.normalizeAttributeRule( rules, type, method, value );
		}
		return rules;
	},

	staticRules: function( element ) {
		var rules = {},
			validator = $.data( element.form, "validator" );

		if ( validator.settings.rules ) {
			rules = $.validator.normalizeRule( validator.settings.rules[ element.name ] ) || {};
		}
		return rules;
	},

	normalizeRules: function( rules, element ) {
		// handle dependency check
		$.each( rules, function( prop, val ) {
			// ignore rule when param is explicitly false, eg. required:false
			if ( val === false ) {
				delete rules[ prop ];
				return;
			}
			if ( val.param || val.depends ) {
				var keepRule = true;
				switch ( typeof val.depends ) {
				case "string":
					keepRule = !!$( val.depends, element.form ).length;
					break;
				case "function":
					keepRule = val.depends.call( element, element );
					break;
				}
				if ( keepRule ) {
					rules[ prop ] = val.param !== undefined ? val.param : true;
				} else {
					delete rules[ prop ];
				}
			}
		});

		// evaluate parameters
		$.each( rules, function( rule, parameter ) {
			rules[ rule ] = $.isFunction( parameter ) ? parameter( element ) : parameter;
		});

		// clean number parameters
		$.each([ "minlength", "maxlength" ], function() {
			if ( rules[ this ] ) {
				rules[ this ] = Number( rules[ this ] );
			}
		});
		$.each([ "rangelength", "range" ], function() {
			var parts;
			if ( rules[ this ] ) {
				if ( $.isArray( rules[ this ] ) ) {
					rules[ this ] = [ Number( rules[ this ][ 0 ]), Number( rules[ this ][ 1 ] ) ];
				} else if ( typeof rules[ this ] === "string" ) {
					parts = rules[ this ].replace(/[\[\]]/g, "" ).split( /[\s,]+/ );
					rules[ this ] = [ Number( parts[ 0 ]), Number( parts[ 1 ] ) ];
				}
			}
		});

		if ( $.validator.autoCreateRanges ) {
			// auto-create ranges
			if ( rules.min != null && rules.max != null ) {
				rules.range = [ rules.min, rules.max ];
				delete rules.min;
				delete rules.max;
			}
			if ( rules.minlength != null && rules.maxlength != null ) {
				rules.rangelength = [ rules.minlength, rules.maxlength ];
				delete rules.minlength;
				delete rules.maxlength;
			}
		}

		return rules;
	},

	// Converts a simple string to a {string: true} rule, e.g., "required" to {required:true}
	normalizeRule: function( data ) {
		if ( typeof data === "string" ) {
			var transformed = {};
			$.each( data.split( /\s/ ), function() {
				transformed[ this ] = true;
			});
			data = transformed;
		}
		return data;
	},

	// http://jqueryvalidation.org/jQuery.validator.addMethod/
	addMethod: function( name, method, message ) {
		$.validator.methods[ name ] = method;
		$.validator.messages[ name ] = message !== undefined ? message : $.validator.messages[ name ];
		if ( method.length < 3 ) {
			$.validator.addClassRules( name, $.validator.normalizeRule( name ) );
		}
	},

	methods: {

		// http://jqueryvalidation.org/required-method/
		required: function( value, element, param ) {
			// check if dependency is met
			if ( !this.depend( param, element ) ) {
				return "dependency-mismatch";
			}
			if ( element.nodeName.toLowerCase() === "select" ) {
				// could be an array for select-multiple or a string, both are fine this way
				var val = $( element ).val();
				return val && val.length > 0;
			}
			if ( this.checkable( element ) ) {
				return this.getLength( value, element ) > 0;
			}
			return value.length > 0;
		},

		// http://jqueryvalidation.org/email-method/
		email: function( value, element ) {
			// From https://html.spec.whatwg.org/multipage/forms.html#valid-e-mail-address
			// Retrieved 2014-01-14
			// If you have a problem with this implementation, report a bug against the above spec
			// Or use custom methods to implement your own email validation
			return this.optional( element ) || /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test( value );
		},

		// http://jqueryvalidation.org/url-method/
		url: function( value, element ) {

			// Copyright (c) 2010-2013 Diego Perini, MIT licensed
			// https://gist.github.com/dperini/729294
			// see also https://mathiasbynens.be/demo/url-regex
			// modified to allow protocol-relative URLs
			return this.optional( element ) || /^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})).?)(?::\d{2,5})?(?:[/?#]\S*)?$/i.test( value );
		},

		// http://jqueryvalidation.org/date-method/
		date: function( value, element ) {
			return this.optional( element ) || !/Invalid|NaN/.test( new Date( value ).toString() );
		},

		// http://jqueryvalidation.org/dateISO-method/
		dateISO: function( value, element ) {
			return this.optional( element ) || /^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$/.test( value );
		},

		// http://jqueryvalidation.org/number-method/
		number: function( value, element ) {
			return this.optional( element ) || /^(?:-?\d+|-?\d{1,3}(?:,\d{3})+)?(?:\.\d+)?$/.test( value );
		},

		// http://jqueryvalidation.org/digits-method/
		digits: function( value, element ) {
			return this.optional( element ) || /^\d+$/.test( value );
		},

		// http://jqueryvalidation.org/creditcard-method/
		// based on http://en.wikipedia.org/wiki/Luhn_algorithm
		creditcard: function( value, element ) {
			if ( this.optional( element ) ) {
				return "dependency-mismatch";
			}
			// accept only spaces, digits and dashes
			if ( /[^0-9 \-]+/.test( value ) ) {
				return false;
			}
			var nCheck = 0,
				nDigit = 0,
				bEven = false,
				n, cDigit;

			value = value.replace( /\D/g, "" );

			// Basing min and max length on
			// http://developer.ean.com/general_info/Valid_Credit_Card_Types
			if ( value.length < 13 || value.length > 19 ) {
				return false;
			}

			for ( n = value.length - 1; n >= 0; n--) {
				cDigit = value.charAt( n );
				nDigit = parseInt( cDigit, 10 );
				if ( bEven ) {
					if ( ( nDigit *= 2 ) > 9 ) {
						nDigit -= 9;
					}
				}
				nCheck += nDigit;
				bEven = !bEven;
			}

			return ( nCheck % 10 ) === 0;
		},

		// http://jqueryvalidation.org/minlength-method/
		minlength: function( value, element, param ) {
			var length = $.isArray( value ) ? value.length : this.getLength( value, element );
			return this.optional( element ) || length >= param;
		},

		// http://jqueryvalidation.org/maxlength-method/
		maxlength: function( value, element, param ) {
			var length = $.isArray( value ) ? value.length : this.getLength( value, element );
			return this.optional( element ) || length <= param;
		},

		// http://jqueryvalidation.org/rangelength-method/
		rangelength: function( value, element, param ) {
			var length = $.isArray( value ) ? value.length : this.getLength( value, element );
			return this.optional( element ) || ( length >= param[ 0 ] && length <= param[ 1 ] );
		},

		// http://jqueryvalidation.org/min-method/
		min: function( value, element, param ) {
			return this.optional( element ) || value >= param;
		},

		// http://jqueryvalidation.org/max-method/
		max: function( value, element, param ) {
			return this.optional( element ) || value <= param;
		},

		// http://jqueryvalidation.org/range-method/
		range: function( value, element, param ) {
			return this.optional( element ) || ( value >= param[ 0 ] && value <= param[ 1 ] );
		},

		// http://jqueryvalidation.org/equalTo-method/
		equalTo: function( value, element, param ) {
			// bind to the blur event of the target in order to revalidate whenever the target field is updated
			// TODO find a way to bind the event just once, avoiding the unbind-rebind overhead
			var target = $( param );
			if ( this.settings.onfocusout ) {
				target.off( ".validate-equalTo" ).on( "blur.validate-equalTo", function() {
					$( element ).valid();
				});
			}
			return value === target.val();
		},

		// http://jqueryvalidation.org/remote-method/
		remote: function( value, element, param ) {
			if ( this.optional( element ) ) {
				return "dependency-mismatch";
			}

			var previous = this.previousValue( element ),
				validator, data;

			if (!this.settings.messages[ element.name ] ) {
				this.settings.messages[ element.name ] = {};
			}
			previous.originalMessage = this.settings.messages[ element.name ].remote;
			this.settings.messages[ element.name ].remote = previous.message;

			param = typeof param === "string" && { url: param } || param;

			if ( previous.old === value ) {
				return previous.valid;
			}

			previous.old = value;
			validator = this;
			this.startRequest( element );
			data = {};
			data[ element.name ] = value;
			$.ajax( $.extend( true, {
				mode: "abort",
				port: "validate" + element.name,
				dataType: "json",
				data: data,
				context: validator.currentForm,
				success: function( response ) {
					var valid = response === true || response === "true",
						errors, message, submitted;

					validator.settings.messages[ element.name ].remote = previous.originalMessage;
					if ( valid ) {
						submitted = validator.formSubmitted;
						validator.prepareElement( element );
						validator.formSubmitted = submitted;
						validator.successList.push( element );
						delete validator.invalid[ element.name ];
						validator.showErrors();
					} else {
						errors = {};
						message = response || validator.defaultMessage( element, "remote" );
						errors[ element.name ] = previous.message = $.isFunction( message ) ? message( value ) : message;
						validator.invalid[ element.name ] = true;
						validator.showErrors( errors );
					}
					previous.valid = valid;
					validator.stopRequest( element, valid );
				}
			}, param ) );
			return "pending";
		}
	}

});

// ajax mode: abort
// usage: $.ajax({ mode: "abort"[, port: "uniqueport"]});
// if mode:"abort" is used, the previous request on that port (port can be undefined) is aborted via XMLHttpRequest.abort()

var pendingRequests = {},
	ajax;
// Use a prefilter if available (1.5+)
if ( $.ajaxPrefilter ) {
	$.ajaxPrefilter(function( settings, _, xhr ) {
		var port = settings.port;
		if ( settings.mode === "abort" ) {
			if ( pendingRequests[port] ) {
				pendingRequests[port].abort();
			}
			pendingRequests[port] = xhr;
		}
	});
} else {
	// Proxy ajax
	ajax = $.ajax;
	$.ajax = function( settings ) {
		var mode = ( "mode" in settings ? settings : $.ajaxSettings ).mode,
			port = ( "port" in settings ? settings : $.ajaxSettings ).port;
		if ( mode === "abort" ) {
			if ( pendingRequests[port] ) {
				pendingRequests[port].abort();
			}
			pendingRequests[port] = ajax.apply(this, arguments);
			return pendingRequests[port];
		}
		return ajax.apply(this, arguments);
	};
}

}));;/**/
/* Arabic Translation for jQuery UI date picker plugin. */
/* Used in most of Arab countries, primarily in Bahrain, Kuwait, Oman, Qatar, Saudi Arabia and the United Arab Emirates, Egypt, Sudan and Yemen. */
/* Written by Mohammed Alshehri -- m@dralshehri.com */

( function( factory ) {
	if ( typeof define === "function" && define.amd ) {

		// AMD. Register as an anonymous module.
		define( [ "../widgets/datepicker" ], factory );
	} else {

		// Browser globals
		factory( jQuery.datepicker );
	}
}( function( datepicker ) {

datepicker.regional.ar = {
	closeText: "إغلاق",
	prevText: "&#x3C;السابق",
	nextText: "التالي&#x3E;",
	currentText: "اليوم",
	monthNames: [ "يناير", "فبراير", "مارس", "أبريل", "مايو", "يونيو",
	"يوليو", "أغسطس", "سبتمبر", "أكتوبر", "نوفمبر", "ديسمبر" ],
	monthNamesShort: [ "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12" ],
	dayNames: [ "الأحد", "الاثنين", "الثلاثاء", "الأربعاء", "الخميس", "الجمعة", "السبت" ],
	dayNamesShort: [ "أحد", "اثنين", "ثلاثاء", "أربعاء", "خميس", "جمعة", "سبت" ],
	dayNamesMin: [ "ح", "ن", "ث", "ر", "خ", "ج", "س" ],
	weekHeader: "أسبوع",
	dateFormat: "dd/mm/yy",
	firstDay: 0,
		isRTL: true,
	showMonthAfterYear: false,
	yearSuffix: "" };
datepicker.setDefaults( datepicker.regional.ar );

return datepicker.regional.ar;

} ) );
;/**/
/**
 * Number.prototype.format(n, x, s, c)
 *
 * @param n         integer, length of decimal
 * @param x         integer, length of whole part
 * @param s         mixed, sections delimiter
 * @param c         mixed, decimal delimiter
 */
Number.prototype.format = function (n, x, s, c) {
  var re  = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
      num = this.toFixed(Math.max(0, ~~n));

  return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
};

/**
 * Trim at length
 * @param m
 * @returns {string}
 */
String.prototype.trimToLength = function (m) {

  return (this.length > m)
    ? jQuery.trim(this).substring(0, m) : this;
};

function placeCaretAtEnd(el) {
  el.focus();
  if (typeof window.getSelection != "undefined"
    && typeof document.createRange != "undefined") {
    var range = document.createRange();
    range.selectNodeContents(el);
    range.collapse(false);
    var sel   = window.getSelection();
    sel.removeAllRanges();
    sel.addRange(range);
  }
  else if (typeof document.body.createTextRange != "undefined") {
    var textRange = document.body.createTextRange();
    textRange.moveToElementText(el);
    textRange.collapse(false);
    textRange.select();
  }
}

/**
 * Load Cart from JSON
 * @param url
 * @param data
 */
function loadFromJSON(url, data) {

  if (data == null) {
    data = {};
  }

  $.getJSON(url, data)
    .done(function (json) {
      console.log('Tickets loaded from JSON', json);
      return json;
    })
    .fail(function (jqxhr, textStatus, error) {
      var err = textStatus + ", " + error;
      console.log("Request Failed: " + err);
    });

  return null
}

function addStylesheetRules(rules) {
  var styleEl = document.createElement('style'),
      styleSheet;

  // Append style element to head
  document.head.appendChild(styleEl);

  // Grab style sheet
  styleSheet = styleEl.sheet;

  for (var i = 0, rl = rules.length; i < rl; i++) {
    var j = 1, rule = rules[i], selector = rules[i][0], propStr = '';
    // If the second argument of a rule is an array of arrays, correct our variables.
    if (Object.prototype.toString.call(rule[1][0]) === '[object Array]') {
      rule = rule[1];
      j    = 0;
    }

    for (var pl = rule.length; j < pl; j++) {
      var prop = rule[j];
      propStr += prop[0] + ':' + prop[1] + (prop[2] ? ' !important' : '') + ';\n';
    }

    // Insert CSS Rule
    styleSheet.insertRule(selector + '{' + propStr + '}', styleSheet.cssRules.length);
  }
}

function isNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

function isDate(txtDate) {
  var currVal = txtDate;
  if (currVal == '')
    return false;

  /*var rxDatePattern = /^(\d{1,2}) ([A-Za-z]{3}) (\d{4})$/; //Declare Regex
  var dtArray = currVal.match(rxDatePattern); // is format OK?

  if (dtArray == null)
    return false;

  console.log("array DATE", dtArray);

  //Checks for dd/mm/yyyy format.
  var dtDay   = dtArray[1];
  var dtMonth = dtArray[2];
  var dtYear  = dtArray[3];

  console.log("VERIFY DATE", dtMonth, dtDay, dtYear);

  var months = [
    "JAN",
    "FEB",
    "MAR",
    "APR",
    "MAY",
    "JUN",
    "JUL",
    "AUG",
    "SEP",
    "OCT",
    "NOV",
    "DEC"
  ];

  if (!months.indexOf(dtMonth)) {
    return false;
  }
  else if (dtDay < 1 || dtDay > 31) {
    return false;
  }
  else if ((dtMonth == 4 || dtMonth == 6 || dtMonth == 9 || dtMonth == 11) && dtDay == 31) {
    return false;
  }
  else if (dtMonth == 2) {
    var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));

    if (dtDay > 29 || (dtDay == 29 && !isleap)) {
      return false;
    }
  }*/
  return true;
};/**/
var $ = jQuery;

// Variables max/limit in booking
var MAX_TICKETS      = 15;
var MAX_HOTEL_GUESTS = 15;
var MAX_SHOW_ROOM    = 2;
var MAX_SHOW_TICKETS = 3;
var MAX_ROOMS        = 5;
var MIN_CHILD_AGE    = 3;
var MAX_CHILD_AGE    = 10;
var MIN_ADULT_GUEST  = 1;

// Initial filter configuration
var MIN_PRICE_RANGE      = 0;
var MAX_PRICE_RANGE      = 20000;
var START_RATING         = 0;
var START_RECOMMENDATION = true;
var DISTANCE_FROM        = "poiDubaiPark579";

// Set default max Date on all datepicker +18 months from now
$.datepicker.setDefaults({
  maxDate: "+18m"
});;/**/
var messages = {
  'errorQtyTickets'         : 'You cannot buy more than @nbTickets tickets.',
  'errorNullGuests'         : 'You must select guests for this ticket.',
  'errorNullDate'           : 'You must select a valid date.',
  'erasePreviousTicket'     : 'Tickets in cart will be replace by these ones.',
  'erasePreviousHotel'      : 'Hotel and hotel rooms in cart will be replaced by these ones.',
  'erasePreviousHotelTicket': 'Hotel, hotel rooms, and tickets in cart will be replaced by these ones.',
  'errorSearchCheckin'      : 'You must select a checkin date.',
  'errorSearchCheckout'     : 'You must select a checkout date.',
  'errorSearchNullGuests'   : 'You must select at least one guest.',
  'errorSearchNullRooms'    : 'You must insert at least one room.',
  'errorSearchMaxRooms'     : 'You can insert a maximum of @maxRooms rooms.',
  'errorQtyGuests'          : 'You cannot add more than @nbGuests guests.',
  'errorMinAdultGuest'      : 'At least one adult must be inserted.',
  'errorMinAdultRoomGuest'  : 'At least one adult per room must be inserted.',
  'errorChildAge'           : 'Please indicate children ages.',
  'errorNumberRoomsStep2'   : 'Please select @nbRooms rooms before continuing.',
  'errorInsertCart'         : 'Error while adding in Cart. Please retry later.',
  'errorSearch'             : 'Error while searching. Please retry later.',
  'errorFormFields'         : 'All fields are required.',
  'errorMissingGuestsForm'  : 'You must fill the guests form before booking.',
  'invalidChrInField'       : 'Invalid characters in @fieldName.'
};

;/**/
var $ = jQuery;

var ticketsParks    = {};
var hotelsList      = {};
var cartTicketsType = "EXPERIENCE";
var cartHotelType   = "HOTEL";
var cartPackageType = "PACKAGE";

/**
 * @constructor
 */
var Cart = function () {

  this.tickets = {};
  this.hotels  = {};
  this.package = {};

  this.type = "";

  this.totalPrice  = 0;
  this.totalGuests = 0;

  this.ticketsTotalGuests = 0;
  this.ticketsTotalPrice  = 0;

  this.hotelTotalGuests = 0;
  this.hotelTotalPrice  = 0;
  this.hotelTotalRooms  = 0;

  this.pluralGuestsForm = {
    adult : {
      singular: 'Adult',
      plural  : 'Adults'
    },
    child : {
      singular: 'Child',
      plural  : 'Children'
    },
    senior: {
      singular: 'Senior',
      plural  : 'Seniors'
    }
  };

};

/**
 * Register Cart in Cookie
 * @param name
 * @param path
 */
Cart.prototype.register = function (name, path) {

  if (name == null) {
    name = 'cartSummary';
  }
  var date = new Date();
  var minutes = 60;
  date.setTime(date.getTime() + (minutes * 60 * 1000));

  $.cookie('cartTickets', JSON.stringify(this.tickets), {path: path, expires: date});
  $.cookie('cartHotels', JSON.stringify(this.hotels), {path: path});
  $.cookie('cartPackage', JSON.stringify(this.package), {path: path});
  $.cookie('cartType', JSON.stringify(this.type), {path: path});

  //console.log('SAVING CART', this);
};

/**
 * Load Cart from Cookie
 * @param name
 */
Cart.prototype.load = function (name) {

  this.tickets = JSON.parse($.cookie('cartTickets')) || {};
  this.hotels  = JSON.parse($.cookie('cartHotels')) || {};
  this.package = JSON.parse($.cookie('cartPackage')) || {};
  this.type    = JSON.parse($.cookie('cartType')) || "";

  //console.log('LOADING CART', this);
};

/**
 * Erase cart
 */
Cart.prototype.eraseCartTickets = function () {
  for (var ticket_id in this.tickets) {
    if (this.tickets.hasOwnProperty(ticket_id)) {
      for (var access_type in this.tickets[ticket_id]) {
        if (this.tickets[ticket_id].hasOwnProperty(access_type)) {
          for (var ticket_type in this.tickets[ticket_id][access_type]) {
            if (this.tickets[ticket_id][access_type].hasOwnProperty(ticket_type)) {
              delete this.tickets[ticket_id][access_type][ticket_type];
            }
          }
          delete this.tickets[ticket_id][access_type];
        }
      }
      delete this.tickets[ticket_id];
    }
  }
};

/**
 * Calculate Cart total price.
 * @returns {number}
 */
Cart.prototype.calcTotalPrice = function () {

  var totalPrice = 0;

  for (var ticket_id in this.tickets) {
    if (this.tickets.hasOwnProperty(ticket_id)) {
      for (var access_type in this.tickets[ticket_id]) {
        if (this.tickets[ticket_id].hasOwnProperty(access_type)) {
          for (var ticket_type in this.tickets[ticket_id][access_type]) {
            if (this.tickets[ticket_id][access_type].hasOwnProperty(ticket_type)) {
              totalPrice += this.tickets[ticket_id][access_type][ticket_type].ticketPrice;
            }
          }
        }
      }
    }
  }

  for (var hotel_id in this.hotels) {
    if (this.hotels.hasOwnProperty(hotel_id)) {
      totalPrice += this.hotels[hotel_id].totalPrice;
    }
  }

  this.totalPrice = totalPrice;

  //console.log('TOTAL PRICE', this.totalPrice);

  /*$(document).trigger({
   type: 'updateTotalPrice'
   });*/

  return totalPrice;
};

/**
 *
 * @returns {number}
 */
Cart.prototype.calcTotalGuests = function () {

  var totalGuests = 0;

  totalGuests += this.calcTicketsTotalGuests();
  totalGuests += this.calcHotelTotalGuests();

  this.totalGuests = totalGuests;

  $(document).trigger({
    type: 'updateTicketsGuests'
  });

  return totalGuests;
};

/************* TICKETS *************/

/**
 * Calculate Cart total price.
 * @returns {number}
 */
Cart.prototype.calcTicketsTotalPrice = function () {

  var totalPrice = 0;

  for (var ticket_id in this.tickets) {
    if (this.tickets.hasOwnProperty(ticket_id)) {
      for (var access_type in this.tickets[ticket_id]) {
        if (this.tickets[ticket_id].hasOwnProperty(access_type)) {
          for (var ticket_type in this.tickets[ticket_id][access_type]) {
            if (this.tickets[ticket_id][access_type].hasOwnProperty(ticket_type)) {
              totalPrice += this.tickets[ticket_id][access_type][ticket_type].ticketPrice;
            }
          }
        }
      }
    }
  }

  this.ticketsTotalPrice = totalPrice;

  //console.log('TOTAL PRICE', this.totalPrice);

  $(document).trigger({
    type: 'updateTicketsTotalPrice'
  });

  return totalPrice;
};

/**
 * Calc price of one ticket
 * @param ticket_id         park_id
 * @param access_type
 * @param ticket_type
 * @returns {number}
 */
Cart.prototype.calcTicketPrice = function (ticket_id, access_type, ticket_type) {

  var ticket      = this.tickets[ticket_id][access_type][ticket_type];
  var ticketPrice = 0;

  if (ticket) {
    ticketPrice += (ticket.adult.number * ticket.adult.price);
    ticketPrice += (ticket.child.number * ticket.child.price);
    ticketPrice += (ticket.senior.number * ticket.senior.price);

    this.tickets[ticket_id][access_type][ticket_type].ticketPrice = ticketPrice;
  }

  $(document).trigger({
    type      : 'updateTicketPrice',
    ticketId  : ticket_id,
    accessType: access_type,
    ticketType: ticket_type
  });

  return ticketPrice;
};

/**
 *
 * @returns {number}
 */
Cart.prototype.calcTicketsTotalGuests = function () {

  var totalGuests = 0;

  for (var ticket_id in this.tickets) {
    if (this.tickets.hasOwnProperty(ticket_id)) {
      for (var access_type in this.tickets[ticket_id]) {
        if (this.tickets[ticket_id].hasOwnProperty(access_type)) {
          for (var ticket_type in this.tickets[ticket_id][access_type]) {
            if (this.tickets[ticket_id][access_type].hasOwnProperty(ticket_type)) {
              totalGuests += this.tickets[ticket_id][access_type][ticket_type].total;
            }
          }
        }
      }
    }
  }

  this.ticketsTotalGuests = totalGuests;

  $(document).trigger({
    type: 'updateTicketsGuests'
  });

  return totalGuests;
};

/**
 * Create ticket if not exists
 * @param ticket_id
 * @param access_type
 * @param ticket_type
 * @returns {bool}
 */
Cart.prototype.ticketExistsOrCreate = function (ticket_id, access_type, ticket_type) {

  //console.log("CREATE TICKET", this.tickets, ticket_id, access_type, ticket_type);

  if (!this.tickets[ticket_id]) {
    this.tickets[ticket_id] = {};
  }
  if (!this.tickets[ticket_id][access_type]) {
    this.tickets[ticket_id][access_type] = {};
  }
  if (!this.tickets[ticket_id][access_type][ticket_type]) {
    this.tickets[ticket_id][access_type][ticket_type]        = {};
    this.tickets[ticket_id][access_type][ticket_type].total  = 0;
    this.tickets[ticket_id][access_type][ticket_type].adult  = {
      number: 0,
      price : 0
    };
    this.tickets[ticket_id][access_type][ticket_type].child  = {
      number: 0,
      price : 0
    };
    this.tickets[ticket_id][access_type][ticket_type].senior = {
      number: 0,
      price : 0
    };

    return false;
  }

  return true;
};

/**
 *
 * @param ticket_id
 * @param access_type
 * @param ticket_type
 * @param ticket
 * @returns {number}
 */
Cart.prototype.addTicketGuest = function (ticket_id, access_type, ticket_type, ticket) {

  var ticket_total = ticket.adult + ticket.child + ticket.senior;

  this.ticketExistsOrCreate(ticket_id, access_type, ticket_type, ticket);

  if (!ticket_total) {
    this.removeTicket(ticket_id, access_type, ticket_type);
    return;
  }

  this.tickets[ticket_id][access_type][ticket_type].ticketName      = ticket.ticketName;
  this.tickets[ticket_id][access_type][ticket_type].ticketEventName = ticket.ticketEventName;
  this.tickets[ticket_id][access_type][ticket_type].adult.number    = ticket.adult;
  this.tickets[ticket_id][access_type][ticket_type].child.number    = ticket.child;
  this.tickets[ticket_id][access_type][ticket_type].senior.number   = ticket.senior;

  if (ticket.PriceMode) {
    this.tickets[ticket_id][access_type][ticket_type].adult.price = this.tickets[ticket_id][access_type][ticket_type].child.price = this.tickets[ticket_id][access_type][ticket_type].senior.price = parseFloat(ticket.Prices[0].Amount);
  }
  else {

    if (ticket.Prices) {
      for (var i = 0; i < ticket.Prices.length; i++) {
        switch (ticket.Prices[i].Type) {
          case "ADT":
            this.tickets[ticket_id][access_type][ticket_type].adult.price = parseFloat(ticket.Prices[i].Amount);
            break;
          case "CHD":
            this.tickets[ticket_id][access_type][ticket_type].child.price = parseFloat(ticket.Prices[i].Amount);
            break;
          case "SEN":
            this.tickets[ticket_id][access_type][ticket_type].senior.price = parseFloat(ticket.Prices[i].Amount);
            break;
        }
      }
    }
    else {
      this.tickets[ticket_id][access_type][ticket_type].ticketName      = ticketsParks.tickets[ticket_id].title;
      this.tickets[ticket_id][access_type][ticket_type].ticketEventName = ticketsParks.tickets[ticket_id].access_types[access_type][ticket_type].description;
      this.tickets[ticket_id][access_type][ticket_type].adult.price     = ticketsParks.tickets[ticket_id].access_types[access_type][ticket_type].adult;
      this.tickets[ticket_id][access_type][ticket_type].child.price     = ticketsParks.tickets[ticket_id].access_types[access_type][ticket_type].child;
      this.tickets[ticket_id][access_type][ticket_type].senior.price    = ticketsParks.tickets[ticket_id].access_types[access_type][ticket_type].senior;
    }
  }

  this.tickets[ticket_id][access_type][ticket_type].datedTicket = ticket.datedTicket;
  this.tickets[ticket_id][access_type][ticket_type].date1       = ticket.date;
  this.tickets[ticket_id][access_type][ticket_type].raw_date1   = ticket.raw_date;
  this.tickets[ticket_id][access_type][ticket_type].raw_date2   = ticket.cart_date;
  this.tickets[ticket_id][access_type][ticket_type].total       = ticket.adult + ticket.child + ticket.senior;

  this.calcTicketPrice(ticket_id, access_type, ticket_type);
  this.calcTotalPrice();
  this.calcTotalGuests();

  $(document).trigger({
    type      : 'addTicketGuest',
    ticketId  : ticket_id,
    accessType: access_type,
    ticketType: ticket_type
  });

  return this.tickets[ticket_id][access_type][ticket_type].total;
};

/**
 *
 * @param ticket_id
 * @param access_type
 * @param ticket_type
 * @param ticket
 * @returns {number}
 */
Cart.prototype.addPackageTicket = function (ticket_id, access_type, ticket_type, ticket) {

  var ticket_total = ticket.adult + ticket.child + ticket.senior;

  this.ticketExistsOrCreate(ticket_id, access_type, ticket_type, ticket);

  if (!ticket_total) {
    this.removeTicket(ticket_id, access_type, ticket_type);
    return;
  }

  this.tickets[ticket_id][access_type][ticket_type].ticketName      = ticket.ticketName;
  this.tickets[ticket_id][access_type][ticket_type].Park            = ticket.Park;
  this.tickets[ticket_id][access_type][ticket_type].ticketEventName = ticket.ticketEventName;
  this.tickets[ticket_id][access_type][ticket_type].adult.number    = ticket.adult;
  this.tickets[ticket_id][access_type][ticket_type].child.number    = ticket.child;
  this.tickets[ticket_id][access_type][ticket_type].senior.number   = ticket.senior;
  this.tickets[ticket_id][access_type][ticket_type].datedTicket     = ticket.datedTicket;
  this.tickets[ticket_id][access_type][ticket_type].date1           = ticket.date;
  this.tickets[ticket_id][access_type][ticket_type].raw_date1       = ticket.raw_date;
  this.tickets[ticket_id][access_type][ticket_type].total           = ticket.adult + ticket.child + ticket.senior;

  $(document).trigger({
    type      : 'addPackageTicket',
    ticketId  : ticket_id,
    accessType: access_type,
    ticketType: ticket_type
  });

  return this.tickets[ticket_id][access_type][ticket_type].total;
};

/**
 *
 * @param ticket_id
 * @param access_type
 * @param ticket_type
 * @param ticket
 * @returns {number}
 */
Cart.prototype.replaceTicketinCart = function (ticket_id, access_type, ticket_type, ticket) {
  this.eraseCartTickets();
  this.addTicketGuest(ticket_id, access_type, ticket_type, ticket);
};

/**
 * Set guests number in ticket by type
 * @param num
 * @param ticket_id
 * @param access_type
 * @param ticket_type
 * @param guest_type
 * @returns {boolean}
 */
Cart.prototype.setTicketGuests = function (num, ticket_id, access_type, ticket_type, guest_type) {

  if (num <= 0) {
    return false;
  }

  //console.log('SET GUEST', num, ticket_id, access_type, ticket_type, guest_type);

  this.tickets[ticket_id][access_type][ticket_type].total -= this.tickets[ticket_id][access_type][ticket_type][guest_type].number;
  this.tickets[ticket_id][access_type][ticket_type][guest_type].number = num;
  this.tickets[ticket_id][access_type][ticket_type].total += num;

  if (!this.tickets[ticket_id][access_type][ticket_type].total) {
    this.removeTicket(ticket_id, access_type, ticket_type);
  }

  this.calcTicketPrice(ticket_id, access_type, ticket_type);
  this.calcTotalPrice();
  this.calcTotalGuests();

  $(document).trigger({
    type      : 'addTicketGuest',
    ticketId  : ticket_id,
    accessType: access_type,
    ticketType: ticket_type
  });

  return true;
};

/**
 * Clone cart
 */
Cart.prototype.clone = function () {
  return jQuery.extend({}, this);
};

/**
 * Remove ticket object
 * @param ticket_id
 * @param access_type
 * @param ticket_type
 */
Cart.prototype.removeTicket = function (ticket_id, access_type, ticket_type) {
  delete this.tickets[ticket_id][access_type][ticket_type];

  if (!Object.keys(this.tickets[ticket_id][access_type]).length) {
    delete this.tickets[ticket_id][access_type];
  }

  if (!Object.keys(this.tickets[ticket_id]).length) {
    delete this.tickets[ticket_id];
  }
};

/**
 *
 * @param ticket_id
 * @param access_type
 * @param ticket_type
 * @param guest_type
 * @returns {number}
 */
Cart.prototype.removeTicketGuest = function (ticket_id, access_type, ticket_type, guest_type) {

  this.tickets[ticket_id][access_type][ticket_type][guest_type].number--;
  this.tickets[ticket_id][access_type][ticket_type].total--;

  this.calcTicketPrice(ticket_id, access_type, ticket_type);
  /*this.calcTotalPrice();
   this.calcTotalGuests();*/

  $(document).trigger({
    type      : 'removeTicketGuest',
    ticketId  : ticket_id,
    accessType: access_type,
    ticketType: ticket_type
  });

  return this.tickets[ticket_id][access_type].total;
};

/**
 *
 * @param ticket_id
 * @param access_type
 * @param ticket_type
 * @param guest_type
 */
Cart.prototype.removeMultipleTicketGuests = function (ticket_id, access_type, ticket_type, guest_type) {

  this.tickets[ticket_id][access_type][ticket_type].total -= this.tickets[ticket_id][access_type][ticket_type][guest_type].number;
  this.tickets[ticket_id][access_type][ticket_type][guest_type].number = 0;

  this.calcTicketPrice(ticket_id, access_type, ticket_type);
  this.calcTicketsTotalPrice();
  this.calcTotalPrice();
  this.calcTotalGuests();

  if (!this.tickets[ticket_id][access_type][ticket_type].total) {
    this.removeTicket(ticket_id, access_type, ticket_type);
  }

  $(document).trigger({
    type      : 'removeTicketGuest',
    ticketId  : ticket_id,
    accessType: access_type,
    ticketType: ticket_type
  });
};

/************* HOTELS *************/

/**
 * Calculate Cart total price.
 * @returns {number}
 */
Cart.prototype.calcHotelTotalPrice = function () {

  var totalPrice = 0;

  for (var hotel_id in this.hotels) {
    if (this.hotels.hasOwnProperty(hotel_id)) {
      totalPrice += this.hotels[hotel_id].totalPrice;
    }
  }

  this.hotelTotalPrice = totalPrice;

  //console.log('TOTAL PRICE', this.hotelTotalPrice);

  $(document).trigger({
    type: 'updateHotelTotalPrice'
  });

  return totalPrice;
};

/**
 * Erase all hotels in Cart
 */
Cart.prototype.eraseCartHotels = function () {
  for (var hotel_id in this.hotels) {
    if (this.hotels.hasOwnProperty(hotel_id)) {
      for (var room_id in this.hotels[hotel_id].rooms) {
        if (this.hotels[hotel_id].rooms.hasOwnProperty(room_id)) {
          for (var index in this.hotels[hotel_id].rooms[room_id].list) {
            if (this.hotels[hotel_id].rooms[room_id].list.hasOwnProperty(index)) {

              delete this.hotels[hotel_id].rooms[room_id][index];
            }
          }

          delete this.hotels[hotel_id].rooms[room_id];
        }
      }

      delete this.hotels[hotel_id];
    }
  }
};

/**
 *
 * @returns {number}
 */
Cart.prototype.calcHotelTotalGuests = function () {

  var hotelTotalGuests = 0;
  var hotelTotalRooms  = 0;

  for (var hotel_id in this.hotels) {
    if (this.hotels.hasOwnProperty(hotel_id)) {
      for (var room_id in this.hotels[hotel_id].rooms) {
        if (this.hotels[hotel_id].rooms.hasOwnProperty(room_id)) {
          for (var index in this.hotels[hotel_id].rooms[room_id].list) {
            if (this.hotels[hotel_id].rooms[room_id].list.hasOwnProperty(index)) {

              var room = this.hotels[hotel_id].rooms[room_id].list[index];
              hotelTotalRooms++;
              hotelTotalGuests += room.adult + room.child;
            }
          }
        }
      }
    }
  }

  this.hotelTotalRooms  = hotelTotalRooms;
  this.hotelTotalGuests = hotelTotalGuests;

  $(document).trigger({
    type: 'updateTotalHotels'
  });

  return hotelTotalGuests;
};

/**
 * Calc price of hotel booking
 * @param hotel_id         id of the hotel, by ex. lapita
 * @returns {number}
 */
Cart.prototype.calcHotelPrice = function (hotel_id) {

  var hotel      = this.hotels[hotel_id];
  var hotelprice = 0;

  for (var room_id in hotel.rooms) {
    if (hotel.rooms.hasOwnProperty(room_id)) {
      for (var index in hotel.rooms[room_id].list) {
        if (hotel.rooms[room_id].list.hasOwnProperty(index)) {
          //var room_info = hotelsList.hotels[hotel_id].rooms[room_id];
          /*var room      = hotel.rooms[room_id].list[index];
           var roomPrice = 0;

           roomPrice += (room.adult * hotel.rooms[room_id].price);
           roomPrice += (room.child * hotel.rooms[room_id].price);

           hotelprice += roomPrice;*/

          var roomPrice = Number(this.hotels[hotel_id].rooms[room_id].list[index].price.Amount);

          hotelprice += roomPrice;

          this.hotels[hotel_id].rooms[room_id].list[index].roomPrice = roomPrice;

          //console.log("Price of the room", roomPrice);
        }
      }
    }
  }

  this.hotels[hotel_id].totalPrice = hotelprice;

  //console.log('Hotel Price', this.hotels[hotel_id].totalPrice);

  $(document).trigger({
    type   : 'updateHotelPrice',
    hotelId: hotel_id
  });

  return roomPrice;
};

/**
 * Create hotel if not exists
 * @returns {bool}
 * @param hotel_id
 */
Cart.prototype.hotelExistsOrCreate = function (hotel_id) {

  if (!this.hotels[hotel_id]) {
    this.hotels[hotel_id] = {};
  }
  if (!this.hotels[hotel_id].rooms) {
    this.hotels[hotel_id].rooms = {};

    return false;
  }

  return true;
};
;/**/
var cart = new Cart();

(function ($) {
  Drupal.behaviors.cart_summary = {
    attach: function (context, settings) {
      /**
       * Max guests length
       * (3 = XXX)
       * @type {number}
       */
      var MAX_GUESTS_LENGTH = 3;

      /**
       * Helper if dt
       */
      Handlebars.registerHelper('ifEquals', function (a, b, options) {
        if (a === b) {
          return options.fn(this);
        }

        return options.inverse(this);
      });

      /**
       * Format Guests Plural
       */
      Handlebars.registerHelper("formatPlural", function (guestsNumber, single, plural) {
        return Drupal.formatPlural(guestsNumber, Drupal.t(single), Drupal.t(plural));
      });

      /**
       * Handler loop for Tickets
       */
      /*Handlebars.registerHelper('each', function (context, options) {
       var ret = "";

       if (options.data) {
       data = Handlebars.createFrame(options.data);
       }

       for (var index in context) {
       if (context.hasOwnProperty(index)) {

       if (data) {
       data.index = index;
       }

       ret = ret + options.fn(context[index], {data: data});
       }
       }

       return ret;
       });*/

      /**
       * Change access labels
       */
      Handlebars.registerHelper("printAccess", function (access_type) {
        var access_string = "";

        switch (access_type) {
          case "1DAY":
            access_string = Drupal.t("1 day ticket");
            break;
          case "2DAY":
            access_string = Drupal.t("2 days ticket");
            break;
          case "3DAY":
            access_string = Drupal.t("3 days ticket");
            break;
          case "4DAY":
            access_string = Drupal.t("4 days ticket");
            break;
          case "ANNPASS":
            access_string = Drupal.t("Annual pass");
            break;
        }

        return access_string;
      });

      /**
       * TODO : Rimuovere dal JSON quando l'API ci dara gli link esatti
       */
      /*var base_path_dprs        = settings.dpr_booking_flow.base_path + settings.dpr_booking_flow.dprs_theme;
       var base_path_dprs_common = settings.dpr_booking_flow.base_path + settings.dpr_booking_flow.dprs_common_theme;*/

      $(document).on('hideSummary', hideSummaryFrame);
      $(document).on('showSummary', showSummaryFrame);
      $(document).on('showScrollSummary', showScrollSummaryFrame);
      $(document).on('generateStaticCart', generateStaticCartSummary);
      $(document).on('generateCart', generateAndShowCart);

      function generateStaticCartSummary() {
        generateCartSummary();

        if (!jQuery.isEmptyObject(cart.tickets) || !jQuery.isEmptyObject(cart.hotels) || !jQuery.isEmptyObject(cart.package)) {
          showSummaryFrame();
        }
      }

      function generateAndShowCart() {
        console.log("generate and scroll");
        generateCartSummary();
        showScrollSummaryFrame();
      }

      /**
       * Event on Ticket Cart
       */
      /*$(document).on('updateGuests', updateGuests);
       $(document).on('addTicketGuest', updateGuests);*/
      $(document).on('updateDate', updateDate);
      $(document).on('updateTicketPrice', updatePrice);
      $(document).on('updateTotalPrice', updateTotalPrice);
      $(document).on('updateTicketsSummary', setSummaryFromList);
      $(document).on('updateTicketsTotalPrice', updateTicketsTotalPrice);
      $(document).on('updateTicketsGuests', updateTicketGuests);

      /**
       * Update ui guests
       * by ex., editing guests number on editing summary
       * @param event
       */
      function updateTicketGuests(event) {
        $('#cart_tickets_number').text(cart.ticketsTotalGuests);
      }

      /**
       * Event on Hotel Cart
       */
      $(document).on('bookHotel', setHotelSummaryFromList);
      $(document).on('updateHotelsSummary', setHotelSummaryFromList);
      $(document).on('updateHotelTotalPrice', updateHotelTotalPrice);

      /**
       * Event on Hotel-Ticket Cart
       */
      $(document).on('addHotelTicketGuest', updateHotelTicketSummary);
      $(document).on('updateHotelsTicketsSummary', updateHotelTicketSummary);
      $(document).on('bookPackage', updateHotelTicketSummary);

      /*function updateHotelTicketSummary() {

       if (cart.type == cartPackageType) {
       if (Object.keys(cart.tickets).length) {
       setSummaryFromList();
       }
       if (Object.keys(cart.hotels).length) {
       setHotelSummaryFromList();
       }

       updatePackageTotalPrice();
       }
       }*/

      function updateHotelTicketSummary() {

        setSummaryFromList();
        setHotelSummaryFromList();

        if (cart.type == cartPackageType) {
          updatePackageTotalPrice();
        }
      }

      /**
       * Generate Cart
       */
      function generateCartSummary() {

        if (!jQuery.isEmptyObject(cart.tickets) || !jQuery.isEmptyObject(cart.hotels) || !jQuery.isEmptyObject(cart.package)) {
          var cartSummaryData = {
            CartTitle : "",
            bookingUrl: ""
          };

          if (cart.type == cartTicketsType) {
            //console.log("Set Tickets");

            cartSummaryData = {
              CartTitle : Drupal.t("Tickets selected"),
              bookingUrl:  '/booking/ticket-booking-summary-flow',
            };
          }

          //console.log("Generate CART", cartSummaryData);

          var source   = $("#cart-summary-template").html();
          var template = Handlebars.compile(source);
          var cartHtml = template(cartSummaryData);

          $('#tickets_selected_frame').html(cartHtml);

          updateHotelTicketSummary();

          $(document).trigger('cartSummaryGenerated')
        }
        else {
          hideSummaryFrame();
        }
      }

      /**
       * Load Cart from cookie
       * if it exists set summary
       */
      cart.load('cartSummary');

      /**
       * Update Cart summary
       */
      /*generateCartSummary();

       if (!jQuery.isEmptyObject(cart.tickets) || !jQuery.isEmptyObject(cart.hotels) || !jQuery.isEmptyObject(cart.package)) {
       console.log("SHOW SUMMARY START");
       showSummaryFrame();
       }*/

      //updateHotelTicketSummary();

      /**
       * Update ui guests
       * by ex., editing guests number on editing summary
       * @param event
       */
      /*function updateGuests(event) {

       console.log("IN UPD GUESTS");

       var ticket_id   = event.ticketId,
       access_type = event.accessType,
       ticket_type = event.ticketType;

       var guest_title  = "";
       var ticket_total = 0;
       var $form        = $('#' + ticket_id + '_' + access_type + '_' + ticket_type);

       if ($form.length && ticket_type != $form.data('type')) {
       return;
       }

       if (cart.tickets[ticket_id]) {
       if (cart.tickets[ticket_id][access_type]) {
       if (cart.tickets[ticket_id][access_type][ticket_type]) {
       var adult_number  = cart.tickets[ticket_id][access_type][ticket_type].adult.number;
       var child_number  = cart.tickets[ticket_id][access_type][ticket_type].child.number;
       var senior_number = cart.tickets[ticket_id][access_type][ticket_type].senior.number;
       ticket_total      = cart.tickets[ticket_id][access_type][ticket_type].total;
       }
       }
       }

       //console.log('NUMBERS', adult_number, child_number, senior_number);

       if (adult_number > 0) {
       guest_title += adult_number + ' ';
       guest_title += Drupal.formatPlural(adult_number, Drupal.t('Adult'), Drupal.t('Adults'));
       }
       if (child_number > 0) {
       if (guest_title != "") {
       guest_title += ' - ';
       }
       guest_title += child_number + ' ';
       guest_title += Drupal.formatPlural(child_number, Drupal.t('Child'), Drupal.t('Children'));
       }
       if (senior_number > 0) {
       if (guest_title != "") {
       guest_title += ' - ';
       }
       guest_title += senior_number + ' ';
       guest_title += Drupal.formatPlural(senior_number, Drupal.t('Senior'), Drupal.t('Seniors'));
       }

       //console.log('TITLE', guest_title, $form);

       var dropdown_title = $form.find('.dropdown-title');

       console.log("TICKET TOTAL IN UPD", ticket_total);

       if (ticket_total == 0) {
       guest_title = Drupal.t('Guests');

       if (dropdown_title.hasClass('has-guests')) {
       dropdown_title.removeClass('has-guests');
       }
       }
       else {
       if (!dropdown_title.hasClass('has-guests')) {
       dropdown_title.addClass('has-guests');
       }
       }

       dropdown_title.text(guest_title);

       console.log("TOTAL GUESTS", cart.totalGuests);

       if (!cart.totalGuests) {
       hideSummaryFrame();
       }
       else {
       setSummaryFromList();
       showScrollSummaryFrame();
       }
       }*/

      /**
       * Update ui with local date
       * @param event
       */
      function updateDate(event) {

        var ticket_id   = event.ticketId,
            access_type = event.accessType,
            ticket_type = event.ticketType;

        var booking_row = $('#' + ticket_id + '_' + access_type + '_' + ticket_type);

        booking_row.find('.datepicker').val(cart.tickets[ticket_id][access_type][ticket_type].date1);
        booking_row.find('.date-start-ticket').val(cart.tickets[ticket_id][access_type][ticket_type].raw_date1);
      }

      /**
       * Update ui with local price
       * @param event
       */
      function updatePrice(event) {

        var ticket_id   = event.ticketId,
            access_type = event.accessType,
            ticket_type = event.ticketType;

        var booking_row = $('#' + ticket_id + '_' + access_type + '_' + ticket_type);
        var $price      = booking_row.find('.price');

        //$price.text(cart.tickets[ticket_id][access_type][ticket_type].ticketPrice.format(2, 3, ',', '.'));
      }

      /**
       * Update cart ui with total Price
       * @param event
       */
      function updatePackageTotalPrice(event) {
        var $total_price = $('.tsf_price');
        $total_price.text(cart.package.packageTotalPrice.format(2, 3, ',', '.'));
      }

      /**
       * Update cart ui with total Price
       * @param event
       */
      function updateTotalPrice(event) {
        var $total_price = $('.tsf_price');
        $total_price.text(cart.totalPrice.format(2, 3, ',', '.'));
      }

      /**
       * Update cart ui with total Price
       * @param event
       */
      function updateTicketsTotalPrice(event) {
        var $total_price = $('.tsf_price');
        $total_price.text(cart.ticketsTotalPrice.format(2, 3, ',', '.'));
      }

      /**
       * Update cart ui with total Price
       * @param event
       */
      function updateHotelTotalPrice(event) {
        var $total_price = $('.tsf_price');
        $total_price.text(cart.hotelTotalPrice.format(2, 3, ',', '.'));
      }

      /**
       * Show tikets/guests summary
       */
      function showScrollSummaryFrame() {

        //console.log("NOW SHOW SUMMARY AND SCROLL");

        var ticket_frame = $('#tickets_selected_frame');

        if (ticket_frame.css('display') == 'none') {
          TweenLite.to(ticket_frame, 0.25, {
            autoAlpha : 1,
            display   : 'table',
            onComplete: function () {
              TweenLite.to(window, .5, {scrollTo: {y: ticket_frame.offset().top}, ease: Power2.easeOut});
            }
          });
        }
        else {
          TweenLite.to(window, .5, {scrollTo: {y: ticket_frame.offset().top}, ease: Power2.easeOut});
        }
      }

      /**
       * Show tikets/guests summary
       */
      function showSummaryFrame() {

        var ticket_frame = $('#tickets_selected_frame');

        if (ticket_frame.css('display') == 'none') {
          TweenLite.to(ticket_frame, 0.25, {
            autoAlpha: 1,
            display  : 'table',
          });
        }
      }

      /**
       * Hide tikets/guests summary
       */
      function hideSummaryFrame() {

        var ticket_frame    = $('#tickets_selected_frame');
        var booking_summary = ticket_frame.find('#booking_summary');

        //console.log("HIDE SUMMARY");

        TweenLite.to(ticket_frame, 0.25, {
          autoAlpha : 0,
          display   : 'none',
          onComplete: function () {
            booking_summary.find('.container').empty();
          }
        });
      }

      /**
       * Create summary from cart
       */
      function setSummaryFromList() {

        var ticket_frame    = $('#tickets_selected_frame');
        var booking_summary = ticket_frame.find('#booking_summary');

        booking_summary.find('.container-tickets').html('');

        if (Object.keys(cart.tickets).length) {
          // Ticket summary
          for (var ticket_id in cart.tickets) {
            if (cart.tickets.hasOwnProperty(ticket_id)) {
              for (var access_type in cart.tickets[ticket_id]) {
                if (cart.tickets[ticket_id].hasOwnProperty(access_type)) {
                  for (var ticket_type in cart.tickets[ticket_id][access_type]) {
                    if (cart.tickets[ticket_id][access_type].hasOwnProperty(ticket_type)) {

                      var cart_ticket = cart.tickets[ticket_id][access_type][ticket_type];

                      var allowRemove = cart.type != cartPackageType;

                      var row_data = {
                        allowRemove: allowRemove,
                        ticket_id  : ticket_id,
                        access_type: access_type,
                        ticket_type: ticket_type,
                        title      : cart_ticket.ticketName.replace('+', ' '),
                        description: cart_ticket.ticketEventName.replace('+', ' '),
                        adult      : cart_ticket.adult.number,
                        senior     : cart_ticket.senior.number,
                        child      : cart_ticket.child.number,
                        raw_date   : cart_ticket.raw_date1,
                        ticket_date: cart_ticket.date1
                      };

                      //console.log("ROW HERE", booking_summary);

                      var source      = $("#cart-row-template").html();
                      var template    = Handlebars.compile(source);
                      var cartRowHtml = template(row_data);

                      booking_summary.find('.container-tickets').append(cartRowHtml);
                    }
                  }
                }
              }
            }
          }

          cart.calcTotalGuests();
          cart.calcTicketsTotalPrice();
          cart.calcTotalPrice();

          //console.log("CART IN TICKETS", cart);

          /*if (cart.totalGuests) {
           if (ticket_frame.css('display') == 'none') {
           TweenLite.to(ticket_frame, 0.25, {
           autoAlpha : 1,
           display   : 'table',
           onComplete: function () {
           TweenLite.to(window, .5, {scrollTo: {y: ticket_frame.offset().top}, ease: Power2.easeOut});
           }
           });
           }
           else {
           TweenLite.to(window, .5, {scrollTo: {y: ticket_frame.offset().top}, ease: Power2.easeOut});
           }
           }*/
        }
      }

      /**
       * Create hotels summary from cart
       */
      function setHotelSummaryFromList() {

        var ticket_frame    = $('#tickets_selected_frame');
        var booking_summary = ticket_frame.find('#booking_summary');

        if (Object.keys(cart.hotels).length) {
          console.log("Hotal summary");

          booking_summary.find('.container-hotel').html('');

          // Hotel Summary
          for (var hotel_id in cart.hotels) {
            if (cart.hotels.hasOwnProperty(hotel_id)) {

              var source      = $("#cart-row-hotel-template").html();
              var template    = Handlebars.compile(source);
              var cartRowHtml = template(cart.hotels[hotel_id]);

              booking_summary.find('.container-hotel').append(cartRowHtml);

              cart.calcHotelPrice(hotel_id);
            }
          }

          cart.calcTotalGuests();
          cart.calcHotelTotalPrice();
          cart.calcTotalPrice();

          console.log("CART IN HOTEL", cart);

          /*if (cart.hotelTotalGuests) {

           console.log("Framcart", ticket_frame_cart);

           if (ticket_frame.css('display') == 'none') {
           TweenLite.to(ticket_frame, 0.25, {
           autoAlpha : 1,
           display   : 'table',
           onComplete: function () {
           TweenLite.to(window, .5, {scrollTo: {y: ticket_frame.offset().top}, ease: Power2.easeOut});
           }
           });
           }
           else {
           TweenLite.to(window, .5, {scrollTo: {y: ticket_frame.offset().top}, ease: Power2.easeOut});
           }
           }*/
        }

      }

      /**
       * edit ticket on summary
       */
      /*$(document).on('click', '.booking-edit', function (e) {

       e.preventDefault();

       var booking_container = $(this).closest('.booking-row-container');
       var form_id           = booking_container.data('form');
       var booking_row       = $('#' + form_id);
       var ticket_id         = booking_container.data('ticket');
       var access_type       = booking_container.data('access');
       var ticket_type       = booking_container.data('type');
       var $booking_numbers  = booking_container.find('.booking-number');
       var $booking_date     = booking_container.find('.booking-date');
       var raw_date          = $booking_date.data('raw_date');

       $(this).toggleClass('inedit');

       if ($booking_numbers.prop('contenteditable') == 'true') {
       $booking_numbers.prop('contenteditable', 'false');

       $.each($booking_numbers, function () {
       var edit_value      = $(this).text();
       var section         = $(this).data('section');
       var booking_section = $(this).closest('.booking-' + section);
       var guests_value    = parseInt(edit_value);

       if (isNaN(parseInt(guests_value)) || typeof parseInt(guests_value) == 'undefined' || parseInt(guests_value) <= 0) {
       cart.removeMultipleTicketGuests(ticket_id, access_type, ticket_type, section);
       booking_section.remove();
       }
       else {
       console.log('Set Guest', guests_value);
       cart.setTicketGuests(guests_value, ticket_id, access_type, ticket_type, section);
       }
       });

       cart.register('cartSummary', '/');

       if (!cart.tickets[ticket_id][access_type][ticket_type].total) {
       booking_container.remove();
       }

       if (!cart.totalGuests) {
       hideSummaryFrame();
       }
       }
       else {
       $booking_numbers.prop('contenteditable', 'true');

       if ($booking_numbers[0] !== document.activeElement) {
       $booking_numbers[0].focus();
       }

       $booking_numbers.on('input', function (e) {

       // TODO: reinstall after resolving caret position problem on changin text.
       /!*var edit_value = $(this).text().replace(/\D/g,'');

       $(this).text(edit_value);*!/

       var edit_value = $(this).text();

       var len = edit_value.length;

       if (len >= MAX_GUESTS_LENGTH) {
       $(this).keypress(function () {
       return false;
       });

       $(this).text(edit_value.trimToLength(3));
       }
       else {
       $(this).off('keypress');
       }

       if (!Number.isInteger(parseInt(edit_value)) || isNaN(parseInt(edit_value))) {
       $(this).text('');
       }

       //console.log('Before Guest', cart.totalGuests);

       var section      = $(this).data('section');
       var guests_value = parseInt(edit_value);

       if (isNaN(parseInt(guests_value))
       || typeof parseInt(guests_value) == 'undefined'
       || parseInt(guests_value) <= 0) {
       //cart.removeMultipleGuests(ticket_id, section);
       $(this).text('');
       }
       /!*else {

       console.log('Set Guest', guests_value);
       cart.setTicketGuests(guests_value, ticket_id, section);
       }*!/

       //console.log('After Guest', cart.totalGuests, cart.tickets[ticket_id].total);

       /!*if (!cart.tickets[ticket_id].total) {
       booking_container.remove();
       }

       if (!cart.totalGuests) {
       hideSummaryFrame();
       }*!/

       /!*$(document).trigger({
       type: 'updateGuests',
       ticketId: ticket_id
       });*!/
       });
       }
       });*/

      /**
       * remove row booking
       */
      $(document).on('click', '.booking-remove', function (e) {

        e.preventDefault();

        var section            = $(this).data('section');
        var booking_section    = $(this).closest('.booking-' + section);
        var booking_container  = booking_section.closest('.booking-row-container');
        var tickets_container  = booking_container.closest('.container-tickets');
        var ticket_id          = booking_container.data('ticket');
        var access_type        = booking_container.data('access');
        var ticket_type        = booking_container.data('type');
        var booking_row_values = $(this).closest('.booking-row-values');

        booking_section.remove();

        var row_num = booking_row_values.find('div').length;

        if (row_num) {
          while ((3 - row_num) > 0) {
            booking_row_values.append('<div></div>');
            row_num++;
          }
        }

        cart.removeMultipleTicketGuests(ticket_id, access_type, ticket_type, section);

        if (!cart.ticketsTotalGuests) {
          tickets_container.remove();
        }

        /*if (!cart.totalGuests) {
         hideSummaryFrame();
         }*/

        $(document).trigger('cartTicketRemoved', [ticket_id, access_type, ticket_type, section]);

        cart.register('cartSummary', '/');
      });

      /**
       * booking summary open close
       */
      $(document).on('click', '.ticket-summary-opener', function (e) {

        e.preventDefault();

        console.log('SUMMARY OPENER');

        var ticket_frame    = $('#tickets_selected_frame');
        var booking_summary = ticket_frame.find('#booking_summary');

        if (booking_summary.css('display') == 'none') {
          TweenLite.to(booking_summary, 0.25, {autoAlpha: 1, display: 'block'});

          $(this).find('.glyphicon').switchClass('glyphicon-menu-down', 'glyphicon-menu-up');
        }
        else {
          TweenLite.to(booking_summary, 0.25, {autoAlpha: 0, display: 'none'});

          $(this).find('.glyphicon').switchClass('glyphicon-menu-up', 'glyphicon-menu-down');
        }
      });
    }
  }
})(jQuery);
;/**/
var ticketsFilters = {};

(function ($) {

  Drupal.behaviors.dpr_booking_flow = {
    attach: function (context, settings) {

      // Empty Cart on changing Cart type
      if (cart.type != cartTicketsType) {
        //console.log("Empty cart for new search");
        cart = new Cart();
        cart.register('cartSummary', '/');
      }

      $(document).trigger({
        type: 'generateStaticCart'
      });

      var $total_price = $('.tsf_price');
      var searching    = false;

      /**
       * Update cart ui with total Price
       * @param event
       */
      function updateTicketsTotalPrice(event) {
        $total_price.text(cart.ticketsTotalPrice.format(2, 3, ',', '.'));
      }

      /**
       * Update ui summary
       * by ex., editing guests number on editing summary
       * @param e
       */
      function updateTicketsCartSummary(e) {

        //console.log("UPDATE AFTER REMOVE", cart, Object.keys(cart.tickets).length);

        totalTicket = cart.ticketsTotalGuests;

        if (Object.keys(cart.tickets).length) {
          $(document).trigger('updateTicketsSummary');
        }
        else {
          cart.calcTotalPrice();
          updateTicketsTotalPrice(e);
          $(document).trigger('hideSummary');
        }
      }

      $(document).on('removeTicketGuest', updateTicketsCartSummary);

      var start         = true;
      var $modalInfo    = $('#modalInfosMessages');
      var $modalConfirm = $('#modalInfosMessagesConfirm');
      var totalTicket   = 0;

      //console.log("TICKETS FILTERS", ticketsFilters);

      // List of in/active filters
      if (!Object.getOwnPropertyNames(ticketsFilters).length) {
        ticketsFilters = {
          parks: {
            DPR: true,
            MG  : false,
            BP  : false,
            LEGOLAND: false
          },
          type : "ANNPASS",
          start: true
        };
      }

      /**
       * More info on ticket
       */
      $(document).on('click', '.more_info_opener', function (e) {
        e.preventDefault();

        var info_frame = $(this).closest('.ticket-row').find('.more_info_frame');

        if (info_frame.css('display') == 'none') {
          TweenLite.to(info_frame, 0.25, {autoAlpha: 1, display: 'inline-block'});
        }
        else {
          TweenLite.to(info_frame, 0.25, {autoAlpha: 0, display: 'none'});
        }
      });

      /**
       * Filter search submit
       */
      $('#tickets_filters_btn').off('click').on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();

        if (searching) {
          return false;
        }

        var searchParks = "";

        var first = true;

        for (var filter_park in ticketsFilters.parks) {
          if (ticketsFilters.parks.hasOwnProperty(filter_park)) {
            if (ticketsFilters.parks[filter_park]) {
              if (!first) {
                searchParks += "|";
              }

              //console.log("FILTER ", filter_park);

              searchParks += filter_park;

              first = false;
            }
          }
        }

        var searchData = {
          parks     : searchParks,
          ticketType: ticketsFilters.type
        };

        //console.log("Request search", searchData);

        jsFunctions.toggleSpinner();

        searching = true;

        getTickets(searchData);
      });

      /**
       * Parks filter
       */
      $(document).on('click', '.menu-custom-dpr a', function (e) {
        e.preventDefault();
        e.stopPropagation();

        var filter = $(this).data('filter');

        if ($('.menu-custom-dpr').find('a').filter('.active').length > 1 || !$(this).hasClass('active')) {
          $(this).toggleClass('active');
          ticketsFilters.parks[filter] = !ticketsFilters.parks[filter];
        }
        var itemToMatch = $(this).data('filter');
        $('.menu-custom-dpr>a').each(function(key, val){
          if($(this).data('filter')!=itemToMatch){
            $(this).removeClass('active');
            ticketsFilters.parks[$(this).data('filter')] = false;
          }
        });
        $('#tickets_filters_btn').click();
      });

      /**
       * Access Filter
       */
      $('.tickets-filters-list').find('a').off('click').click(function (e) {
        e.preventDefault();
        e.stopPropagation();

        ticketsFilters.type = $(this).data('type');

        $('.tickets-filters-list').find('a').removeClass('selected');
        $(this).addClass('selected');
      });

      $(document).on('click', '.frame-opener', function (e) {
        e.preventDefault();
        e.stopPropagation();

        var frame = $(this).data('frame');

        if ($(frame).css('display') == 'none') {
          TweenLite.to($(frame), 0.3, {autoAlpha: 1, display: 'block'});
          $(this).addClass('open');
        }
        else {
          TweenLite.to($(frame), 0.3, {autoAlpha: 0, display: 'none'});
          $(this).removeClass('open');
        }
      });

      /**
       * Change access labels
       */
      Handlebars.registerHelper("printAccess", function (access_type) {
        var access_string = "";

        switch (access_type) {
          case "1DAY":
            access_string = Drupal.t("1 day ticket");
            break;
          case "2DAY":
            access_string = Drupal.t("2 days ticket");
            break;
          case "3DAY":
            access_string = Drupal.t("3 days ticket");
            break;
          case "4DAY":
            access_string = Drupal.t("4 days ticket");
            break;
          case "ANNPASS":
            access_string = Drupal.t("Annual pass");
            break;
        }

        return access_string;
      });

      /**
       * Format Price
       */
      Handlebars.registerHelper("formatPrice", function (price) {
        return price ? price.format(2, 3, ',', '.') : 0;
      });

      /**
       * Set lang for datepicker from Drupal Language
       * @type {string}
       */
      var regional = "";
      if (settings.dpr_booking_flow.current_lang != "en") {
        regional = settings.dpr_booking_flow.current_lang;
      }

      $.datepicker.setDefaults($.datepicker.regional[regional]);

      /**
       * datepicket jQuery Ui
       */
      function setDateTicketsPicker() {

        var $datepickers = $(".datepicker", context);

        $datepickers.datepicker({
          minDate       : "+1d",
          dateFormat    : "d M yy",
          constrainInput: true,
          beforeShow    : function (textbox, instance) {

            $(this).parent().append(instance.dpDiv);
            instance.dpDiv.hide();

            instance.dpDiv.css('border-color', '#EEEEEE');
            instance.input.toggleClass('toggled');
          },
          onClose       : function (textbox, instance) {
            instance.input.toggleClass('toggled');
          },
          onSelect      : function (dateText) {

            $(this).prop('value', dateText);
            $(this).valid();

            var form        = $(this).closest('form');
            var access_type = form.data('access');

            form.find('#date_start_ticket').val(dateText);

            var d1   = new Date(dateText);
            var d2   = new Date();
            d2.setTime(d1.getTime() + 86400000);
            var d3   = new Date();
            d3.setTime(d2.getTime() + 86400000);
            var d4   = new Date();
            d4.setTime(d3.getTime() + 86400000);
            var d360 = new Date();
            d360.setFullYear(d1.getFullYear() + 1);

            switch (access_type) {
              case '1DAY':
                $(this).val(dateText.toUpperCase());
                break;
              case '2DAY':
                $(this).val(dateText.toUpperCase() + ' - ' + $.datepicker.formatDate('d M yy', d2).toUpperCase());
                break;
              case '3DAY':
                $(this).val(dateText.toUpperCase() + ' - ' + $.datepicker.formatDate('d M yy', d2).toUpperCase() + ' - ' + $.datepicker.formatDate('d M yy', d3).toUpperCase());
                break;
              case '4DAY':
                $(this).val(dateText.toUpperCase() + ' - ' + $.datepicker.formatDate('d M yy', d2).toUpperCase() + ' - ' + $.datepicker.formatDate('d M yy', d3).toUpperCase() + ' - ' + $.datepicker.formatDate('d M yy', d4).toUpperCase());
                break;
              case 'ANNPASS':
                $(this).val(dateText.toUpperCase() + ' - ' + $.datepicker.formatDate('d M yy', d360).toUpperCase());
                break;
              default:
            }

            /*$.validator.setDefaults({
             debug  : true,
             success: "valid"
             });*/
          }
        });

        // TODO: Mask for English, mask for Arabic
        /*$datepickers.mask('00-00-0000', {
         translation: {
         '0': {pattern: /\d/},
         'A': {pattern: /[A-Z]/},
         },
         onComplete: function(cep) {
         var d = new Date(cep);
         console.log("MASK", cep, d);
         $(this).val($.datepicker.formatDate('d M yy', d).toUpperCase());
         }
         });*/
      }

      /**
       * Update print Guests
       * @param ticketId
       * @param accessType
       * @param ticketType
       */
      function UpdateGuestDropdownTitle(ticketId, accessType, ticketType) {

        var $form = $('#' + ticketId + '_' + accessType + '_' + ticketType);

        var input_adult_number = parseInt($form.find('#input_adult').val());
        var adult_number       = isNumeric(input_adult_number) ? input_adult_number : 0;

        var input_child_number = parseInt($form.find('#input_child').val());
        var child_number       = isNumeric(input_child_number) ? input_child_number : 0;

        var input_senior_number = parseInt($form.find('#input_senior').val());
        var senior_number       = isNumeric(input_senior_number) ? input_senior_number : 0;

        var ticket_total = adult_number + child_number + senior_number;

        var guest_title = "";

        if (adult_number > 0) {
          guest_title += adult_number + ' ';
          guest_title += Drupal.formatPlural(adult_number, Drupal.t('Guest'), Drupal.t('Guests'));
        }
        if (child_number > 0) {
          if (guest_title != "") {
            guest_title += ' - ';
          }
          guest_title += child_number + ' ';
          guest_title += Drupal.formatPlural(child_number, Drupal.t('Child'), Drupal.t('Children'));
        }
        if (senior_number > 0) {
          if (guest_title != "") {
            guest_title += ' - ';
          }
          guest_title += senior_number + ' ';
          guest_title += Drupal.formatPlural(senior_number, Drupal.t('Senior'), Drupal.t('Seniors'));
        }

        var dropdown_title = $form.find('.dropdown-title');

        if (ticket_total == 0) {
          guest_title = Drupal.t('Guests');

          if (dropdown_title.hasClass('has-guests')) {
            dropdown_title.removeClass('has-guests');
          }
        }
        else {
          if (!dropdown_title.hasClass('has-guests')) {
            dropdown_title.addClass('has-guests');
          }
        }

        dropdown_title.text(guest_title);
      }

      /**
       * Set Plus/Minus Guests
       */
      function addEventOnGuests() {
        var guest_list = $('.guest_form');

        if (guest_list.length > 0) {
          $.each(guest_list, function () {

            var booking_row = $(this);

            /*booking_row.validate({
             rules         : {
             date_booking: {
             required: true,
             dateFormat: true
             }
             },
             messages      : {
             date_booking: Drupal.t("Please enter a valid date.")
             },
             errorPlacement: function (error, element) {
             console.log(error);
             $modalInfo.find('.messageTxt').html(error);
             $modalInfo.modal("show");
             //$(element[0].form).prepend(error);
             }
             });*/

            /**
             * Substract Guest
             */
            $(this).find('.minus').click(function (e) {

              e.preventDefault();
              e.stopPropagation();

              var guest_type = $(this).data('guest');

              var ticketId   = booking_row.data('ticket');
              var accessType = booking_row.data('access');
              var ticketType = booking_row.data('type');

              var guest_number = parseInt(booking_row.find('#input_' + guest_type).val());

              var input_child_number = parseInt(booking_row.find('#input_child').val());
              var child_number       = isNumeric(input_child_number) ? input_child_number : 0;

              if (guest_type == 'adult' && guest_number == MIN_ADULT_GUEST && child_number > 0) {
                $modalInfo.find('.messageTxt').text(Drupal.t(messages['errorMinAdultGuest']));
                $modalInfo.modal("show");
                return false;
              }

              if (guest_number > 0) {

                guest_number--;
                totalTicket--;

                booking_row.find('.' + guest_type + '_row').find('.guest-number').html(guest_number);
                booking_row.find('#input_' + guest_type).val(guest_number);

                UpdateGuestDropdownTitle(ticketId, accessType, ticketType);
              }
            });

            /**
             * Add guest
             */
            $(this).find('.plus').click(function (e) {

              e.preventDefault();
              e.stopPropagation();

              var guest_type = $(this).data('guest');

              var ticketId   = booking_row.data('ticket');
              var accessType = booking_row.data('access');
              var ticketType = booking_row.data('type');

              if (totalTicket >= MAX_TICKETS) {

                var args = {
                  '@nbTickets': MAX_TICKETS
                };

                $modalInfo.find('.messageTxt').text(Drupal.t(messages['errorQtyTickets'], args));
                $modalInfo.modal("show");
                return false;
              }

              var guest_number = parseInt(booking_row.find('#input_' + guest_type).val());
              guest_number++;
              totalTicket++;

              booking_row.find('.' + guest_type + '_row').find('.guest-number').html(guest_number);
              booking_row.find('#input_' + guest_type).val(guest_number);

              if (guest_type == 'child') {
                var input_adult_number = parseInt(booking_row.find('#input_adult').val());
                var adult_number       = isNumeric(input_adult_number) ? input_adult_number : 0;

                if (!adult_number) {
                  adult_number = MIN_ADULT_GUEST;
                  totalTicket++;
                  booking_row.find('.adult_row').find('.guest-number').html(adult_number);
                  booking_row.find('#input_adult').val(adult_number);
                }
              }

              UpdateGuestDropdownTitle(ticketId, accessType, ticketType);
            });

          });
        }
      }

      /**
       * Generate Tickets from JSON
       */
      function generateTickets() {

        var source     = $("#ticket-template").html();
        var template   = Handlebars.compile(source);
        var ticketHtml = template(ticketsParks);

        $('#bf_tickets_container').html(ticketHtml);

        var $bookinItems = $('.booking-item');
        var totalItems   = $bookinItems.length;

        $.each($bookinItems, function (index) {
          if (!(index % 2)) {
            $(this).addClass('odd');
          }
          else {
            $(this).addClass('even');
          }

          if (index === (totalItems - 1)) {

          }
        });

        if ($bookinItems.last().hasClass('odd')) {
          $('.full-container', context).css('background-color', '#F0F2F5');
        }
        else {
          $('.full-container', context).css('background-color', '#FFFFFF');
        }

        setDateTicketsPicker();

        addEventOnGuests();

        setTicketsFromCart();
      }

      /**
       * Get Hotel + Experience from Json
       * @param searchData
       */
      function getTickets(searchData) {

        // jsFunctions.toggleSpinner();

        $.getJSON(settings.dpr_booking_flow.base_url + "/booking_searchExperience", searchData,
          function (data) {

            jsFunctions.toggleSpinner();

            ticketsParks.tickets = data.data || [];

            //console.log("TICKETS LIST", ticketsParks);

            generateTickets();

            //$(document).trigger('generateStaticCart');
            cart.load();
            cart.calcTicketsTotalPrice();

            jsFunctions.hotelInit();

           /* TweenLite.to($('.tickets-filters'), 0.3, {
              autoAlpha : 0,
              display   : 'none',
              onComplete: function () {
                TweenLite.to(window, 0.5, {
                  scrollTo: {y: $('#bf_tickets_container').offset().top},
                  ease    : Power1.easeInOut
                });

                //$('.ticket-filter-frame').find('.bf_header').find('.frame-opener').removeClass('open');
              }
            });*/

            start     = false;
            searching = false;
          }
        ).fail(function (response, status, xhr) {
            // jsFunctions.toggleSpinner();

            $modalInfo.find('.messageTxt').text(Drupal.t(messages['errorSearch']));
            $modalInfo.modal("show");
            console.log("error", response, status, xhr);

            searching = false;
          }
        );
      }

      /**
       * Submit Ticket with guests and date
       */
      $(document).on('click', '.submit-ticket .action-btn', function (e) {

        e.preventDefault();

        var $form         = $(this.form);
        var ticket_id     = $form.data('ticket');
        var access_type   = $form.data('access');
        var ticket_type   = $form.data('type');
        var ticket_index  = $form.data('ticketindex');
        var $date_booking = $form.find("input[name=date_booking]");

        //console.log('ACTION TYPE', ticket_type);

        var input_adult_number = parseInt($form.find('#input_adult').val());
        var adult_number       = isNumeric(input_adult_number) ? input_adult_number : 0;

        var input_child_number = parseInt($form.find('#input_child').val());
        var child_number       = isNumeric(input_child_number) ? input_child_number : 0;

        var input_senior_number = parseInt($form.find('#input_senior').val());
        var senior_number       = isNumeric(input_senior_number) ? input_senior_number : 0;

        var click_totalTicket = (adult_number+child_number+senior_number);

        //console.log('NUMBERS', adult_number, child_number, senior_number);
        //console.log('totalTicket:', totalTicket);
        //console.log('click_totalTicket', click_totalTicket);

        if (!totalTicket || click_totalTicket<1) {
          $modalInfo.find('.messageTxt').text(Drupal.t(messages['errorNullGuests']));
          $modalInfo.modal("show");
          return false;
        }

        //console.log("TICKET TOTAL", totalTicket);

        if (ticket_type == 'dt') {

          var dateBooking = $form.find('#date_start_ticket').val();

          if (!isDate(dateBooking)) {
            $modalInfo.find('.messageTxt').text(Drupal.t(messages['errorNullDate']));
            $modalInfo.modal("show");
            return false;
          }

          /*if (!$date_booking.valid()) {
           return false;
           }
           */
        }

        var $booking_row = $('#br_' + ticket_id + '_' + access_type + '_' + ticket_type);
        var raw_date     = "",
            ticket_date  = "",
            cart_date    = "";

        if (ticket_type == 'dt') {
          raw_date = $form.find('#date_start_ticket').val();
          //cart_date = $.datepicker.formatDate('dd mm yy', new Date(raw_date));

          if (raw_date) {
            var d1   = new Date(raw_date);
            var d2   = new Date();
            d2.setTime(d1.getTime() + 86400000);
            var d3   = new Date();
            d3.setTime(d2.getTime() + 86400000);
            var d4   = new Date();
            d4.setTime(d3.getTime() + 86400000);
            var d360 = new Date();
            d360.setFullYear(d1.getFullYear() + 1);

            //console.log(access_type);

            switch (access_type) {
              case '1DAY':
                ticket_date = raw_date.toUpperCase();
                break;
              case '2DAY':
                ticket_date = raw_date.toUpperCase() + ' - ' + $.datepicker.formatDate('d M yy', d2).toUpperCase();
                break;
              case '3DAY':
                ticket_date = raw_date.toUpperCase() + ' - ' + $.datepicker.formatDate('d M yy', d3).toUpperCase();
                break;
              case '4DAY':
                ticket_date = raw_date.toUpperCase() + ' - ' + $.datepicker.formatDate('d M yy', d4).toUpperCase();
                break;
              case 'ANNPASS':
                ticket_date = raw_date.toUpperCase() + ' - ' + $.datepicker.formatDate('d M yy', d360).toUpperCase();
                break;
              default:
                ticket_date = raw_date.toUpperCase();
            }
          }
        }

        var ticket = ticketsParks.tickets[ticket_index];

        moment.locale('en');
        //var eng_datein = moment(d).format('DD MM YYYY');

        ticket.adult     = adult_number;
        ticket.child     = child_number;
        ticket.senior    = senior_number;
        ticket.date      = ticket_date;
        ticket.raw_date  = moment(new Date(raw_date)).format('DD MM YYYY');
        ticket.cart_date = raw_date;
        //ticket.raw_date  = raw_date;

        //console.log("TICKET TO ADD", ticket);

        //jsFunctions.toggleSpinner();

        addTicketOnSubmit(ticket_id, access_type, ticket_type, ticket);
      });

      function addTicketOnSubmit(ticket_id, access_type, ticket_type, ticket) {

        if (cart.type != cartTicketsType) {
          cart = new Cart();
        }

        cart.type = cartTicketsType;
        /*console.log("CARTYPE NOW", cart.type);
         console.log("IN API CART", data);*/

        cart.addTicketGuest(ticket_id, access_type, ticket_type, ticket);

        //console.log('ADD/UP TICKET', cart.tickets[ticket_id][access_type][ticket_type], ticket_id, access_type, ticket_type);

        // REGISTER CART
        cart.register('cartSummary', '/');

        $(document).trigger({
          type: 'generateCart'
        });

        /*$.getJSON(settings.dpr_booking_flow.base_url + "/booking_chooseEventForm", ticket,
          function (data) {

            jsFunctions.toggleSpinner();
            //console.log("CARTYPE", cart.type);
            if (cart.type != cartTicketsType) {
              cart = new Cart();
            }

            cart.type = cartTicketsType;
            /!*console.log("CARTYPE NOW", cart.type);
             console.log("IN API CART", data);*!/

            cart.addTicketGuest(ticket_id, access_type, ticket_type, ticket);

            //console.log('ADD/UP TICKET', cart.tickets[ticket_id][access_type][ticket_type], ticket_id, access_type, ticket_type);

            // REGISTER CART
            cart.register('cartSummary', '/');

            $(document).trigger({
              type: 'generateCart'
            });
          }
        ).fail(function (response, status, xhr) {

            jsFunctions.toggleSpinner();

            $modalInfo.find('.messageTxt').text(Drupal.t(messages['errorInsertCart']));
            $modalInfo.modal("show");
            console.log("error", response, status, xhr);
          }
        );*/
      }

      $(document).on('cartTicketRemoved', function(e, ticket_id, access_type, ticket_type, guest_type) {

        var guest_number = 0;
        var $form = $('#' + ticket_id + '_' + access_type + '_' + ticket_type);
        $form.find('#input_' + guest_type).val(guest_number);
        $form.find('.' + guest_type + '_row').find('.guest-number').html(guest_number);

        UpdateGuestDropdownTitle(ticket_id, access_type, ticket_type);
      });

      function setTicketsFromCart() {

        var booking_row   = "";
        var adult_number  = 0,
            child_number  = 0,
            senior_number = 0;

        totalTicket = 0;

        if (Object.keys(cart.tickets).length) {
          // Ticket summary
          for (var ticket_id in cart.tickets) {
            if (cart.tickets.hasOwnProperty(ticket_id)) {
              for (var access_type in cart.tickets[ticket_id]) {
                if (cart.tickets[ticket_id].hasOwnProperty(access_type)) {
                  for (var ticket_type in cart.tickets[ticket_id][access_type]) {
                    if (cart.tickets[ticket_id][access_type].hasOwnProperty(ticket_type)) {
                      // Here set Ticket

                      booking_row   = $('form[id="' + ticket_id + '_' + access_type + '_' + ticket_type +  '"]');

                      adult_number  = cart.tickets[ticket_id][access_type][ticket_type].adult.number || 0;
                      child_number  = cart.tickets[ticket_id][access_type][ticket_type].child.number || 0;
                      senior_number = cart.tickets[ticket_id][access_type][ticket_type].senior.number || 0;

                      booking_row.find('.adult_row').find('.guest-number').html(adult_number);
                      booking_row.find('#input_adult').val(adult_number);

                      booking_row.find('.child_row').find('.guest-number').html(child_number);
                      booking_row.find('#input_child').val(child_number);

                      booking_row.find('.senior_row').find('.guest-number').html(senior_number);
                      booking_row.find('#input_senior').val(senior_number);

                      if (ticket_type == 'dt') {

                        var raw_date = cart.tickets[ticket_id][access_type][ticket_type].raw_date1;
                        var cart_date = cart.tickets[ticket_id][access_type][ticket_type].raw_date2;
                        var d = new Date(raw_date);

                        booking_row.find(".datepicker").datepicker('setDate', d);
                        booking_row.find(".datepicker").val(cart.tickets[ticket_id][access_type][ticket_type].date1);
                        booking_row.find('#date_start_ticket').val(cart_date);
                      }

                      totalTicket += adult_number + child_number + senior_number;

                      UpdateGuestDropdownTitle(ticket_id, access_type, ticket_type);
                    }
                  }
                }
              }
            }
          }
        }
      }

      /**
       * Create tickets from Type and Park
       */
      function initParksTickets() {

        var searchParks = "";

        var first = true;

        for (var filter_park in ticketsFilters.parks) {
          if (ticketsFilters.parks.hasOwnProperty(filter_park)) {
            if (ticketsFilters.parks[filter_park]) {
              if (!first) {
                searchParks += "|";
              }

              //console.log("FILTER ", filter_park);

              searchParks += filter_park;

              first = false;
            }
          }
        }

        var searchData = {
          parks     : searchParks,
          ticketType: ticketsFilters.type
        };

        //console.log("Request search", searchData);

        getTickets(searchData);

        /**
         * Switch from Dated to Not Dated Ticket
         */
        /*$('.dated-undated-block').find('a').click(function (e) {
         e.preventDefault();
         e.stopPropagation();

         var $bookingItem = $(this).closest('.booking-item');
         var ticket_id    = $bookingItem.data('ticket');
         var access_type  = $bookingItem.data('access');
         var ticket_type  = $(this).data('type');

         var form = $('#' + ticket_id + '_' + access_type + '_' + ticket_type);

         var formsItem = $('form[id^="' + ticket_id + '_' + access_type + '_"]');
         formsItem.removeClass('selected');

         if (!form.hasClass('selected')) {
         form.addClass('selected');
         }

         $bookingItem.find('.dated-undated-block').find('a').removeClass('selected');
         $(this).addClass('selected');

         switch (ticket_type) {
         case 'ndt':
         form.find('.datepicker').closest('.ticket_form').css('display', 'none');
         form.find('label.error').css('display', 'none');
         form.find('.action-btn').attr('formnovalidate', '');
         break;
         case 'dt':
         form.find('.datepicker').closest('.ticket_form').css('display', 'block');
         if (form.find('label.error').length) {
         form.find('label.error').css('display', 'inline-block');
         }
         form.find('.action-btn').removeAttr('formnovalidate');
         break;
         }

         /!*$(document).trigger({
         type: 'updateGuests',
         ticketId: ticket_id,
         accessType: access_type,
         ticketType: ticket_type
         });*!/
         });*/

        // Initialize results by triggering search at start
        //$('#tickets_filters_btn').trigger('click');
      }

      if (ticketsFilters.start) {
        jsFunctions.toggleSpinner();
        initParksTickets();
      }
    }
  }

})(jQuery);;/**/
(function ($) {

  Drupal.behaviors.menu_custom_dprs = {
    attach: function(context, settings) {

      /* MULTI-FILTER */

      /*$(document).on('click', '.menu-custom-dpr a', function (e) {
        e.preventDefault();

        var filter = $(this).data('filter');

        console.log(filter);

        $(this).toggleClass('active');

        var filter_blocks = $('.booking-items-block.' + filter);

        filter_blocks.toggleClass('active');

        if (filter_blocks.hasClass('active')) {

          $('#bf_tickets_container').prepend(filter_blocks);

          $('.booking-items-block.' + filter + ':not(:first-child)').remove();

          filter_blocks = $('.booking-items-block.' + filter);

          TweenLite.to(filter_blocks, 0.3, {autoAlpha: 1, display:'block'});
        }
        else {
          TweenLite.to(filter_blocks, 0.3, {autoAlpha: 0, display: 'none'});
        }

      });*/

      // SINGLE FILTER

      /*$(document).on('click', '.menu-custom-dpr a', function (e) {
        e.preventDefault();

        var filter = $(this).data('filter');

        console.log(filter);

        console.log(this);

        $('.menu-custom-dpr a').removeClass('active');

        if (!$(this).hasClass('active')) {
         $(this).addClass('active');
        }

        var filter_blocks = $('.booking-items-block');

        filter_blocks.removeClass('active');

        filter_blocks = $('.booking-items-block.' + filter);

        filter_blocks.addClass('active');

        filter_blocks = $('.booking-items-block:not(.active)');

        TweenLite.to(filter_blocks, 0.25, {autoAlpha: 0, display: 'none'});

        filter_blocks = $('.booking-items-block.active');

        TweenLite.to(filter_blocks, 0.25, {autoAlpha: 1, display:'block'});

        $('.more_info_frame').css('opacity', 0).css('display', 'none');

      });*/
    }
  }

})(jQuery);;/**/
