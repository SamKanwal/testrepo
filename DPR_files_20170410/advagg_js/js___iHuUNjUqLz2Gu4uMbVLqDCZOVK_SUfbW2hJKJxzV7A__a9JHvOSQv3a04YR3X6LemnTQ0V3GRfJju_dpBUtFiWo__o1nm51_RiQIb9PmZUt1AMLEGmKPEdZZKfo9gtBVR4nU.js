var wallet=function($){return{init:function(jsonData,error){this.errors=error,this.setAmount(this.options.setBtn,this.options.walletName),this.insertItems(this.options.rechargeBtn),this.saveModel(jsonData),this.initMoneyTransfer(),jsFunctions.activateSelect2(),cart=new CartNew;var $wrapper;$wrapper=$(".wallet-wrapper"),0==Object.keys(this.Model).length&&($wrapper.find(".zero-found").show().end().find("#money-transfer-modal").remove(),console.log(jsonData)),$wrapper.on("keydown",".qty",function(e){var c,rx,key;if(key=e.keyCode,rx=new RegExp("[0-9,.]"),c=String.fromCharCode(key),!rx.test(c)&&8!=key&&46!=key)return!1}),$(".wallet-item .qty").on("blur",function(){var curr;curr=parseInt($(this).val()),curr>wallet.options.plus?$(this).siblings(".qtyminus").removeClass("disabled"):$(this).siblings(".qtyminus").addClass("disabled"),curr<wallet.options.max?$(this).siblings(".qtyplus").removeClass("disabled"):$(this).siblings(".qtyplus").addClass("disabled"),curr<wallet.options.plus?$(this).val(wallet.options.plus):curr>wallet.options.max&&$(this).val(wallet.options.max)})},options:{rechargeBtn:".recharge-cta",plus:50,max:1e4,setBtn:".qtyminus,.qtyplus",walletName:"wallet-name"},settings:{ref:"wallets"},items:{},Model:{},moneyTransfer:"#money-transfer-modal",requestUri:"",setAmount:function(btn){$(btn).on("click",function(e){var form,qty,val,curr;e.preventDefault(),btn=$(this),form=btn.parents("form"),qty=form.find(".qty"),val=$(this).hasClass("qtyminus")?"-":"+",curr=parseInt(qty[0].value)||0,qty[0].value=curr>0?curr:0,"-"==val?curr>0&&(curr-wallet.options.plus<2*wallet.options.plus?(qty[0].value=wallet.options.plus,btn.addClass("disabled")):(qty[0].value=curr-wallet.options.plus,btn.removeClass("disabled"),btn.siblings().removeClass("disabled"))):"+"==val&&(curr+wallet.options.plus<wallet.options.max?(qty[0].value=curr+wallet.options.plus,btn.siblings().removeClass("disabled"),btn.removeClass("disabled")):(qty[0].value=wallet.options.max,btn.addClass("disabled"))),qty.trigger("change")})},saveModel:function(json){var parsed,ret;try{ret={},parsed=JSON.parse(json),parsed=parsed.data.data.wallets,parsed.forEach(function(i){var t,mc;t=i.tickets.pop(),mc=t.mediaCode,delete i.tickets,delete i.portfolioId,i.mediaCode=mc,ret[mc]=i}),parsed=ret}catch(e){parsed={}}this.Model=parsed},insertItems:function(el){var that;that=this,$(el).on("click",function(){var id,amount,$this,$qty,cache,data,name,currValue;$this=$(this),$qty=$this.parents("form").find(".qty"),name=$this.parents("form").data("wallet-name"),data={},id=$this.closest(".wallet-item").data("id"),amount=$qty.val(),currValue=that.Model[id].balanceList[0].balance,currValue+parseInt(amount)<=wallet.options.max?parseInt(amount)>=wallet.options.plus&&(cache=$.extend({price:amount,name:name,qty:1},that.Model[id]),data[id]=cache,cart.add(data,that.settings),$qty.val(0)):$("#walletAlert").find(".modal-body .other").text(that.errors.max_amount).end().modal("show")})},currency:function(c){this.settings.currency=c||"AED"},initMoneyTransfer:function(){var $modal,that;$modal=$(this.moneyTransfer),that=this,$modal.on("change","select",function(){var $this,val,$others,id,limit,cache;$this=$(this),val=$this.val(),$others=$modal.find("select").not($this),$others.filter(function(){return $(this).val()===val}).val(""),$this.filter('[name="MediaCodeFrom"]').length?(id=$this.val(),""!=id&&(cache=that.Model[id].balanceList,limit=cache[cache.length-1].balance||0,$modal.find(".qty").each(function(){$(this).val()>limit&&$(this).val(limit)}),$this.closest(".form-group").removeClass("has-error"))):$this.closest(".form-group").addClass("has-error"),jsFunctions.activateSelect2()}),$modal.on("change",".qty",function(){var $this,curr,id,limit,cache;$this=$(this),curr=$this.val()||"0",curr=curr>0?curr:"0",curr=curr.replace(/[^0-9,\.]/,"")||0,$this.val(curr),id=$this.closest("form").find('select[name="MediaCodeFrom"]').val(),""!=id&&(cache=that.Model[id].balanceList,limit=cache[cache.length-1].balance||0,curr>limit&&$this.val(limit))}),$modal.on("click","[type='submit']",function(e){var $this,$form;e.preventDefault(),$this=$(this),$form=$this.closest("form"),$form.find(".qty").val()>0?($form.find(".qty").closest(".form-group").removeClass("has-error"),$form.submit()):$form.find(".qty").closest(".form-group").addClass("has-error")}),$modal.find("form").on("submit",function(e){var $this,$form,data,reduced,callback,destAmount;e.preventDefault(),$this=$(this),$form=$this.closest("form"),data=$form.serializeArray(),reduced={},data.reduce(function(prev,curr){return prev[curr.name]=curr.value,prev},reduced),callback=function(response){if(jsFunctions.toggleSpinner(),"success"==response.status)$form[0].reset(),$modal.modal("toggle"),$("#walletSuccess").find(".pnr").html(response.data.pnr).end().on("hidden.bs.modal",function(){location.reload()}).modal("show");else{var errorText=Drupal.t(wallet.getError(response.errorCode,response.errorMessage));$("#walletAlert").find(".modal-body .other").text(errorText).end().modal("show")}},destAmount="undefined"!=typeof wallet.Model[reduced.MediaCodeTo]?wallet.Model[reduced.MediaCodeTo].balanceList[0].balance:0,parseInt(reduced.Amount)+parseInt(destAmount)<=wallet.options.max?(jsFunctions.toggleSpinner(),$.ajax({type:"POST",url:that.requestUri,data:reduced,success:callback})):$("#walletAlert").find(".modal-body .other").text(Drupal.t("You can't charge more than ")+wallet.options.max+" "+Drupal.t("AED")).end().modal("show")})},setReqUri:function(uri){/http+|www+/gim.test(uri)?console.log("ERROR: You cannot set an external URI for security reason. Please relative path instead."):this.requestUri=uri},getError:function(key,mess){var dictionary,dictionary,_that=this,_mess=mess.match(/[^[\]]+(?=])/g)?mess.match(/[^[\]]+(?=])/g):_that.errors.generic_err;return dictionary={"DPR_01.001.010":_that.errors.invalid_mediacode,"DPR_04.001.005":_that.errors.max_amount,"DPR_04.000.002":_that.errors.same_wallet},"undefined"!=typeof dictionary[key]?dictionary[key]:_mess}}}(jQuery);;/**/
/**
 * @file
 * bootstrap.js
 *
 * Provides general enhancements and fixes to Bootstrap's JS files.
 */

var Drupal = Drupal || {};

(function($, Drupal){
  "use strict";

  Drupal.behaviors.bootstrap = {
    attach: function(context) {
      // Provide some Bootstrap tab/Drupal integration.
      $(context).find('.tabbable').once('bootstrap-tabs', function () {
        var $wrapper = $(this);
        var $tabs = $wrapper.find('.nav-tabs');
        var $content = $wrapper.find('.tab-content');
        var borderRadius = parseInt($content.css('borderBottomRightRadius'), 10);
        var bootstrapTabResize = function() {
          if ($wrapper.hasClass('tabs-left') || $wrapper.hasClass('tabs-right')) {
            $content.css('min-height', $tabs.outerHeight());
          }
        };
        // Add min-height on content for left and right tabs.
        bootstrapTabResize();
        // Detect tab switch.
        if ($wrapper.hasClass('tabs-left') || $wrapper.hasClass('tabs-right')) {
          $tabs.on('shown.bs.tab', 'a[data-toggle="tab"]', function (e) {
            bootstrapTabResize();
            if ($wrapper.hasClass('tabs-left')) {
              if ($(e.target).parent().is(':first-child')) {
                $content.css('borderTopLeftRadius', '0');
              }
              else {
                $content.css('borderTopLeftRadius', borderRadius + 'px');
              }
            }
            else {
              if ($(e.target).parent().is(':first-child')) {
                $content.css('borderTopRightRadius', '0');
              }
              else {
                $content.css('borderTopRightRadius', borderRadius + 'px');
              }
            }
          });
        }
      });
    }
  };

  /**
   * Behavior for .
   */
  Drupal.behaviors.bootstrapFormHasError = {
    attach: function (context, settings) {
      if (settings.bootstrap && settings.bootstrap.formHasError) {
        var $context = $(context);
        $context.find('.form-item.has-error:not(.form-type-password.has-feedback)').once('error', function () {
          var $formItem = $(this);
          var $input = $formItem.find(':input');
          $input.on('keyup focus blur', function () {
            var value = $input.val() || false;
            $formItem[value ? 'removeClass' : 'addClass']('has-error');
            $input[value ? 'removeClass' : 'addClass']('error');
          });
        });
      }
    }
  };

  /**
   * Bootstrap Popovers.
   */
  Drupal.behaviors.bootstrapPopovers = {
    attach: function (context, settings) {
      if (settings.bootstrap && settings.bootstrap.popoverEnabled) {
        var $currentPopover = $();
        if (settings.bootstrap.popoverOptions.triggerAutoclose) {
          $(document).on('click', function (e) {
            if ($currentPopover.length && !$(e.target).is('[data-toggle=popover]') && $(e.target).parents('.popover.in').length === 0) {
              $currentPopover.popover('hide');
              $currentPopover = $();
            }
          });
        }
        var elements = $(context).find('[data-toggle=popover]').toArray();
        for (var i = 0; i < elements.length; i++) {
          var $element = $(elements[i]);
          var options = $.extend({}, settings.bootstrap.popoverOptions, $element.data());
          if (!options.content) {
            options.content = function () {
              var target = $(this).data('target');
              return target && $(target) && $(target).length && $(target).clone().removeClass('element-invisible').wrap('<div/>').parent()[$(this).data('bs.popover').options.html ? 'html' : 'text']() || '';
            }
          }
          $element.popover(options).on('click', function (e) {
            e.preventDefault();
          });
          if (settings.bootstrap.popoverOptions.triggerAutoclose) {
            $element.on('show.bs.popover', function () {
              if ($currentPopover.length) {
                $currentPopover.popover('hide');
              }
              $currentPopover = $(this);
            });
          }
        }
      }
    }
  };

  /**
   * Bootstrap Tooltips.
   */
  Drupal.behaviors.bootstrapTooltips = {
    attach: function (context, settings) {
      if (settings.bootstrap && settings.bootstrap.tooltipEnabled) {
        var elements = $(context).find('[data-toggle="tooltip"]').toArray();
        for (var i = 0; i < elements.length; i++) {
          var $element = $(elements[i]);
          var options = $.extend({}, settings.bootstrap.tooltipOptions, $element.data());
          $element.tooltip(options);
        }
      }
    }
  };

  /**
   * Anchor fixes.
   */
  var $scrollableElement = $();
  Drupal.behaviors.bootstrapAnchors = {
    attach: function(context, settings) {
      var i, elements = ['html', 'body'];
      if (!$scrollableElement.length) {
        for (i = 0; i < elements.length; i++) {
          var $element = $(elements[i]);
          if ($element.scrollTop() > 0) {
            $scrollableElement = $element;
            break;
          }
          else {
            $element.scrollTop(1);
            if ($element.scrollTop() > 0) {
              $element.scrollTop(0);
              $scrollableElement = $element;
              break;
            }
          }
        }
      }
      if (!settings.bootstrap || settings.bootstrap.anchorsFix !== '1') {
        return;
      }
      var anchors = $(context).find('a').toArray();
      for (i = 0; i < anchors.length; i++) {
        if (!anchors[i].scrollTo) {
          this.bootstrapAnchor(anchors[i]);
        }
      }
      $scrollableElement.once('bootstrap-anchors', function () {
        $scrollableElement.on('click.bootstrap-anchors', 'a[href*="#"]:not([data-toggle],[data-target],[data-slide])', function(e) {
          if (this.scrollTo) {
            this.scrollTo(e);
          }
        });
      });
    },
    bootstrapAnchor: function (element) {
      element.validAnchor = element.nodeName === 'A' && (location.hostname === element.hostname || !element.hostname) && (element.hash.replace(/#/,'').length > 0);
      element.scrollTo = function(event) {
        var attr = 'id';
        var $target = $(element.hash);
        // Check for anchors that use the name attribute instead.
        if (!$target.length) {
          attr = 'name';
          $target = $('[name="' + element.hash.replace('#', '') + '"]');
        }
        // Immediately stop if no anchors are found.
        if (!this.validAnchor && !$target.length) {
          return;
        }
        // Anchor is valid, continue if there is an offset.
        var offset = $target.offset().top - parseInt($scrollableElement.css('paddingTop'), 10) - parseInt($scrollableElement.css('marginTop'), 10);
        if (offset > 0) {
          if (event) {
            event.preventDefault();
          }
          var $fakeAnchor = $('<div/>')
            .addClass('element-invisible')
            .attr(attr, $target.attr(attr))
            .css({
              position: 'absolute',
              top: offset + 'px',
              zIndex: -1000
            })
            .appendTo($scrollableElement);
          $target.removeAttr(attr);
          var complete = function () {
            location.hash = element.hash;
            $fakeAnchor.remove();
            $target.attr(attr, element.hash.replace('#', ''));
          };
          if (Drupal.settings.bootstrap.anchorsSmoothScrolling) {
            $scrollableElement.animate({ scrollTop: offset, avoidTransforms: true }, 400, complete);
          }
          else {
            $scrollableElement.scrollTop(offset);
            complete();
          }
        }
      };
    }
  };

  /**
   * Tabledrag theming elements.
   */
  Drupal.theme.tableDragChangedMarker = function () {
    return '<span class="tabledrag-changed glyphicon glyphicon-warning-sign text-warning"></span>';
  };

  Drupal.theme.tableDragChangedWarning = function () {
    return '<div class="tabledrag-changed-warning alert alert-warning messages warning">' + Drupal.theme('tableDragChangedMarker') + ' ' + Drupal.t('Changes made in this table will not be saved until the form is submitted.') + '</div>';
  };

})(jQuery, Drupal);
;/**/
