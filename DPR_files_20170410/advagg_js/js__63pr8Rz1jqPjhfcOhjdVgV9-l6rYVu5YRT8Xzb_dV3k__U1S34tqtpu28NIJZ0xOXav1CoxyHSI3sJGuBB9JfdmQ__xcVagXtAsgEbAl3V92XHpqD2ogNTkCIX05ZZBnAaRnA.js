/**
* @file
* Javascript, modifications of DOM.
*
* Manipulates links to include jquery load funciton
*/

(function ($) {
  Drupal.behaviors.jquery_ajax_load = {
    attach: function (context, settings) {
      jQuery.ajaxSetup ({
      // Disable caching of AJAX responses
        cache: false
      });

      var trigger = Drupal.settings.jquery_ajax_load.trigger;
      var target = Drupal.settings.jquery_ajax_load.target;
      // Puede ser más de un valor, hay que usar foreach()
      $(trigger).once(function() {
        var html_string = $(this).attr( 'href' );
        // Hay que validar si la ruta trae la URL del sitio
        $(this).attr( 'href' , target );
        var data_target = $(this).attr( 'data-target' );
        if (typeof data_target === 'undefined' ) {
          data_target = target;
        }
        else {
          data_target = '#' + data_target;
        }
        $(this).click(function(evt) {
          evt.preventDefault();
          jquery_ajax_load_load($(this), data_target, html_string);
        });
      });
      $(trigger).removeClass(trigger);
    }
  };  

// Handles link calls
  function jquery_ajax_load_load(el, target, url) {
    var module_path = Drupal.settings.jquery_ajax_load.module_path;
    var toggle = Drupal.settings.jquery_ajax_load.toggle;
    var base_path = Drupal.settings.jquery_ajax_load.base_path;
    var animation = Drupal.settings.jquery_ajax_load.animation;
    if( toggle && $(el).hasClass( "jquery_ajax_load_open" ) ) {
      $(el).removeClass( "jquery_ajax_load_open" );
      if ( animation ) {
        $(target).hide('slow', function() {
          $(target).empty();
        });
      }
      else {
        $(target).empty();
      }
    }
    else {
      var loading_html = Drupal.t('Loading'); 
      loading_html += '... <img src="/';
      loading_html += module_path;
      loading_html += '/jquery_ajax_load_loading.gif">';
      $(target).html(loading_html);
      $(target).load(base_path + 'jquery_ajax_load/get' + url, function( response, status, xhr ) {
        if ( status == "error" ) {
          var msg = "Sorry but there was an error: ";
          $(target).html( msg + xhr.status + " " + xhr.statusText );
        }
        else {
          if ( animation ) {
            $(target).hide();
            $(target).show('slow')
          }
//        Drupal.attachBehaviors(target);
        }
      });
      $(el).addClass( "jquery_ajax_load_open" );
    }
  }
}(jQuery));
;/**/
(function($) {

    Drupal.behaviors.discover = {

        attach: function(context, settings) {

            var self = this;

            self.player = null;
            self.slider = null;

            $(window).load(function() {
                //$(".discover-slideshow").fitVids();
                // menu mobile
                window.addEventListener('popstate', function(event) {
                    if (event.state) {
                        console.log(event.state);
                        console.log(event);
                    }
                }, false);
                $('.discover-nav div.label').html($('.zactive div').html());

                var rtl_on = false;
                ($("html[lang='ar']").length) && (rtl_on = true);
                self.slider = $('.discover-slideshow').slick({
                    autoplay: true,
                    arrows: false,
                    dots: true,
                    rtl: rtl_on
                });

                if ($(window).width() < 768) {
                    $(".outer_map_cont").slideUp();
                }

                self._initYTAPi();                

                $('.discover-slideshow').on('afterChange', function(event, slick, currentSlide) {
                    if (!!self.player) {
                        if (!!self.player.stopVideo) {
                            self.player.stopVideo();
                        }
                    }
                });

                $(document).click(function(e){
                    e.stopPropagation();
                    var clickTarget = $(e.target);
                    if (e.target.id=='booking-time') {
                        $('.ui-hour-select').toggle();
                    } else if ( !clickTarget.hasClass('ui-hour-select') && (!clickTarget.closest('.ui-hour-select').length )){
                        $('.ui-hour-select').hide();
                    }
                });

                //START EDIT FOR 8748

                !function dataBinder(){
                    var $modal;

                    $modal = $('.booking-form-container');

                    $modal.on('show.bs.collapse', function (e) {
                        var rangeMin, rangeMax, data, hours, pm, am;

                        rangeMin = '';
                        rangeMax = '';
                        $modal.data({
                            rangeMin: rangeMin,
                            rangeMax: rangeMax
                        });

                        data = $('[data-target="#' + $(this).attr('id') + '"]').data();
                        rangeMin = data.rangeMin.split(':');
                        rangeMax = data.rangeMax.split(':');
                        if(rangeMin && rangeMax && rangeMin != rangeMax){

                            $(this).data({
                                rangeMin: rangeMin,
                                rangeMax: rangeMax
                            });

                            hours = rangeMin[0];
                            pm = hours > 12;
                            if(pm){
                                hours -= 12;
                                $('.ui-hour-select .merid.pm').trigger('click');
                                $('.ui-hour-select .merid.am').addClass('disabled');
                            }

                            am = rangeMax[0] <= 12;
                            if(am){
                                $('.ui-hour-select .merid.pm').addClass('disabled');
                            }

                            $('.ui-hour-select input#hours').val(hours);
                            $('.ui-hour-select input#mins').val(rangeMin[1]);
                            updateTime(hours, rangeMin[1]);
                        }

                    });

                }();

                function updateTime(hours, mins) {
                    if (mins == '0') {
                        mins = '00';
                    }
                    hours = parseInt(hours);
                    $('.ui-hour-select input#hours').val(hours);
                    $('.ui-hour-select input#mins').val(mins);
                    $('.ui-hour-select .value.hour div.value').html(hours < 10 ? '0' + hours : hours);
                    $('.ui-hour-select .value.min div.value').html(mins);
                }

                var hours = $('.ui-hour-select input#hours').val();
                var mins = $('.ui-hour-select input#mins').val();
                updateTime(hours, mins);

                $('.ui-hour-select .merid').click(function() {
                    var $modal,rangeMinH, mins, hours;

                    if (!$(this).hasClass('disabled') && !$(this).hasClass('selected')) {
                        $('.ui-hour-select .merid').removeClass('selected');
                        $(this).toggleClass('selected');

                        $modal = $(this).closest('.booking-form-container');

                        if($(this).hasClass('am')){
                            rangeMinH = parseInt($modal.data('rangeMin')[0]);
                            hours = rangeMinH > 12 ? rangeMinH - 12 : rangeMinH;
                            mins = parseInt($modal.data('rangeMin')[1]);
                        } else {
                            hours = 12;
                            mins = 0;
                        }


                        $('.ui-hour-select input#hours').val(hours);
                        $('.ui-hour-select input#mins').val(mins);
                        updateTime(hours < 10 ? '0' + hours : hours, mins < 10 ? '0' + mins : mins);
                    }
                });

                $('.ui-hour-select .mod').click(function() {
                    var $modal,rangeMinH, rangeMinM, rangeMaxH, rangeMaxM, conv;

                    $modal = $(this).closest('.booking-form-container');
                    rangeMinH = parseInt($modal.data('rangeMin')[0]);
                    rangeMinM = parseInt($modal.data('rangeMin')[1]);
                    rangeMaxH = parseInt($modal.data('rangeMax')[0]);
                    rangeMaxM = parseInt($modal.data('rangeMax')[1]);

                    conv = $('.ui-hour-select .merid.pm').hasClass('selected') ? 12 : 0;
                    hours = parseInt($('.ui-hour-select input#hours').val()) + conv;
                    mins = parseInt($('.ui-hour-select input#mins').val());

                    if ($(this).hasClass('plus')) {
                        if ($(this).hasClass('hour') && (hours - conv < 12 -1 && (!rangeMaxH || hours < rangeMaxH) || hours - conv == 12)) {
                            if($('.ui-hour-select .merid.pm').hasClass('selected') && hours - conv == 12){
                                hours = 12;
                            }
                            hours++;
                            if(!rangeMaxH || (hours == rangeMaxH && mins > rangeMaxM)){
                                mins = rangeMaxM;
                            }
                        } else if ($(this).hasClass('min') && mins < 45 && (!rangeMaxH || (hours == rangeMaxH && mins < rangeMaxM || hours < rangeMaxH || (hours == 24 && hours - conv < rangeMaxH)))) {
                            mins = parseInt(mins) + 15;
                        }
                    } else if ($(this).hasClass('minus')) {
                        if ($(this).hasClass('hour') && hours - conv > 0 && (!rangeMinH || hours > rangeMinH) && hours - conv < 12) {
                            hours--;
                            if(!rangeMinH || hours == rangeMinH && mins < rangeMinM){
                                mins = rangeMinM;
                            }
                            if(hours == 12){
                                hours = 24;
                            }
                        } else if ($(this).hasClass('min') && mins > 0 && (!rangeMinH || (hours == rangeMinH && mins > rangeMinM || hours > rangeMinH))) {
                            mins = parseInt(mins) - 15;
                        }
                    }
                    hours -= conv;
                    updateTime(hours < 10 ? '0' + hours : hours, mins < 10 ? '0' + mins : mins);
                });

                //END EDIT FOR 8748

                $('.ui-hour-select .action-btn').click(function() {
                    hours = $('.ui-hour-select input#hours').val();
                    mins = $('.ui-hour-select input#mins').val();
                    merid = $('.merid.selected').html();
                    $(this).closest(".form-group")
                      .removeClass("has-error")
                      .find("#booking-time")
                      .val(hours + ':' + mins + ' ' + merid);
                    $('.ui-hour-select').toggle();
                });
                /* End time selector */

                /* start bambining around */
                var $boutput = $('.bambinout');
                var inputOutput = $('[name="filter-height-hidden"]');

                function updateOutput(el, val) {
                    el.textContent = val;
                }

                function alienateKid(value) {

                    //var top = Math.max(Math.min((210-value)/2.25, _max), _min);
                    var top = (210 - value) / 2.25;
                    $('.skeletor .top').css({ top: top + 'px' });;
                }

                if (!!$('#bambinel').length) {
                    $('#bambinel').rangeslider({
                        polyfill: false,
                        onInit: function() {
                            updateOutput($boutput[0], this.value);
                            inputOutput.val(this.value);
                            alienateKid(this.value);
                        },
                        onSlide: function() {
                            updateOutput($boutput[0], this.value);
                            inputOutput.val(this.value);
                            alienateKid(this.value);
                        }
                    });
                }

                $('.height-filter .control').on('click', function(e) {
                    var value = $('#bambinel').val();

                    if ($(this).hasClass('minus')) {
                        $('#bambinel').val(parseInt($('#bambinel').val()) - 20).change();
                    } else {
                        $('#bambinel').val(parseInt($('#bambinel').val()) + 20).change();
                    }
                    //$('#bambinel').val(value + step);

                    //$('#bambinel').val(parseInt(value + step)).change();
                    //var value = $('[data-rangeslider]').value;
                    //$inputRange
                    value = $('#bambinel').val();
                    alienateKid(parseInt(value));
                });

                /* end bambino */

                $('#filters select').focus(function() {
                    $(this).parent().children('.select2-selection__rendered').focus();
                });
                if (!!$('.ionslider').length) {
                    var arr = [];
                    $(".filterables").each(function() {
                        arr.push($(this).data("price"));
                    });
                    var max = Math.max.apply(null, arr);
                    $("[name='filter-price-hidden']").val(0)
                    $('.ionslider').ionRangeSlider({
                        type: "single",
                        grid: false,
                        min: 0,
                        max: max,
                        from: max,
                        to: 800,
                        prefix: "AED ",
                        onStart: function() {
                            $('.irs-min').after('<span class="irs-teo-label left">min</span>');
                            $('.irs-max').after('<span class="irs-teo-label right">max</span>');
                        },
                        onChange: function(data) {
                            $("[name='filter-price-hidden']").val($("[name='filter-price']").val());
                        }
                    });
                }

                
                //
                $('.food-rating .value').on("click", function() {
                    $('#filter-rating').val(parseInt($(this).data("rating")));
                    $('.food-rating .value').removeClass('active');
                    var ind = $(this).index();
                    for (var i = 0; i < ind; i++) {
                        $('.food-rating .value').eq(i).addClass("active")
                    }
                    $("body").trigger({
                        type: "filter_changed"
                    });
                });
                $(document).on("click", '.form-group .ico-label', function() {
                    $(this).toggleClass('active');
                    var $checkbox = $(this).find(':checkbox');
                    $checkbox.attr('checked', !$checkbox.attr('checked'));

                    var obj = [];

                    $(this).closest("div")
                      .find(".ico-label")
                      .each(function() {
                          var name = $(this).find("label").text();
                          name = $(this).hasClass("active") ? name : "";
                          obj.push(name);
                      });

                    $(this).closest("div")
                      .find("[data-type='filter']")
                      .data("val", obj)
                      .val(obj)

                    $("body").trigger({
                        type: "filter_changed"
                    });
                });


                $(".map-info.hover-me").off("click")
                  .on("click", function() {
                      $(this).toggleClass("opened");
                      $(this).find(".hidden-row").animate({ width: 'toggle' }).css("position");
                  });

                //masonry and friends
                //fix masonry for tabletå
                
                if ($(window).width() > 550 && $(window).width() < 1024 && !!$(".booking-form-container").length) {
                    var counter = 0;
                    var bf_a = [];
                    $(".booking-form-container:even").each(function() {
                        bf_a.push($(this).clone());
                    });
                    $(".booking-form-container:even").remove();
                    if ($(".booking-form-container").length == bf_a.length) {
                        $(".booking-form-container").each(function() {
                            $(this).before(bf_a[counter]);
                            counter++;
                        });
                    } else {
                        $(".booking-form-container").each(function() {
                            $(this).before(bf_a[counter]);
                            counter++;
                        });
                        $(".masonry-container").append(bf_a[bf_a.length - 1]);
                    }

                    $(".booking-form").validator('destroy')
                    $(".booking-form").validator();

                }
                
                self.$masonry = $('.masonry-container').masonry({
                    itemSelector: '.masonry-item'
                });

                $(".datepicker").datepicker({
                    minDate: '+0d',
                    startDate: '+0d',
                    endDate: '+3m',
                    maxDate: '+3m',
                    dateFormat: "d M yy",
                    constrainInput: true,
                    autoclose: true,
                    beforeShow: function(textbox, instance) {
                        $(this).parent().append(instance.dpDiv);
                        instance.dpDiv.hide();
                        instance.dpDiv.css('border-color', '#EEEEEE');
                        instance.input.toggleClass('toggled');
                    }
                }, [0, 0]);
                // 
                $(".datepicker").eq(0).on("change", function() {
                    $(".datepicker").val($(this).val());
                    if (!!$(".node-type-events").length && !!$("html[lang='ar']").length) {
                        var d = self._formatDate($(this).val());
                        $(".datepicker").val(d).css("unicode-bidi", "bidi-override");
                    }
                })

                $('.booking-form-container').on('shown.bs.collapse', function() {
                    $(this).siblings().collapse('hide');
                    self.$masonry.masonry();
                });
                $('.booking-form-container').on('hidden.bs.collapse', function() {
                    self.$masonry.masonry();
                })

                // Detail Code
                var direzione = false;
                ($("html[lang='ar']").length) && (direzione = true);

                if (!!$('.gallery-container.details-page #carousel').length) {
                    $('.gallery-container.details-page #carousel').flexslider({
                        animation: "slide",
                        controlNav: true,
                        animationLoop: false,
                        slideshow: false,
                        itemWidth: 210,
                        itemMargin: 5,
                        asNavFor: '.gallery-container.details-page #slider',
                        // rtl          : direzione
                    });
                }
                if (!!$('.gallery-container.details-page #slider').length) {
                    $('.gallery-container.details-page #slider').flexslider({
                        animation: "slide",
                        controlNav: false,
                        animationLoop: false,
                        slideshow: false,
                        // rtl       : direzione,
                        sync: ".gallery-container.details-page #carousel",
                        start: function(slider) {
                            $('body').removeClass('loading');
                        }
                    });
                }


                //init differents pages bindings
                //RESTAURANTS
                if (!!$(".page-discover-motiongate-restaurants").length) {
                    self._discoverRestaurants();
                }

                if (!!$(".page-discover-motiongate-events").length) {
                    self._discoverEvents();
                }

                //init different func
                //FILTERS
                if (!!$("#filter-options").length) {
                    self._filters();
                }

                $('select').select2({
                    minimumResultsForSearch: Infinity
                });

            });

            self._bindings();

            $(".image-actions a:first-child").off("click")
              .on("click", function() {
                  if ($(window).width() < 1025) {
                      $(".outer_map_cont").slideDown();
                  }
                  var _thisData = $(this).attr("href").replace("#", "");

                  var target = $("[data-location='" + _thisData + "']");

                  if ($(window).width() < 1025) {
                      $("[data-location]").css("display", "none");
                      target.css("display", "block");
                  } else {
                      target.trigger("click");
                  }
                  if (!!$(".outer_map_cont").length) {
                      $('html, body').animate({
                          scrollTop: $(".outer_map_cont").offset().top
                      }, 1800);
                  }
              })

            $(document).on("click", ".location_header .right", function() {
                if ($(window).width() < 1025) {
                    $(".outer_map_cont").slideUp();
                    $(".mapplic-tooltip-close").trigger("click");
                }
                var data_ref = $(this).data("ref");
                $('html, body').animate({
                    scrollTop: $(".image-actions a[href='#" + data_ref + "']").closest(".masonry-item").offset().top - 50
                }, 1500, function() {
                    $(".mapplic-tooltip-close").trigger("click");
                });

            })

            $('#booking-date, #booking-time').on('keypress',function (e) {
                e.preventDefault();
                return false;
            })

        },
        _formatDate: function(date) {
            var arr_date = date.split(" ")
            if (date != "") {
                var monthNames = [
                    "January", "February", "March",
                    "April", "May", "June", "July",
                    "August", "September", "October",
                    "November", "December"
                ];

                var ar_monthNames = [
                    "يناير", "فبراير", "مارس",
                    "أبريل", "مايو", "يونيو",
                    "يوليو", "أغسطس", "سبتمبر",
                    "أكتوبر", "نوفمبر", "ديسمبر"
                ];

                var day = arr_date[0];
                var monthIndex = arr_date[1] - 1;
                var year = arr_date[2];
                var month = monthNames[monthIndex];
                if (!!jQuery("html[lang='ar']").length) {
                    var month = ar_monthNames[monthIndex];
                    var s = year + ' ' + month + ' ' + day;
                } else {
                    var s = month + ' ' + day + ' ' + year;
                }

                return s
            }

        },
        _initYTAPi: function() {
            var self = this;
            self.paused = 0;
            var tag = document.createElement('script');
            tag.src = "https://www.youtube.com/player_api";
            var firstScriptTag = document.getElementsByTagName('script')[0];
            firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
            var yt_id = $(".discover-slideshow .item.youtube [data-yt]").data("yt");
            var el = $('#video_' + yt_id);
            var sn = new Sniffer();

            if (sn.IsiPhone) {
                $(".reset-video").remove();
            }

            if (!!yt_id) {

                self.player = new YT.Player('video_' + yt_id, {
                    videoId: yt_id,
                    width: "100%",
                    height: "100%",
                    playerVars: {
                        autoplay: 0,
                        enablejsapi: 1,
                        modestbranding: 1,
                        rel: 0,
                        iv_load_policy: 3
                    },
                    events: {
                        'onStateChange': onPlayerStateChange
                    }
                });

            }

            function onPlayerStateChange(event) {
                if (event.data == 1) {
                    $(".reset-video").css("display", "inline");
                    if (!!self.slider) {
                        self.slider.slick('slickPause');;
                    }
                }
            }

            $(".reset-video").on("click", function() {
                self.player.stopVideo();
                $(".yt-wrap-container").css("visibility", "hidden");
                $(this).hide();
            });

            $("#play-video").on("click", function(e) {
                e.preventDefault();
                console.log("clicked");
                $(this).next(".yt-wrap-container").css("visibility", "visible");
                self.player.playVideo();
            });

        },
        _discoverRestaurants: function() {

            var addingTriangle = function(el, type) {
                var _type = type == "odd" ? "triangle_right" : "triangle_left";

                !el.hasClass(_type) ? el.addClass(_type) : "";
            }

            //bindings
            $(".action-btn[data-type='reserve_a_table']").off("click")
              .on("click", function() {
                  var ref = $(this).data("ref");
                  var el = $(this).closest(".masonry-item");
                  var top = $(".content-page.container").offset().top + 60;
                  var posRef = el.offset().top - top + el.height();
                  $(".booking-form-container").not(ref)
                    .slideUp(400);

                  $(ref).css({
                      top: posRef + "px",
                      left: 0,
                      "z-index": "999"
                  })
                    .slideToggle(800);
              });


            $(".filterables:even .form-opener").off("click")
              .on("click", function() {
                  if ($(window).width() > 767) {
                      var id = $(this).data("target");
                      addingTriangle($(id), "even");
                  }
              });

            $(".filterables:odd .form-opener").off("click")
              .on("click", function() {
                  if ($(window).width() < 1025 && $(window).width() > 767) {
                      var id = $(this).data("target");
                      addingTriangle($(id), "odd");
                  }
              });


            $(".booking-form-container").on("shown.bs.collapse", function() {
                var $this;
                $this = $(this);
                setTimeout(function () {
                    TweenLite.to($(window), .6, {scrollTo: {y: $this.offset().top}, ease: Power4.easeInOut});
                }, 650);

            });

            jsFunctions.populatePrefix();

            $("#mailModal [data-dismiss='modal']").on("click", function(){
                $(".booking-form-container.in").collapse("hide")
            });


        },
        _discoverEvents: function() {
            var addingTriangle = function(el, type) {
                var _type = type == "odd" ? "triangle_right" : "triangle_left";

                !el.hasClass(_type) ? el.addClass(_type) : "";
            }
            $(".outer-container:even .form-opener").off("click")
              .on("click", function() {
                  if ($(window).width() > 767) {
                      var id = $(this).data("target");
                      addingTriangle($(id), "even");
                  }
              });

            jsFunctions.populatePrefix();

            $(".outer-container:odd .form-opener").off("click")
              .on("click", function() {
                  if ($(window).width() > 767) {
                      var id = $(this).data("target");
                      addingTriangle($(id), "odd");
                  }
              });
        },
        _filters: function() {

            var self = this;

            self._populateFilters();
            // bindings
            $(document).on("change", "#filter-options .form-group input", function() {
                self._applyFilters();
            })
            $(document).on("change", "#filter-cuisine", function() {
                $(this).closest(".form-group")
                  .find("input[data-type='filter']")
                  .val($(this).val());

                $("body").trigger({
                    type: "filter_changed"
                });
            })
            //ascoltiamo l'evento custom
            $(document).on("filter_changed", function() {
                self._applyFilters();
            });

            //clear filters
            $(document).on("click", ".filter-controls span", function() {
                //todo
                self._resetFilters();
            });
            $("#filter-type").on("change", function() {
                $(this).parent().find("input[name='filter-type-hidden']")
                  .val($("option:selected").val())
                  .data("all", !!$("option:selected").data("all"));

                self._applyFilters();
            });

        },

        _applyFilters: function() {
            var self = this;
            var zone, price, rating, payment, cousines, type, height;
            $("#filter-options .form-group input[data-type='filter']").each(function() {
                //build object for display or not elements
                if ($(this).attr("name") == "filter-zone-hidden") zone = $(this).data("val");
                if ($(this).attr("name") == "filter-type-hidden") type = !!$(this).data("all") ? "all" : $(this).val();
                if ($(this).attr("name") == "filter-price-hidden") price = $(this).val();
                if ($(this).attr("name") == "filter-rating-hidden") rating = $(this).val();
                if ($(this).attr("name") == "filter-payment-hidden") payment = $(this).data("val");
                if ($(this).attr("name") == "filter-cousine-hidden") cousines = $(this).val();
                if ($(this).attr("name") == "filter-height-hidden") height = $(this).val();
            });
            //fix price bug
            price = $("[name='filter-price']").val();
            $(".filter-controls > .ico-label").removeClass("hidden");
            var filter = {
                zone: !!zone ? zone : "na",
                height: !!height ? height : "na",
                type: !!type ? type : "na",
                price: !!price ? price : "na",
                rating: !!rating ? rating : "na",
                payment: !!payment ? payment : "na",
                cousines: !!cousines ? cousines : "na" // batman
            }
            self._emptyResult("remove");
            $(".filterables").each(function() {
                var data = $(this).data();
                var isHidden = false;

                //PAYMENT TYPE
                if (!!data.payment && filter.payment != "na" && !!self._checkIfEmpty(filter.payment)) {
                    for (var i = 0; i < filter.payment.length; i++) {
                        if (filter.payment[i] != "") {
                            if (data.payment.indexOf(filter.payment[i]) == -1) {
                                isHidden = true;
                            } else {
                                isHidden = false;
                                break;
                            }
                        }
                    }
                }
                //ZONES
                if (!!data.zone && filter.zone != "na") {
                    for (var i = 0; i < filter.zone.length; i++) {
                        if (filter.zone[i] != "") {
                            if (data.zone.indexOf(filter.zone[i]) == -1) {
                                isHidden = true;
                            } else {
                                isHidden = false;
                                break;
                            }
                        }
                    }
                }
                //TYPE
                if (!!data.type && filter.type != "na") {
                    if (data.type != filter.type) {
                        if (filter.type != "all") {
                            isHidden = true;
                        } else {
                            if (!filter.zone) {
                                isHidden = false;
                            }
                        }
                    }
                }

                //HEIGHT
                if (!!data.height && filter.height != "na") {
                    if (data.height > filter.height) {
                        isHidden = true;
                    }
                }
                //PRICE

                if (!!data.price && filter.price != "na") {
                    if (data.price >= parseInt(filter.price)) { isHidden = true; }
                }
                //COUSINE
                if (!!data.cuisine && filter.cousines != "na") {
                    if (data.cuisine.indexOf(filter.cousines) == -1) { isHidden = true; }
                }
                //RATING
                if (!!data.rating && filter.rating != "na") {
                    if (data.rating < filter.rating) { isHidden = true; }
                }

                isHidden ? ($(this).removeClass("hidden").addClass("hidden") && self.$masonry.masonry()) : ($(this).removeClass("hidden") && self.$masonry.masonry());
            });


            if ($(window).width() > 767 && $(window).width() < 1023) {
                var width_col = $(".filterables:visible").outerWidth();
                self.$masonry.masonry({
                    columnWidth: 384,
                    itemSelector: '.masonry-item'
                });
            }

            self._recolocateTilesAftersFiltering();
            if (!jQuery(".filterables:visible").length) {
                self._emptyResult("show");
            }

            if ($(window).width() > 1024) {
                $(".mapplic-pin").show();
                $(".filterables.hidden").each(function() {
                    var id = $(this).find(".image-actions a").eq(0).attr("href").replace("#", "");
                    $("[data-location='" + id + "']").hide();
                });
            }

        },
        _checkIfEmpty: function(obj) {
            var isEmpty = true;
            for (var i = 0; i < obj.length; i++) {
                if (obj[i] != "") {
                    isEmpty = false;
                    break;
                }
            }
            return isEmpty;
        },
        _emptyResult: function(s) {
            switch (s) {
                case "show":
                    var txt = !!$("#filters").data("no-results") ? $("#filters").data("no-results") : "No Results";
                    $(".masonry-container.row").after(
                      $("<div>").addClass("noresult_filter text-center")
                        .css({ "padding-bottom": "50px" })
                        .append($("<span>").addClass("icm-negative_feedback")
                            .css({
                                "font-size": "5em",
                                "color": "#C80065"
                            }),
                          "</br>" + Drupal.t(txt)
                        )
                    );
                    break;
                case "remove":
                    $(".noresult_filter").remove();
                    break;
                default:
                    break;
            }
        },
        _recolocateTilesAftersFiltering: function() {
            var counter = 0;
            var classToAdd = "";
            $(".filterables:visible").each(function() {
                if (counter % 2 == 0) {
                    $(this).find("> div").removeClass("text-on-left text-on-right").addClass("text-on-right");
                    $(this).find("> div >div").removeClass("group-left group-right").addClass("group-right");
                } else {
                    $(this).find("> div").removeClass("text-on-left text-on-right").addClass("text-on-left");
                    $(this).find("> div >div").removeClass("group-left group-right").addClass("group-left");
                }
                counter++;
            });
        },
        _resetFilters: function() {
            var self = this;

            if (!!$('input[type="range"]').length) {
                $('input[type="range"]').val(210).change();
            }

            if (!!$("input[name='filter-price']").length) {
                var arr = [];
                $(".filterables").each(function() {
                    arr.push($(this).data("price"));
                });
                var max = Math.max.apply(null, arr);
                $("[name='filter-price-hidden']").val(0)
                var s = $('.ionslider').data("ionRangeSlider");
                s.update({
                    min: 0,
                    max: max,
                    from: max
                });
            }

            if (!!$("#filter-type").length) {
                $("#filter-type").val($("#filter-type option:eq(0)").text()).trigger("change"); //reset select2
            }

            if (!!$("#filter-cuisine").length) {
                $("#filter-cuisine").val($("#filter-cuisine option:eq(0)").text()).trigger("change"); //reset select2
            }

            $(".filterables").each(function() {
                $(this).removeClass("hidden");
                self.$masonry.masonry();
                $("#filter-options span.ico-label.active, #filter-options .food-rating .value.active").removeClass("active");
            });
            //reset values
            $('input[name="filter-zone-hidden"]').data("val", "");
            $('input[name="filter-type-hidden"]').data("all", false).val("");
            $('input[name="filter-price-hidden"]').val("");
            $('input[name="filter-rating-hidden"]').val("");
            $('input[name="filter-payment-hidden"]').data("val", "");
            $('input[name="filter-cousine-hidden"]').val("");
            $('input[name="filter-height-hidden"]').val("");

            self._recolocateTilesAftersFiltering();

            $(".filter-controls > .ico-label").addClass("hidden");
        },
        _bindings: function() {
            var sharer = null;
            var hovered = false;

            var _buildSharer = function(ev) {
                /*hide share
                 if (sharer==null) {
                 var _this = !!ev.target? $(ev.target) : $(ev);
                 var _class = _this.closest(".inner").parent().hasClass("group-right") ? "sharer-left"
                 : "sharer-right";
                 sharer = $("<span>").addClass("sharer " + _class);
                 var inner = $("<span>").addClass("inner_sharer");
                 var main_url = document.URL;
                 str = main_url.substring(main_url.indexOf(Drupal.settings.pathPrefix) + 2);
                 main_url = main_url.replace(str, "");
                 var url = _this.closest(".row")
                 .find(".textual-content .inner a")
                 .attr("href");
                 url = !!url ? main_url+url : document.URL;
                 var fb = $("<a>").addClass("icm-facebook")
                 .attr({
                 "href": "https://www.facebook.com/sharer/sharer.php?u="+ url,
                 "target" : "_blank"
                 });
                 var inst = $("<a>").addClass("icm-instagram")
                 .attr({
                 "href": "https://www.instagram.com/dubaiparksresorts/",
                 "target" : "_blank"
                 });
                 var tw = $("<a>").addClass("icm-twitter")
                 .attr({
                 "href": "https://twitter.com/intent/tweet?text="+ url,
                 "target" : "_blank"
                 });

                 sharer.append(inner.append(
                 fb,
                 inst,
                 tw
                 )
                 );
                 var _class = !!$(".heading-indented").length ? ".heading-indented" : ".image-side";
                 !!ev.target ? $(ev.target).closest(_class).append(sharer)
                 : ev.closest(_class).append(sharer);
                 }
                 */

            }

            $(document).on("click", function(e) {

                if ($(window).width() < 1024) {
                    if ($(e.target).hasClass("icm-share") || $(e.target).hasClass("sharer")) {
                        if (!!sharer) {
                            sharer.remove()
                            sharer = null;
                        }
                        _buildSharer(e);
                        return;
                    } else {
                        if (!!sharer) {
                            sharer.remove();
                            sharer = null;
                        }
                    }
                }

            });

            //Removed as DF-8743
            /*$(document).on("mouseenter", ".sharer", function() {
                if ($(window).width() > 1024) {
                    hovered = true;
                }
            })
              .on("mouseleave", ".sharer", function() {
                  if ($(window).width() > 1024) {
                      hovered = false;
                      if (!!sharer && !hovered) {
                          sharer.remove();
                          sharer = null;
                      }
                  }
              });
            $(".image-actions a:nth-child(2), .image-actions .icm-share").on("mouseenter", function() {
                var _this = $(this)
                if ($(window).width() > 1024) {
                    _buildSharer(_this);
                }
            })
              .on("mouseleave", function() {
                  if ($(window).width() > 1024) {
                      setTimeout(function() {
                          if (!!sharer && !hovered) {
                              sharer.remove();
                              sharer = null;
                          }
                      }, 900);
                  }
              });*/

        },
        _populateFilters: function() {
            var self = this;
            filters = {};
            var lang = "/" + $("html").attr("lang");
            var cuisine = lang + "/local-services/discover-cuisine";
            var shop_type = lang + "/local-services/discover-shop-type";
            var attraction_type = lang + "/local-services/discover-attraction-type";
            var payment = lang + "/local-services/discover-payament";
            var event_type = lang + "/local-services/discover-event-type";

            var park = document.URL.split("/").reverse()[1];

            var zones = lang + "/local-services/discover-filter-zones?ParkName=" + park
            //chiamiamo il servizio per recuperare i dati

            $.when(

              $.get(cuisine, function(data) {
                  filters.cuisine = data;
              }),
              $.get(shop_type, function(data) {
                  filters.shop_type = data;
              }),
              $.get(attraction_type, function(data) {
                  filters.attraction_type = data;
              }),
              $.get(payment, function(data) {
                  filters.payment = data;
              }),
              $.get(event_type, function(data) {
                  filters.event_type = data;
              }),
              $.get(zones, function(data) {
                  filters.zones = data;
              })

            ).then(function() {

                //popoliamo le attraction type 
                if (!!filters.attraction_type) {
                    var el = $(".select-attraction-type")
                    el.empty();
                    el.append($("<option>").text(Drupal.t("All attractions")).data("all", true));
                    for (var i = 0; i < filters.attraction_type.length; i++) {
                        el.append(
                          $("<option>").text(filters.attraction_type[i].name)
                        )
                    }
                    el.select2({
                        minimumResultsForSearch: Infinity
                    });
                }
                //popoliamo le event type 
                if (!!filters.event_type) {
                    var el = $(".select-events-type")
                    el.empty();
                    el.append($("<option>").text(Drupal.t("All events")).data("all", true));
                    for (var i = 0; i < filters.event_type.length; i++) {
                        el.append(
                          $("<option>").text(filters.event_type[i].name)
                        )
                    }
                    el.select2({
                        minimumResultsForSearch: Infinity
                    });

                    el.after('<input type="hidden" name="filter-type-hidden" data-type="filter">')
                }
                //popoliamo le shop type 
                if (!!filters.shop_type) {
                    var el = $(".select-shop-type")
                    el.empty();
                    el.append($("<option>").text(Drupal.t("All shops")).data("all", true));
                    for (var i = 0; i < filters.shop_type.length; i++) {
                        el.append(
                          $("<option>").text(filters.shop_type[i].name)
                        )
                    }


                    el.select2({
                        minimumResultsForSearch: Infinity
                    });

                    el.after('<input type="hidden" name="filter-type-hidden" data-type="filter">')
                }

                //popoliamo le cucine
                if (!!filters.cuisine) {
                    var el = $(".select-cuisine-type")
                    el.empty();
                    el.append($("<option>").text(Drupal.t("All cuisines")).data("all", true));
                    for (var i = 0; i < filters.cuisine.length; i++) {
                        el.append(
                          $("<option>").text(filters.cuisine[i].name)
                        )
                    }


                    el.select2({
                        minimumResultsForSearch: Infinity
                    });

                    el.after('<input type="hidden" name="filter-cousine-hidden" data-type="filter">')

                }
                //popolilamo i payment
                if (!!filters.payment) {
                    var el = $(".payment_container")
                    el.empty();
                    for (var i = 0; i < filters.payment.length; i++) {
                        el.append(
                          $("<span>").addClass("ico-label").append(
                            $("<i>").addClass(filters.payment[i].CssClass),
                            $("<label>").text(filters.payment[i].name),
                            "<input name='filter-payment' type='checkbox'>"
                          )
                        )
                    }
                    el.append('<input type="hidden" name="filter-payment-hidden" data-type="filter">');
                }

                //popoliamo le zones
                if (!!filters.zones) {
                    var el = $(".zones_container")
                    el.empty();
                    if (filters.zones.length <= 5 && !!$("body.events").length) {
                        console.log("classes.changed")
                        el.parent().removeClass("col-md-9").addClass("col-md-6");
                    }
                    for (var i = 0; i < filters.zones.length; i++) {
                        el.append(
                          $("<span>").addClass("ico-label").append(
                            $("<i>").addClass(filters.zones[i].CssClass),
                            $("<label>").text(filters.zones[i].ZoneTitle),
                            "<input type='hidden' name='filter-zone-hidden' data-type='filter'> "
                          )
                        )
                    }
                }
            });
        }
    }
})(jQuery);;/**/
(function ($) {

    Drupal.behaviors.dpr_event_add = {
        attach: function (context, settings) {
            var $modalInfo, currEvent, guests_modal, currency, initialSearch, $time_modal, $parks_modals, $inspire_me_modal,
                guests, $root, eventDataReq, Model, CartModel, fadeInClass, cookieManager, validatedGuestNumber,
                seatSelectionURI, seatSelectionURIdef, cart, guestsLimit, TIMER_LIMIT_EVENTS;

            var $datepickers = $(".datepicker");
            $datepickers.datepicker({
                minDate       : '+0d',
                startDate       : '+0d',
                        constrainInput: true,
                        dayNamesMin: $.datepicker._defaults.dayNamesShort,
                        beforeShow    : function (textbox, instance) {

                            $(this).parent().append(instance.dpDiv);
                            instance.dpDiv.hide();

                            instance.dpDiv.css('border-color', '#EEEEEE');
                            instance.input.toggleClass('toggled');
                        },
                        onClose       : function (textbox, instance) {
                            instance.input.toggleClass('toggled');
                        },
                        onSelect      : function (dateText) {

                            $(this).prop('value', dateText);
                            //$(this).valid();

                            var form        = $(this).closest('form');

                            form.find('#date_start_ticket').val(dateText);

                            var d1   = new Date(dateText);
                            var d2   = new Date();
                            d2.setTime(d1.getTime() + 86400000);
                            var d3   = new Date();
                            d3.setTime(d2.getTime() + 86400000);
                            var d4   = new Date();
                            d4.setTime(d3.getTime() + 86400000);
                            var d360 = new Date();
                            d360.setFullYear(d1.getFullYear() + 1);

                        }
                    });
            guests = {adult: 1};
            guestsLimit = 15;

            currEvent = $(".get_event_id").attr("data-event");

            currency = settings.dpr_event_add.currency || 'AED';
            currency = currency.toUpperCase();

            seatSelectionURI    = settings.dpr_event_add.seatSelectionURI || '/';
            seatSelectionURIdef = settings.dpr_event_add.seatSelectionURIdef || '/';

            $root = $('#pass');

            eventDataReq = {
                uri: ''
            };

            Model = {};
            CartModel = {};

            cart = new CartNew();
            cart.setFallback(settings.dpr_event_add.fallbackUri);
            cart.setRequest(settings.dpr_event_add.requestUri);
            cart.setBooking(settings.dpr_event_add.bookingUri);
            cart.validate(settings.dpr_event_add.validate);

            TIMER_LIMIT_EVENTS = 15;

            /**
             * Search submit
             */

            function saveModel(d){
                var events, priceMinCompare;

                events = [];
                priceMinCompare = [];

                if(d.status == "success" && d.data.events.length){
                    events = d.data.events;
                    events.forEach(function (e, i) {
                        var p = {}, price = {};

                        e.id = 'event-id-' + i;
                        e.products.forEach(function (o, i){
                            var j, k, len, prices;
                            j = 0;
                            prices = o.prices;
                            len = prices.length;
                            while(j < len && prices[j]['currency'] != currency){
                                j++;
                            }
                            if(j > len){
                                j = 0;
                            }
                            e.currency = prices[j]['currency'];
                            o.price = {};
                            e.priceMin = 0;
                            for(k in prices[j]){
                                if(prices[j].hasOwnProperty(k)){
                                    o.price[k] = prices[j][k];
                                    if(!isNaN(o.price[k]) && o.price[k] != null ){
                                        priceMinCompare.push(o.price[k]);
                                    }

                                }
                            }
                            //e.priceMin = Math.min(e.priceMin, prices[j]['price']);
                            var smallest= priceMinCompare[0];
                            for (var iCompare =0; iCompare < priceMinCompare.length; iCompare++){
                                if(priceMinCompare[iCompare] < smallest){
                                    smallest= priceMinCompare[iCompare];
                                }
                            }
                            e.priceMin = smallest;

                            delete o.prices;
                            p['product-id-' + i] = o;
                        });
                        e.products = p;

                        Model[e.id] = e;
                    });
                }

                return events;
            }

            $root.find('.cart-btn_events').on('click', function (e) {
                e.preventDefault();

                var data, $this;
                $this = $(this);

                var guestNumber = parseInt($(".select_guest_dropdown .num").text());
                var dateSelect = $(".datepicker").datepicker( "getDate" );
                //TODO VERIFICARE CHE GUEST E DATE SIANO POPOLATI E RECUPERARE I VALORI
                if (guestNumber >= 0 && dateSelect) {
                    data = normalizeReqObj({
                        //INSERIRE QUI I DATI PRESI POCO SOPRA (guest date)
                        guest: guestNumber,
                        date: moment(dateSelect)
                    });

                    toggleSpinner();
                        sendRequest(data, function (response) {
                            var eventoSelezionato, id, i, events;
                            toggleSpinner();
                            //PRENDERE RESPONSE E TIRARE GIU SOLTANTO L'EVENT CHE CI INTERESSA UTILIZZANDO
                            //currEvent
                           if(response.status == 'success'){
                               events = saveModel(response);


                            for(i = 0; i < events.length; i++){
                                if(currEvent == events[i].eventId){
                                    eventoSelezionato = events[i];
                                }
                            }

                            if(eventoSelezionato){

                                      if(eventoSelezionato["seatMapId"]){
                                          if(cart.canAdd('events')){
                                              requestSeat(eventoSelezionato);
                                          }
                                      } else {
                                          eventoSelezionato = filterData(eventoSelezionato);
                                          eventoSelezionato['qty'] = 1;
                                          addToCart(eventoSelezionato);
                                      }
                            }

                           } else {
                                console.log("response", response);
                                $("#cartAlert").modal("show");
                                $("#cartAlert .body").text(Drupal.t("We haven't found any events matching your search."));
                           }

                        });
                }


            });

            function normalizeReqObj(o){
                var flag, cmb = {}, ret = {}, ds, de;

                    ds = moment(o['date']).hours(0).minutes(0);
                    de = moment(o['date']).hours(23).minutes(59);

                    ret = {
                        fromDateTime: ds.format(),
                        toDateTime: de.format(),
                        pagePos: 1,
                        seatMinQuantity: o['guest'],
                        park: 'MULTIPRK',
                        sellableOnly: true
                    }

                ret['isValid'] = flag;

                return ret;

            }

            function sendRequest(d, callback){
                callback = callback || function () {};
                $.getJSON(settings.dpr_event_add.serviceUri, d).done(function(r) {
                    callback(r);
                });
            }

            function eraseSeatSelCookie(){
                var cookieSet = {
                    cookieName: "seatTimeExpire"
                };
                cookieManager("erase",cookieSet);
            }

            function toggleSpinner(){
                var $spinnerContainer = $('.spinner_container');
                var $spinnerImg = $spinnerContainer.find('img');

                var imageUrl = $spinnerImg.prop('src');
                $spinnerImg.prop('src', '');
                $spinnerImg.prop('src', imageUrl);

                $spinnerContainer.is(":visible") ?
                    ($spinnerContainer.hide() && $('body').css('overflow-y', 'scroll'))
                    :
                    ($spinnerContainer.show() && $('body').css('overflow-y', 'hidden'));
            }


            function addToCart(Cart){
                cart.add(Cart, {
                    currency: currency,
                    ref: 'events'
                });
                cart.registerTimer(moment().add(TIMER_LIMIT_EVENTS, 'minutes').format('X'))
            }

            function requestSeat(obj){
                var callback, filtered, cache;
                var guestNumber = parseInt($(".select_guest_dropdown .num").text());

                cache = obj;
                callback = function (response) {
                    //toggleSpinner();
                    try{
                        response = JSON.parse(response);
                    } catch (e){
                        response = [];
                    }

                    if(response["status"] == "success"){
                        location.href = seatSelectionURIdef;
                        //console.log("guests: ", guestNumber);
                    }
                };

                filtered = {
                    "seatSelection": {
                        eventName        : encodeURI(cache['name']),
                        eventId          : cache['eventId'],
                        performanceId    : cache['performanceId'],
                        seatMapId        : cache['seatMapId'],
                        quantityFree     : cache['quantityFree'],
                        quantityReserved : cache['quantityReserved'],
                        products         : cache['products'],
                        numGuests        : guestNumber
                    }
                };

                toggleSpinner();
                $.ajax({
                    type: "POST",
                    url: seatSelectionURI,
                    data: filtered,
                    success: callback
                });

            }

            function filterData(obj){
                var ret;
                var guestNumber = parseInt($(".select_guest_dropdown .num").text());

                ret = {};
                ret["eventId"] = obj["eventId"];
                ret["name"] = obj["name"];
                ret["performanceId"] = obj["performanceId"];
                ret["productId"] = obj["products"]["product-id-0"]["productId"];
                ret["seatMapId"] = obj["products"]["product-id-0"]["seatMapId"];
                ret["price"] = obj["products"]["product-id-0"]["price"]["price"];
                ret["qty"] = guestNumber;

                return ret;
            }

            //General init
            (function (){

            }());

        }

    }

})(jQuery);;/**/
