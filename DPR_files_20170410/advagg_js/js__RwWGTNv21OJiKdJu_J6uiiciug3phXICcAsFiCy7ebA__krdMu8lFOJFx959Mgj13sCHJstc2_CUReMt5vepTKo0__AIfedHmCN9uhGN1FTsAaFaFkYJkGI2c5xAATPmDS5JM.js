/**
* @file
* Javascript, modifications of DOM.
*
* Manipulates links to include jquery load funciton
*/

(function ($) {
  Drupal.behaviors.jquery_ajax_load = {
    attach: function (context, settings) {
      jQuery.ajaxSetup ({
      // Disable caching of AJAX responses
        cache: false
      });

      var trigger = Drupal.settings.jquery_ajax_load.trigger;
      var target = Drupal.settings.jquery_ajax_load.target;
      // Puede ser más de un valor, hay que usar foreach()
      $(trigger).once(function() {
        var html_string = $(this).attr( 'href' );
        // Hay que validar si la ruta trae la URL del sitio
        $(this).attr( 'href' , target );
        var data_target = $(this).attr( 'data-target' );
        if (typeof data_target === 'undefined' ) {
          data_target = target;
        }
        else {
          data_target = '#' + data_target;
        }
        $(this).click(function(evt) {
          evt.preventDefault();
          jquery_ajax_load_load($(this), data_target, html_string);
        });
      });
      $(trigger).removeClass(trigger);
    }
  };  

// Handles link calls
  function jquery_ajax_load_load(el, target, url) {
    var module_path = Drupal.settings.jquery_ajax_load.module_path;
    var toggle = Drupal.settings.jquery_ajax_load.toggle;
    var base_path = Drupal.settings.jquery_ajax_load.base_path;
    var animation = Drupal.settings.jquery_ajax_load.animation;
    if( toggle && $(el).hasClass( "jquery_ajax_load_open" ) ) {
      $(el).removeClass( "jquery_ajax_load_open" );
      if ( animation ) {
        $(target).hide('slow', function() {
          $(target).empty();
        });
      }
      else {
        $(target).empty();
      }
    }
    else {
      var loading_html = Drupal.t('Loading'); 
      loading_html += '... <img src="/';
      loading_html += module_path;
      loading_html += '/jquery_ajax_load_loading.gif">';
      $(target).html(loading_html);
      $(target).load(base_path + 'jquery_ajax_load/get' + url, function( response, status, xhr ) {
        if ( status == "error" ) {
          var msg = "Sorry but there was an error: ";
          $(target).html( msg + xhr.status + " " + xhr.statusText );
        }
        else {
          if ( animation ) {
            $(target).hide();
            $(target).show('slow')
          }
//        Drupal.attachBehaviors(target);
        }
      });
      $(el).addClass( "jquery_ajax_load_open" );
    }
  }
}(jQuery));
;/**/
/**
 * Draggable Background plugin for jQuery
 *
 * v1.2.4
 *
 * Copyright (c) 2014 Kenneth Chung
 *
 * Licensed under the MIT license:
 *   http://www.opensource.org/licenses/mit-license.php
 */
;(function($) {
  var $window = $(window);

  // Helper function to guarantee a value between low and hi unless bool is false
  var limit = function(low, hi, value, bool) {
    if (arguments.length === 3 || bool) {
      if (value < low) return low;
      if (value > hi) return hi;
    }
    return value;
  };

  // Adds clientX and clientY properties to the jQuery's event object from touch
  var modifyEventForTouch = function(e) {
    e.clientX = e.originalEvent.touches[0].clientX;
    e.clientY = e.originalEvent.touches[0].clientY;
  };

  var getBackgroundImageDimensions = function($el) {
    var bgSrc = ($el.css('background-image').match(/^url\(['"]?(.*?)['"]?\)$/i) || [])[1];
    if (!bgSrc) return;

    var imageDimensions = { width: 0, height: 0 },
        image = new Image();

    image.onload = function() {
      if ($el.css('background-size') == "cover") {
        var elementWidth = $el.innerWidth(),
            elementHeight = $el.innerHeight(),
            elementAspectRatio = elementWidth / elementHeight;
            imageAspectRatio = image.width / image.height,
            scale = 1;

        if (imageAspectRatio >= elementAspectRatio) {
          scale = elementHeight / image.height;
        } else {
          scale = elementWidth / image.width;
        }

        imageDimensions.width = image.width * scale;
        imageDimensions.height = image.height * scale;
      } else {
        imageDimensions.width = image.width;
        imageDimensions.height = image.height;
      }
    };

    image.src = bgSrc;

    return imageDimensions;
  };

  function Plugin(element, options) {
    this.element = element;
    this.options = options;
    this.init();
  }

  Plugin.prototype.init = function() {
    var $el = $(this.element),
        bgSrc = ($el.css('background-image').match(/^url\(['"]?(.*?)['"]?\)$/i) || [])[1],
        options = this.options;

    if (!bgSrc) return;

    // Get the image's width and height if bound
    var imageDimensions = { width: 0, height: 0 };
    if (options.bound) {
      imageDimensions = getBackgroundImageDimensions($el);
    }

    $el.on('mousedown.dbg touchstart.dbg', function(e) {
      if (e.target !== $el[0]) {
        return;
      }
      e.preventDefault();

      if (e.originalEvent.touches) {
        modifyEventForTouch(e);
      } else if (e.which !== 1) {
        return;
      }

      var x0 = e.clientX,
          y0 = e.clientY,
          pos = $el.css('background-position').match(/(-?\d+).*?\s(-?\d+)/) || [],
          xPos = parseInt(pos[1]) || 0,
          yPos = parseInt(pos[2]) || 0;

      $window.on('mousemove.dbg touchmove.dbg', function(e) {
        e.preventDefault();

        if (e.originalEvent.touches) {
          modifyEventForTouch(e);
        }

        var x = e.clientX,
            y = e.clientY;

        xPos = options.axis === 'y' ? xPos : limit($el.innerWidth()-imageDimensions.width, 0, xPos+x-x0, options.bound);
        yPos = options.axis === 'x' ? yPos : limit($el.innerHeight()-imageDimensions.height, 0, yPos+y-y0, options.bound);
        x0 = x;
        y0 = y;

        $el.css('background-position', xPos + 'px ' + yPos + 'px');
      });

      $window.on('mouseup.dbg touchend.dbg mouseleave.dbg', function() {
        if (options.done) {
          options.done();
        }

        $window.off('mousemove.dbg touchmove.dbg');
        $window.off('mouseup.dbg touchend.dbg mouseleave.dbg');
      });
    });
  };

  Plugin.prototype.disable = function() {
    var $el = $(this.element);
    $el.off('mousedown.dbg touchstart.dbg');
    $window.off('mousemove.dbg touchmove.dbg mouseup.dbg touchend.dbg mouseleave.dbg');
  }

  $.fn.backgroundDraggable = function(options) {
    var options = options;
    var args = Array.prototype.slice.call(arguments, 1);

    return this.each(function() {
      var $this = $(this);

      if (typeof options == 'undefined' || typeof options == 'object') {
        options = $.extend({}, $.fn.backgroundDraggable.defaults, options);
        var plugin = new Plugin(this, options);
        $this.data('dbg', plugin);
      } else if (typeof options == 'string' && $this.data('dbg')) {
        var plugin = $this.data('dbg');
        Plugin.prototype[options].apply(plugin, args);
      }
    });
  };

  $.fn.backgroundDraggable.defaults = {
    bound: true,
    axis: undefined
  };
}(jQuery));
;/**/
/* Arabic Translation for jQuery UI date picker plugin. */
/* Used in most of Arab countries, primarily in Bahrain, Kuwait, Oman, Qatar, Saudi Arabia and the United Arab Emirates, Egypt, Sudan and Yemen. */
/* Written by Mohammed Alshehri -- m@dralshehri.com */

( function( factory ) {
	if ( typeof define === "function" && define.amd ) {

		// AMD. Register as an anonymous module.
		define( [ "../widgets/datepicker" ], factory );
	} else {

		// Browser globals
		factory( jQuery.datepicker );
	}
}( function( datepicker ) {

datepicker.regional.ar = {
	closeText: "إغلاق",
	prevText: "&#x3C;السابق",
	nextText: "التالي&#x3E;",
	currentText: "اليوم",
	monthNames: [ "يناير", "فبراير", "مارس", "أبريل", "مايو", "يونيو",
	"يوليو", "أغسطس", "سبتمبر", "أكتوبر", "نوفمبر", "ديسمبر" ],
	monthNamesShort: [ "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12" ],
	dayNames: [ "الأحد", "الاثنين", "الثلاثاء", "الأربعاء", "الخميس", "الجمعة", "السبت" ],
	dayNamesShort: [ "أحد", "اثنين", "ثلاثاء", "أربعاء", "خميس", "جمعة", "سبت" ],
	dayNamesMin: [ "ح", "ن", "ث", "ر", "خ", "ج", "س" ],
	weekHeader: "أسبوع",
	dateFormat: "dd/mm/yy",
	firstDay: 0,
		isRTL: true,
	showMonthAfterYear: false,
	yearSuffix: "" };
//datepicker.setDefaults( datepicker.regional.ar );

return datepicker.regional.ar;

} ) );;/**/
/*!
 * The Final Countdown for jQuery v2.1.0 (http://hilios.github.io/jQuery.countdown/)
 * Copyright (c) 2015 Edson Hilios
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
!function(a){"use strict";"function"==typeof define&&define.amd?define(["jquery"],a):a(jQuery)}(function(a){"use strict";function b(a){if(a instanceof Date)return a;if(String(a).match(g))return String(a).match(/^[0-9]*$/)&&(a=Number(a)),String(a).match(/\-/)&&(a=String(a).replace(/\-/g,"/")),new Date(a);throw new Error("Couldn't cast `"+a+"` to a date object.")}function c(a){var b=a.toString().replace(/([.?*+^$[\]\\(){}|-])/g,"\\$1");return new RegExp(b)}function d(a){return function(b){var d=b.match(/%(-|!)?[A-Z]{1}(:[^;]+;)?/gi);if(d)for(var f=0,g=d.length;g>f;++f){var h=d[f].match(/%(-|!)?([a-zA-Z]{1})(:[^;]+;)?/),j=c(h[0]),k=h[1]||"",l=h[3]||"",m=null;h=h[2],i.hasOwnProperty(h)&&(m=i[h],m=Number(a[m])),null!==m&&("!"===k&&(m=e(l,m)),""===k&&10>m&&(m="0"+m.toString()),b=b.replace(j,m.toString()))}return b=b.replace(/%%/,"%")}}function e(a,b){var c="s",d="";return a&&(a=a.replace(/(:|;|\s)/gi,"").split(/\,/),1===a.length?c=a[0]:(d=a[0],c=a[1])),1===Math.abs(b)?d:c}var f=[],g=[],h={precision:100,elapse:!1};g.push(/^[0-9]*$/.source),g.push(/([0-9]{1,2}\/){2}[0-9]{4}( [0-9]{1,2}(:[0-9]{2}){2})?/.source),g.push(/[0-9]{4}([\/\-][0-9]{1,2}){2}( [0-9]{1,2}(:[0-9]{2}){2})?/.source),g=new RegExp(g.join("|"));var i={Y:"years",m:"months",n:"daysToMonth",w:"weeks",d:"daysToWeek",D:"totalDays",H:"hours",M:"minutes",S:"seconds"},j=function(b,c,d){this.el=b,this.$el=a(b),this.interval=null,this.offset={},this.options=a.extend({},h),this.instanceNumber=f.length,f.push(this),this.$el.data("countdown-instance",this.instanceNumber),d&&("function"==typeof d?(this.$el.on("update.countdown",d),this.$el.on("stoped.countdown",d),this.$el.on("finish.countdown",d)):this.options=a.extend({},h,d)),this.setFinalDate(c),this.start()};a.extend(j.prototype,{start:function(){null!==this.interval&&clearInterval(this.interval);var a=this;this.update(),this.interval=setInterval(function(){a.update.call(a)},this.options.precision)},stop:function(){clearInterval(this.interval),this.interval=null,this.dispatchEvent("stoped")},toggle:function(){this.interval?this.stop():this.start()},pause:function(){this.stop()},resume:function(){this.start()},remove:function(){this.stop.call(this),f[this.instanceNumber]=null,delete this.$el.data().countdownInstance},setFinalDate:function(a){this.finalDate=b(a)},update:function(){if(0===this.$el.closest("html").length)return void this.remove();var b,c=void 0!==a._data(this.el,"events"),d=new Date;b=this.finalDate.getTime()-d.getTime(),b=Math.ceil(b/1e3),b=!this.options.elapse&&0>b?0:Math.abs(b),this.totalSecsLeft!==b&&c&&(this.totalSecsLeft=b,this.elapsed=d>=this.finalDate,this.offset={seconds:this.totalSecsLeft%60,minutes:Math.floor(this.totalSecsLeft/60)%60,hours:Math.floor(this.totalSecsLeft/60/60)%24,days:Math.floor(this.totalSecsLeft/60/60/24)%7,daysToWeek:Math.floor(this.totalSecsLeft/60/60/24)%7,daysToMonth:Math.floor(this.totalSecsLeft/60/60/24%30.4368),totalDays:Math.floor(this.totalSecsLeft/60/60/24),weeks:Math.floor(this.totalSecsLeft/60/60/24/7),months:Math.floor(this.totalSecsLeft/60/60/24/30.4368),years:Math.abs(this.finalDate.getFullYear()-d.getFullYear())},this.options.elapse||0!==this.totalSecsLeft?this.dispatchEvent("update"):(this.stop(),this.dispatchEvent("finish")))},dispatchEvent:function(b){var c=a.Event(b+".countdown");c.finalDate=this.finalDate,c.elapsed=this.elapsed,c.offset=a.extend({},this.offset),c.strftime=d(this.offset),this.$el.trigger(c)}}),a.fn.countdown=function(){var b=Array.prototype.slice.call(arguments,0);return this.each(function(){var c=a(this).data("countdown-instance");if(void 0!==c){var d=f[c],e=b[0];j.prototype.hasOwnProperty(e)?d[e].apply(d,b.slice(1)):null===String(e).match(/^[$A-Z_][0-9A-Z_$]*$/i)?(d.setFinalDate.call(d,e),d.start()):a.error("Method %s does not exist on jQuery.countdown".replace(/\%s/gi,e))}else new j(this,b[0],b[1])})}});;/**/
/**
 * Number.prototype.format(n, x, s, c)
 *
 * @param n         integer, length of decimal
 * @param x         integer, length of whole part
 * @param s         mixed, sections delimiter
 * @param c         mixed, decimal delimiter
 */
Number.prototype.format = function (n, x, s, c) {
  var re  = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
      num = this.toFixed(Math.max(0, ~~n));

  return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
};

/**
 * Trim at length
 * @param m
 * @returns {string}
 */
String.prototype.trimToLength = function (m) {

  return (this.length > m)
    ? jQuery.trim(this).substring(0, m) : this;
};

function placeCaretAtEnd(el) {
  el.focus();
  if (typeof window.getSelection != "undefined"
    && typeof document.createRange != "undefined") {
    var range = document.createRange();
    range.selectNodeContents(el);
    range.collapse(false);
    var sel   = window.getSelection();
    sel.removeAllRanges();
    sel.addRange(range);
  }
  else if (typeof document.body.createTextRange != "undefined") {
    var textRange = document.body.createTextRange();
    textRange.moveToElementText(el);
    textRange.collapse(false);
    textRange.select();
  }
}

/**
 * Load Cart from JSON
 * @param url
 * @param data
 */
function loadFromJSON(url, data) {

  if (data == null) {
    data = {};
  }

  $.getJSON(url, data)
    .done(function (json) {
      console.log('Tickets loaded from JSON', json);
      return json;
    })
    .fail(function (jqxhr, textStatus, error) {
      var err = textStatus + ", " + error;
      console.log("Request Failed: " + err);
    });

  return null
}

function addStylesheetRules(rules) {
  var styleEl = document.createElement('style'),
      styleSheet;

  // Append style element to head
  document.head.appendChild(styleEl);

  // Grab style sheet
  styleSheet = styleEl.sheet;

  for (var i = 0, rl = rules.length; i < rl; i++) {
    var j = 1, rule = rules[i], selector = rules[i][0], propStr = '';
    // If the second argument of a rule is an array of arrays, correct our variables.
    if (Object.prototype.toString.call(rule[1][0]) === '[object Array]') {
      rule = rule[1];
      j    = 0;
    }

    for (var pl = rule.length; j < pl; j++) {
      var prop = rule[j];
      propStr += prop[0] + ':' + prop[1] + (prop[2] ? ' !important' : '') + ';\n';
    }

    // Insert CSS Rule
    styleSheet.insertRule(selector + '{' + propStr + '}', styleSheet.cssRules.length);
  }
}

function isNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

function isDate(txtDate) {
  var currVal = txtDate;
  if (currVal == '')
    return false;

  /*var rxDatePattern = /^(\d{1,2}) ([A-Za-z]{3}) (\d{4})$/; //Declare Regex
  var dtArray = currVal.match(rxDatePattern); // is format OK?

  if (dtArray == null)
    return false;

  console.log("array DATE", dtArray);

  //Checks for dd/mm/yyyy format.
  var dtDay   = dtArray[1];
  var dtMonth = dtArray[2];
  var dtYear  = dtArray[3];

  console.log("VERIFY DATE", dtMonth, dtDay, dtYear);

  var months = [
    "JAN",
    "FEB",
    "MAR",
    "APR",
    "MAY",
    "JUN",
    "JUL",
    "AUG",
    "SEP",
    "OCT",
    "NOV",
    "DEC"
  ];

  if (!months.indexOf(dtMonth)) {
    return false;
  }
  else if (dtDay < 1 || dtDay > 31) {
    return false;
  }
  else if ((dtMonth == 4 || dtMonth == 6 || dtMonth == 9 || dtMonth == 11) && dtDay == 31) {
    return false;
  }
  else if (dtMonth == 2) {
    var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));

    if (dtDay > 29 || (dtDay == 29 && !isleap)) {
      return false;
    }
  }*/
  return true;
};/**/
var messages = {
  'errorQtyTickets'         : 'You cannot buy more than @nbTickets tickets.',
  'errorNullGuests'         : 'You must select guests for this ticket.',
  'errorNullDate'           : 'You must select a valid date.',
  'erasePreviousTicket'     : 'Tickets in cart will be replace by these ones.',
  'erasePreviousHotel'      : 'Hotel and hotel rooms in cart will be replaced by these ones.',
  'erasePreviousHotelTicket': 'Hotel, hotel rooms, and tickets in cart will be replaced by these ones.',
  'errorSearchCheckin'      : 'You must select a checkin date.',
  'errorSearchCheckout'     : 'You must select a checkout date.',
  'errorSearchNullGuests'   : 'You must select at least one guest.',
  'errorSearchNullRooms'    : 'You must insert at least one room.',
  'errorSearchMaxRooms'     : 'You can insert a maximum of @maxRooms rooms.',
  'errorQtyGuests'          : 'You cannot add more than @nbGuests guests.',
  'errorMinAdultGuest'      : 'At least one adult must be inserted.',
  'errorMinAdultRoomGuest'  : 'At least one adult per room must be inserted.',
  'errorChildAge'           : 'Please indicate children ages.',
  'errorNumberRoomsStep2'   : 'Please select @nbRooms rooms before continuing.',
  'errorInsertCart'         : 'Error while adding in Cart. Please retry later.',
  'errorSearch'             : 'Error while searching. Please retry later.',
  'errorFormFields'         : 'All fields are required.',
  'errorMissingGuestsForm'  : 'You must fill the guests form before booking.',
  'invalidChrInField'       : 'Invalid characters in @fieldName.'
};

;/**/
var $ = jQuery;

// Variables max/limit in booking
var MAX_TICKETS      = 15;
var MAX_HOTEL_GUESTS = 15;
var MAX_SHOW_ROOM    = 2;
var MAX_SHOW_TICKETS = 3;
var MAX_ROOMS        = 5;
var MIN_CHILD_AGE    = 3;
var MAX_CHILD_AGE    = 10;
var MIN_ADULT_GUEST  = 1;

// Initial filter configuration
var MIN_PRICE_RANGE      = 0;
var MAX_PRICE_RANGE      = 20000;
var START_RATING         = 0;
var START_RECOMMENDATION = true;
var DISTANCE_FROM        = "poiDubaiPark579";

// Set default max Date on all datepicker +18 months from now
$.datepicker.setDefaults({
  maxDate: "+18m"
});;/**/
(function ($) {

  Drupal.behaviors.frontpage = {
    attach: function (context, settings) {

      //PLAY VIDEO
      /*
       $(".play-pause").click(function () {
       var video = $("#video_0").get(0);
       video.play();

       $(this).css("visibility", "hidden");
       return false;
       });

       $("#video_0").on("pause ended", function () {
       $(".play-pause").css("visibility", "visible");
       });
       */

      // Countdown
      $("#days-number").countdown("2016/12/12", function (event) {

        //var today_string = event.strftime('%D');
        var today             = parseInt(event.strftime('%D'));
        var today_string      = today.toString();
        var yesterday_integer = parseInt(today - 1);

        var yesterday = today - 1;

        var today_first_two = today_string.substring(0, 2);
        var today_last_one  = today_string.substring(3, 2);

        var yesterday_string   = yesterday_integer.toString();
        var yesterday_last_one = yesterday_string.substring(3, 2);
        // alert(today_first_two);
        $('#days-number-first-two').text(today_first_two);
        $('#days-number-last-one').text(yesterday_last_one);
        $('#days-number-yesterday-last-one').text(today_last_one);

      });

      var $bg          = $('#drag-map'),
          origin       = {x: 0, y: 0},
          start        = {x: 0, y: 0},
          movecontinue = false;

      function move(e) {
        var moveby = {
          x: origin.x - e.clientX,
          y: origin.y - e.clientY
        };

        var backgroundPos = $('#drag-map').css("backgroundPosition").split(" ");
        var xPos          = backgroundPos[0],
            yPos          = backgroundPos[1];

        var abs_x = xPos.replace('px', '');
        var abs_y = yPos.replace('px', '');

        console.log('x' + abs_x);
        console.log('y' + abs_y);

        if (movecontinue === true) {
          start.x = start.x - moveby.x;
          start.y = start.y - moveby.y;

          $(this).css('background-position', start.x + 'px ' + start.y + 'px');
        }

        console.log('MoveBy x' + moveby.x);
        console.log('MoveBy y' + moveby.y);
        console.log($bg.width());

        origin.x = e.clientX;
        origin.y = e.clientY;

        e.stopPropagation();
        return false;
      }

      function handle(e) {
        movecontinue = false;
        $bg.unbind('mousemove', move);

        if (e.type == 'mousedown') {
          origin.x     = e.clientX;
          origin.y     = e.clientY;
          movecontinue = true;
          $bg.bind('mousemove', move);
        }
        else {
          $(document.body).focus();
        }

        e.stopPropagation();
        return false;
      }

      function reset() {
        start = {x: 0, y: 0};
        $(this).css('backgroundPosition', '0 0');
      }

      $bg.on('mousedown mouseup mouseleave', handle);
      $bg.on('dblclick', reset);

      /*var controller = new ScrollMagic.Controller();

       new ScrollMagic.Scene({triggerElement: ".parallax-skyline", offset: "-400px", duration: "70%"})
       .setTween("#balloon1", {y: "-300%", ease: Linear.easeNone})
       .addTo(controller);

       new ScrollMagic.Scene({triggerElement: ".parallax-skyline", offset: "-400px", duration: "100%"})
       .setTween("#balloon2", {y: "-150%", ease: Linear.easeNone})
       .addTo(controller);

       new ScrollMagic.Scene({triggerElement: ".parallax-skyline", offset: "-400px", duration: "70%"})
       .setTween("#balloon3", {y: "-250%", ease: Linear.easeNone})
       .addTo(controller);*/

      $(document).on('init', '#slick-views-slider-front-2-slider', function (event, slick) {

        var $topSlider = $(this).find('.slide--' + slick.currentSlide).find('.top-slider');
        var textualContent = "";

        if ($topSlider.hasClass('withcaption')) {
          textualContent = $topSlider.html();
        }

        var $sliderTextMobile = $('.slider-text-mobile');

        $sliderTextMobile.html(textualContent);

        $(window).resize(function () {
          if ($(window).width() <= 768) {
            $sliderTextMobile.show();
          }
          else {
            $sliderTextMobile.hide();
          }
        });
      });

      // On before slide change
      $(document).on('beforeChange', '#slick-views-slider-front-2-slider', function (event, slick, currentSlide, nextSlide) {

        var $topSlider = $(this).find('.slide--' + nextSlide).find('.top-slider');
        var $sliderTextMobile = $('.slider-text-mobile');
        var textualContent = "";

        if ($topSlider.hasClass('withcaption')) {
          //$sliderTextMobile.removeClass("hero");
          textualContent = $topSlider.html();
          /*$('.fate_wrapper').fadeOut(function () {
            $sliderTextMobile.fadeIn(2000);
          });*/
          //$('.fate_wrapper').fadeOut(1000);
          $(this).find('.slick-dots').removeClass('hero');
          $(this).find('.slick-list').removeClass('hero');
        }
        else {
          if ($(window).width() <= 550) {
            $(this).find('.slick-dots').addClass('hero');
            $(this).find('.slick-list').addClass('hero');
            /*$sliderTextMobile.fadeOut(function () {
              $('.fate_wrapper').fadeIn(2000);
            });*/
            /*textualContent = "<div class='fate_wrapper'>" +
              "<img id='luna_hero' src='"+urlSito+"sites/all/themes/dprs_common/images/hero-banner-hp/luna-new.png' class='carousel-animated sx'>" +
              "<img id='nova_hero' src='"+urlSito+"sites/all/themes/dprs_common/images/hero-banner-hp/nova-new.png' class='carousel-animated rx'>" +
              "</div>";
            $sliderTextMobile.addClass("hero");*/
          }
        }

        if ($(window).width() <= 768 && $topSlider.hasClass('withcaption')) {
          $sliderTextMobile.fadeOut(function () {
            $sliderTextMobile.html(textualContent);
            $sliderTextMobile.fadeIn(2000);
          });
        }
        else {
          $sliderTextMobile.hide();
        }
      });

      // change carousel height on init
      $(document).on('init', '#slick-views-slider-front-bottom--1-slider', function (event, slick) {

        if ($(window).width() <= 680) {
          $(this).height($(this).find('.slide--' + slick.currentSlide).height());
        }
      });

      // Change carousel height on change slide
      $(document).on('beforeChange', '#slick-views-slider-front-bottom--1-slider', function (event, slick, currentSlide, nextSlide) {

        if ($(window).width() <= 680) {
          $(this).height($(this).find('.slide--' + nextSlide).height());
        }
      });

      /*
       var $bg = $('#drag-map');
       // data = $('#data')[0],
       var    elbounds = {
       w: parseInt($bg.width()),
       h: parseInt($bg.height())
       };

       if (parseInt($bg.width()) < 1300) {
       var bounds = {w: 1773 - elbounds.w, h: 800 - elbounds.h};
       } else{
       var bounds = {w: 4000 - elbounds.w, h: 700 - elbounds.h};
       }

       var origin = {x: 0, y: 0},
       start = {x: 0, y: 0},
       movecontinue = true;

       function move (e){
       var inbounds = {x: false, y: false},
       offset = {
       x: start.x - (origin.x - e.clientX),
       y: start.y - (origin.y - e.clientY)
       };

       //data.value = 'X: ' + offset.x + ', Y: ' + offset.y;

       inbounds.x = offset.x < 0 && (offset.x * -1) < bounds.w;
       inbounds.y = offset.y < 0 && (offset.y * -1) < bounds.h;

       if (movecontinue && inbounds.x && inbounds.y) {
       start.x = offset.x;
       start.y = offset.y;

       $(this).css('background-position', start.x + 'px ' + start.y + 'px');
       }

       origin.x = e.clientX;
       origin.y = e.clientY;

       e.stopPropagation();
       return false;
       }

       function handle (e){
       movecontinue = true;
       $bg.unbind('mousemove', move);

       if (e.type == 'mousedown') {
       origin.x = e.clientX;
       origin.y = e.clientY;
       movecontinue = true;
       $bg.bind('mousemove', move);
       } else {
       $(document.body).focus();
       }

       e.stopPropagation();
       return false;
       }

       function reset (){
       start = {x: 0, y: 0};
       $(this).css('backgroundPosition', '0 0');
       }

       $bg.bind('mousedown mouseup mouseleave', handle);
       //$bg.bind('dblclick', reset);
       */

      // FRECCE SLIDER - NON SERVONO AL MOMENTO
      /*
       var elemento_corrente = $(".slick-current");
       var position          = elemento_corrente.position();
       var larghezza         = elemento_corrente.width() - 140;

       console.log('CURRRENT INIT', elemento_corrente, position, larghezza);

       $(".slick-prev").offset({top: offset.top, left: larghezza});
       $(".slick-next").offset({top: offset.top, left: larghezza + 60});

       // On before slide change
       $('.home-carousel').on('afterChange', function (event, slick, currentSlide) {

       elemento_corrente = $('.home-carousel').find('.slick-current');
       position          = elemento_corrente.position();
       offset            = elemento_corrente.offset();
       larghezza         = elemento_corrente.width() - 140;

       console.log(currentSlide);
       console.log('larghezza', larghezza);
       console.log('width', elemento_corrente.width());
       console.log('position', position);
       console.log('offset', offset);

       $(".slick-prev").offset({top: offset.top, left: larghezza});
       $(".slick-next").offset({top: offset.top, left: larghezza + 60});

       });
       */

      /*
       $('.scrollto').click(function() {
       if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
       var target = $(this.hash);
       target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
       if (target.length) {
       $('html,body').animate({
       scrollTop: target.offset().top
       }, 1000);
       return false;
       }
       }
       });
       */

    }
  }

})(jQuery);;/**/
var ticketFunction;

(function ($) {

  Drupal.behaviors.search_experience = {
    attach: function (context, settings) {

      ticketFunction = function () {

        var $modalInfo    = $('#modalInfosMessages');
        var $modalConfirm = $('#modalInfosMessagesConfirm');

        var guests_experience_modal       = $('#hotel_experience_front_guests_modal');
        var rooms_experience_modal        = $('#hotel_experience_front_rooms_modal');
        var calendar_experience_modal     = $('#hotel-experience-calendars');
        var $select_experience_rooms      = rooms_experience_modal.find('#select_rooms');
        var $rooms_experience_occupancy   = rooms_experience_modal.find('.rooms-occupancy');
        var $children_experience_selector = rooms_experience_modal.find('.children-age-selector');
        var rooms_experience_total        = 0;

        var totalOccupation = {
          hotel_ticket_form           : {
            adult: 0,
            child: 0
          },
          hotel_form                  : {
            adult: 0,
            child: 0
          },
          "hotel_ticket_form-attached": {
            adult: 0,
            child: 0
          },
          "hotel_form-attached"       : {
            adult: 0,
            child: 0
          }
        };

        var guests_hotel_modal       = $('#hotel_front_guests_modal');
        var rooms_hotel_modal        = $('#hotel_front_rooms_modal');
        var calendar_hotel_modal     = $('#hotel-calendars');
        var $select_hotel_rooms      = rooms_hotel_modal.find('#select_rooms');
        var $rooms_hotel_occupancy   = rooms_hotel_modal.find('.rooms-occupancy');
        var $children_hotel_selector = rooms_hotel_modal.find('.children-age-selector');
        var rooms_hotel_total        = 0;

        var navigate_experience = 0;
        var navigate_hotel      = 0;

        var navigate_experience_children = {};
        var navigate_hotel_children      = {};

        //MODAL ON INPUT FIELDS
        //DATE
        $('#date-hotel-experience, #date-hotel-experience-attached').focus(function () {
          calendar_experience_modal.modal('show');
        });

        if ($('html').attr('dir') === 'rtl') {
          $.datepicker.setDefaults($.datepicker.regional["ar"]);
        } else {
          $.datepicker.setDefaults($.datepicker.regional["en"]);
        }

        var he_datein  = '';
        var he_dateout = '';
        var h_datein   = '';
        var h_dateout  = '';

        $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

          console.log("TAB", $(this), $(this).parent());

          if ($(this).parent().hasClass('disabled')) {

            console.log("IS DISABLED");

            e.preventDefault();
            e.stopPropagation();

            return false;
          }
        });

        $("#checkin-hotel-experience, #checkin-hotel-experience-attached", context).datepicker({
          minDate   : "+1d",
          dateFormat: "d M yy",
          altFormat : "dd mm yy",
          altField  : $("[id^='date_hotel_experience_checkin']"),
          onSelect  : function (selectedDate) {

            he_datein = selectedDate;

            if (he_dateout != '') {
              selectedDate += ' - ' + he_dateout;
            }

            $('#date-hotel-experience, #date-hotel-experience-attached').val(selectedDate);

            var date = $(this).datepicker('getDate');
            if (date) {
              /*date.setDate(date.getDate() + 1);
              $('#checkout-hotel-experience, #checkout-hotel-experience-attached').datepicker("option", "minDate", date);*/

              var $checkoutDP  = $('#checkout-hotel-experience, #checkout-hotel-experience-attached');
              var checkoutDate = $checkoutDP.datepicker('getDate');

              date.setDate(date.getDate() + 1);
              $checkoutDP.datepicker("option", "minDate", date);

              if (date > checkoutDate) {
                $checkoutDP.datepicker("setDate", date);
                $checkoutDP.find('.ui-datepicker-current-day').click();
              }
            }

            $('#tabs_hotel_experience > li.disabled, #tabs_hotel_experience-attached > li.disabled').removeClass('disabled');

            $('#tabs_hotel_experience a[href="#check-experience-out"], #tabs_hotel_experience-attached a[href="#check-experience-out-attached"]').tab('show');

            calendar_experience_modal.find('.ui-datepicker-close.btn-ok').css('display', 'inline-block');
          }
        });

        $("#checkin-hotel-experience, #checkin-hotel-experience-attached").datepicker('setDate', null);

        $('#checkout-hotel-experience, #checkout-hotel-experience-attached').datepicker({
          minDate   : "+1d",
          dateFormat: "d M yy",
          altFormat : "dd mm yy",
          altField  : $("[id^='date_hotel_experience_checkout']"),
          onSelect  : function (selectedDate) {

            he_dateout = selectedDate;

            if (he_datein != '') {
              selectedDate = he_datein + ' - ' + he_dateout;
            }

            $('#date-hotel-experience, #date-hotel-experience-attached').val(selectedDate);

            /*var date = $(this).datepicker('getDate');
             if (date) {
             date.setDate(date.getDate() - 1);
             $('#checkin-hotel-experience, #checkin-hotel-experience-attached').datepicker("option", "maxDate", date);
             }*/
          }
        });

        $('#checkout-hotel-experience, #checkout-hotel-experience-attached').datepicker('setDate', null);

        /**
         * SLIDER CHILDREN
         */
        rooms_experience_modal.find(".slider-child-age").slider({
          range : "min",
          value : Math.floor((MAX_CHILD_AGE / 2) + (MIN_CHILD_AGE / 2)),
          min   : MIN_CHILD_AGE,
          max   : MAX_CHILD_AGE,
          isRTL : $('html').prop('dir') == 'rtl',
          step  : 1,
          create: function (event, ui) {

            var baseValue = $(this).slider('value');

            //console.log("SLIDER BASE VALUE", baseValue, $(this).find('.ui-slider-handle'), $(this).find('#child-age-label-' + baseValue));

            $(this).find('.ui-slider-handle').attr('data-range', baseValue);
            //console.log("CHILD AGE SELECTED", $(this).find('.child-age-label'), $(this).find('#child-age-label-7'));
            $(this).find('#child-age-label-' + baseValue).css('opacity', 1);
          },
          slide : function (event, ui) {
            $(ui.handle).attr('data-range', ui.value);
            $(this).find('.child-age-label').css('opacity', 0.5);
            $(this).find('#child-age-label-' + ui.value).css('opacity', 1);

            $('form[id^="hotel_ticket_form"]').find('input[name="room_' + navigate_experience + '_childage_' + navigate_experience_children[navigate_experience] + '"]').val(ui.value);
            $('form[id^="hotel_ticket_form-attached"]').find('input[name="room_' + navigate_experience + '_childage_' + navigate_experience_children[navigate_experience] + '"]').val(ui.value);
          }
        }).each(function () {

          var opt  = $(this).data().uiSlider.options;
          var vals = opt.max - opt.min;

          for (var i = 0; i <= vals; i++) {

            var positionLeft = (i / vals * 100) + '%';

            if ($('html').prop('dir') == 'rtl') {
              positionLeft = (100 - (i / vals * 100)) + '%';
            }

            var el = $('<label class="child-age-label" id="child-age-label-' + (i + opt.min) + '">' + Drupal.t((i + opt.min)) + '</label>').css('left', positionLeft);

            /*var el = $('<label class="child-age-label child-age-label-' + i + '" id="child-age-label-' + i + '">' + (i + opt.min) + '</label>').css('left', (i / vals * 100) + '%');*/

            $(this).append(el);
          }

        });

        rooms_hotel_modal.find(".slider-child-age").slider({
          range : "min",
          value : Math.floor((MAX_CHILD_AGE / 2) + (MIN_CHILD_AGE / 2)),
          min   : MIN_CHILD_AGE,
          max   : MAX_CHILD_AGE,
          isRTL : $('html').prop('dir') == 'rtl',
          step  : 1,
          create: function (event, ui) {

            var baseValue = $(this).slider('value');

            $(this).find('.ui-slider-handle').attr('data-range', baseValue);
            $(this).find('#child-age-label-' + baseValue).css('opacity', 1);
          },
          slide : function (event, ui) {
            $(ui.handle).attr('data-range', ui.value);
            $(this).find('.child-age-label').css('opacity', 0.5);
            $(this).find('#child-age-label-' + ui.value).css('opacity', 1);

            $('form[id^="hotel_form"]').find('input[name="room_' + navigate_hotel + '_childage_' + navigate_hotel_children[navigate_hotel] + '"]').val(ui.value);
            $('form[id^="hotel_form-attached"]').find('input[name="room_' + navigate_hotel + '_childage_' + navigate_hotel_children[navigate_hotel] + '"]').val(ui.value);
          }
        }).each(function () {

          var opt  = $(this).data().uiSlider.options;
          var vals = opt.max - opt.min;

          for (var i = 0; i <= vals; i++) {

            var positionLeft = (i / vals * 100) + '%';

            if ($('html').prop('dir') == 'rtl') {
              positionLeft = (100 - (i / vals * 100)) + '%';
            }

            var el = $('<label class="child-age-label" id="child-age-label-' + (i + opt.min) + '">' + Drupal.t((i + opt.min)) + '</label>').css('left', positionLeft);

            $(this).append(el);
          }

        });

        //GUESTS

        /**
         * Substract Guest
         */
        guests_experience_modal.find('.minus').click(function (e) {

          e.preventDefault();
          e.stopPropagation();

          var guest_type    = $(this).data('guest');
          var $guest_number = guests_experience_modal.find('#' + guest_type + '_number');
          var guest_number  = isNumeric(parseInt($guest_number.text())) ? parseInt($guest_number.text()) : 0;

          if (guest_type == 'adult' && guest_number == MIN_ADULT_GUEST) {

            var $child_number = guests_experience_modal.find('#child_number');
            var child_number  = isNumeric(parseInt($child_number.text())) ? parseInt($child_number.text()) : 0;

            if (child_number > 0) {

              $modalInfo.find('.messageTxt').text(Drupal.t(messages['errorMinAdultGuest']));
              $modalInfo.modal("show");
              return false;
            }
          }

          if (guest_number > 0) {
            guest_number--;
            rooms_experience_total--;
            $guest_number.text(guest_number);

            updateFrontHotelGuests(guests_experience_modal, 'hotel_ticket_form', 'hotel_ticket_form-attached');
            updateFrontRooms(rooms_experience_modal, 'hotel_ticket_form', 'hotel_ticket_form-attached', navigate_experience);
          }
        });

        /**
         * Add guest
         */
        guests_experience_modal.find('.plus').click(function (e) {

          e.preventDefault();
          e.stopPropagation();

          var guest_type    = $(this).data('guest');
          var $guest_number = guests_experience_modal.find('#' + guest_type + '_number');
          var guest_number  = isNumeric(parseInt($guest_number.text())) ? parseInt($guest_number.text()) : 0;

          if (guest_type == 'child') {
            var $adult_number = guests_experience_modal.find('#adult_number');
            var adult_number  = isNumeric(parseInt($adult_number.text())) ? parseInt($adult_number.text()) : 0;

            if (!adult_number) {
              adult_number = MIN_ADULT_GUEST;
              rooms_experience_total++;
              $adult_number.text(adult_number);
            }
          }

          if (rooms_experience_total >= MAX_HOTEL_GUESTS) {

            var args = {
              '@nbGuests': MAX_HOTEL_GUESTS
            };
            //guests_experience_modal.modal("hide");
            $modalInfo.find('.messageTxt').text(Drupal.t(messages['errorQtyGuests'], args));
            $modalInfo.modal("show");
            return false;
          }
          guest_number++;
          rooms_experience_total++;
          $guest_number.text(guest_number);

          updateFrontHotelGuests(guests_experience_modal, 'hotel_ticket_form', 'hotel_ticket_form-attached');
          updateFrontRooms(rooms_experience_modal, 'hotel_ticket_form', 'hotel_ticket_form-attached', navigate_experience);
        });

        /**
         * Update ui guests
         * by ex., editing guests number on editing summary
         * @param $modal
         * @param formId
         */
        function updateFrontHotelGuests($modal, formId, formId2) {

          var $form         = $('#' + formId);
          var $form2        = $('#' + formId2);
          var $adult_number = $modal.find('#adult_number');
          var adult_number  = isNumeric(parseInt($adult_number.text())) ? parseInt($adult_number.text()) : 0;
          var $child_number = $modal.find('#child_number');
          var child_number  = isNumeric(parseInt($child_number.text())) ? parseInt($child_number.text()) : 0;
          var guest_title   = "";

          // console.log(adult_number, child_number, $modal, $form);

          if (adult_number > 0) {
            guest_title += adult_number + ' ';
            guest_title += Drupal.formatPlural(adult_number, Drupal.t('Adult'), Drupal.t('Adults'));
          }
          if (child_number > 0) {
            if (guest_title != "") {
              guest_title += ' - ';
            }
            guest_title += child_number + ' ';
            guest_title += Drupal.formatPlural(child_number, Drupal.t('Child'), Drupal.t('Children'));
          }

          var $guest_container = $form.find('input.guest');
          var $guest_adult     = $form.find('input[name="guest_adult"]');
          var $guest_child     = $form.find('input[name="guest_child"]');

          var $guest_container2 = $form2.find('input.guest');
          var $guest_adult2     = $form2.find('input[name="guest_adult"]');
          var $guest_child2     = $form2.find('input[name="guest_child"]');

          $guest_container.val(guest_title);
          $guest_adult.val(adult_number);
          $guest_child.val(child_number);

          $guest_container2.val(guest_title);
          $guest_adult2.val(adult_number);
          $guest_child2.val(child_number);
        }

        $('#guest-hotel-experience,#guest-hotel-experience-attached').focus(function () {
          $('#hotel_experience_front_guests_modal').modal('show');
        });

        //ROOMS
        $('#rooms-hotel-experience,#rooms-hotel-experience-attached').focus(function () {
          $('#hotel_experience_front_rooms_modal').modal('show');
        });

        /**
         * Substract room
         */
        $select_experience_rooms.find('.minus').click(function (e) {

          e.preventDefault();
          e.stopPropagation();

          var $rooms_number = rooms_experience_modal.find('#rooms_numbers');
          var room_number   = isNumeric(parseInt($rooms_number.text())) ? parseInt($rooms_number.text()) : 0;

          if (room_number > 0) {

            var lastRoomNb        = room_number - 1;
            var $form             = $('#hotel_ticket_form');
            var $form2            = $('#hotel_ticket_form-attached');
            var $adult_num_input  = $form.find('input[name="room_' + lastRoomNb + '_adult"]');
            var $child_num_input  = $form.find('input[name="room_' + lastRoomNb + '_child"]');
            var $adult_num_input2 = $form.find('input[name="room_' + lastRoomNb + '_adult"]');
            var $child_num_input2 = $form.find('input[name="room_' + lastRoomNb + '_child"]');
            var adult_num         = $adult_num_input.val();
            var child_num         = $child_num_input.val();

            totalOccupation['hotel_ticket_form']['adult'] -= adult_num;
            totalOccupation['hotel_ticket_form']['child'] -= child_num;
            totalOccupation['hotel_ticket_form-attached']['adult'] -= adult_num;
            totalOccupation['hotel_ticket_form-attached']['child'] -= child_num;

            $adult_num_input.remove();
            $adult_num_input2.remove();
            $child_num_input.remove();
            $child_num_input2.remove();

            room_number--;
            $rooms_number.text(room_number);

            if (navigate_experience > (room_number - 1)) {
              navigate_experience = (room_number - 1);
            }

            $form.find('input[name="nb_rooms"]').val(room_number);
            $form2.find('input[name="nb_rooms"]').val(room_number);

            updateFrontRooms(rooms_experience_modal, 'hotel_ticket_form', 'hotel_ticket_form-attached', navigate_experience);
          }
        });

        /**
         * Add room
         */
        $select_experience_rooms.find('.plus').off('click').click(function (e) {

          e.preventDefault();
          e.stopPropagation();

          var $rooms_number = rooms_experience_modal.find('#rooms_numbers');
          var room_number   = isNumeric(parseInt($rooms_number.text())) ? parseInt($rooms_number.text()) : 0;
          var $form         = $('#hotel_ticket_form');
          var $form2        = $('#hotel_ticket_form-attached');

          if (room_number < MAX_ROOMS) {
            room_number++;
            console.log("TOTAL ADULT BEFORE", totalOccupation['hotel_ticket_form'].adult);
            totalOccupation['hotel_ticket_form'].adult++;
            totalOccupation['hotel_ticket_form-attached'].adult++;
            $rooms_number.text(room_number);

            console.log("TOTAL ADULT", totalOccupation['hotel_ticket_form'].adult);

            $form.find('input[name="nb_rooms"]').val(room_number);
            $form2.find('input[name="nb_rooms"]').val(room_number);

            updateFrontRooms(rooms_experience_modal, 'hotel_ticket_form', 'hotel_ticket_form-attached', navigate_experience);
          }
          else {

            var args = {
              '@maxRooms': MAX_ROOMS
            };

            $modalInfo.find('.messageTxt').text(Drupal.t(messages['errorSearchMaxRooms'], args));
            $modalInfo.modal("show");
            return false;
          }
        });

        /**
         * Next room
         */
        $rooms_experience_occupancy.find('.room-next').click(function (e) {
          e.preventDefault();
          e.stopPropagation();

          var $rooms_number = rooms_experience_modal.find('#rooms_numbers');
          var room_number   = isNumeric(parseInt($rooms_number.text())) ? parseInt($rooms_number.text()) : 0;

          // console.log('NAVIGATE', navigate_experience, room_number);

          if ((navigate_experience + 1) < room_number) {
            navigate_experience++;
            navigate_experience_children[navigate_experience] = 0;
          }

          updateFrontRooms(rooms_experience_modal, 'hotel_ticket_form', 'hotel_ticket_form-attached', navigate_experience);
        });

        /**
         * Prev room
         */
        $rooms_experience_occupancy.find('.room-prev').click(function (e) {
          e.preventDefault();
          e.stopPropagation();

          var $rooms_number = rooms_experience_modal.find('#rooms_numbers');
          var room_number   = isNumeric(parseInt($rooms_number.text())) ? parseInt($rooms_number.text()) : 0;

          // console.log('NAVIGATE', navigate_experience, room_number);

          if ((navigate_experience - 1) >= 0) {
            navigate_experience--;
            navigate_experience_children[navigate_experience] = 0;
          }

          updateFrontRooms(rooms_experience_modal, 'hotel_ticket_form', 'hotel_ticket_form-attached', navigate_experience);
        });

        /**
         * Substract Guest from Selected Room
         */
        $rooms_experience_occupancy.find('.minus').click(function (e) {

          e.preventDefault();
          e.stopPropagation();

          var guest_type = $(this).data('guest');

          var $form                  = $('#hotel_ticket_form');
          var $form2                 = $('#hotel_ticket_form-attached');
          var $guest_type_num_input  = $form.find('input[name="room_' + navigate_experience + '_' + guest_type + '"]');
          var $guest_type_num_input2 = $form2.find('input[name="room_' + navigate_experience + '_' + guest_type + '"]');
          var guest_type_num         = $guest_type_num_input.val();
          var guest_type_num2        = $guest_type_num_input2.val();

          if (guest_type == 'adult' && guest_type_num == MIN_ADULT_GUEST) {
            $modalInfo.find('.messageTxt').text(Drupal.t(messages['errorMinAdultRoomGuest']));
            $modalInfo.modal("show");
            return false;
          }

          if (guest_type_num > 0) {
            guest_type_num--;
            totalOccupation['hotel_ticket_form'][guest_type]--;
            totalOccupation['hotel_ticket_form-attached'][guest_type]--;

            $guest_type_num_input.val(guest_type_num);
            $guest_type_num_input2.val(guest_type_num2);

            if (guest_type == 'child') {

              var $childAgeInputs = $('input[name^="room_' + navigate_experience + '_childage_"]');

              if (navigate_experience_children[navigate_experience] >= guest_type_num) {
                navigate_experience_children[navigate_experience]--;
              }

              console.log('NAV EXP', navigate_experience, $childAgeInputs.length, $childAgeInputs);
              if ($childAgeInputs.length) {
                $form.find('input[name^="room_' + navigate_experience + '_childage_"]').last().remove();
                $form2.find('input[name^="room_' + navigate_experience + '_childage_"]').last().remove();
                console.log('CHILDAGE REM', $('input[name^="room_' + navigate_experience + '_childage_"]').length, $('input[name^="room_' + navigate_experience + '_childage_"]'));
              }
            }

            //rooms_experience_modal.find('#room_occupancy_' + guest_type).text(guest_type_num);

            updateFrontRooms(rooms_experience_modal, 'hotel_ticket_form', 'hotel_ticket_form-attached', navigate_experience);
          }

        });

        /**
         * Add Guest from Selected Room
         */
        $rooms_experience_occupancy.find('.plus').click(function (e) {

          e.preventDefault();
          e.stopPropagation();

          var guest_type = $(this).data('guest');

          var $form                 = $('#hotel_ticket_form');
          var $form2                = $('#hotel_ticket_form-attached');
          var $guest_type_max_input = $form.find('input[name="guest_' + guest_type + '"]');
          //var $guest_type_max_input2 = $form2.find('input[name="guest_' + guest_type + '"]');
          var $guest_type_num_input  = $form.find('input[name="room_' + navigate_experience + '_' + guest_type + '"]');
          var $guest_type_num_input2 = $form2.find('input[name="room_' + navigate_experience + '_' + guest_type + '"]');
          var guest_type_max         = $guest_type_max_input.val();
          var guest_type_num         = $guest_type_num_input.val();
          /*var guest_type_max2        = $guest_type_max_input2.val();
           var guest_type_num2        = $guest_type_num_input2.val();*/

          console.log('Occupation', totalOccupation['hotel_ticket_form'][guest_type], guest_type_num, guest_type_max, navigate_experience);

          if (totalOccupation['hotel_ticket_form'][guest_type] < guest_type_max) {
            guest_type_num++;
            totalOccupation['hotel_ticket_form'][guest_type]++;
            totalOccupation['hotel_ticket_form-attached'][guest_type]++;

            if (guest_type == 'child' && !navigate_experience_children.hasOwnProperty(navigate_experience)) {
              navigate_experience_children[navigate_experience] = 0;
            }

            $guest_type_num_input.val(guest_type_num);
            $guest_type_num_input2.val(guest_type_num);

            //rooms_experience_modal.find('#room_occupancy_' + guest_type).text(guest_type_num);
            /*$form.find('input[name=total_occupation_' + guest_type + ']').val(totalOccupation['hotel_ticket_form'][guest_type]);
             $form2.find('input[name=total_occupation_' + guest_type + ']').val(totalOccupation['hotel_ticket_form'][guest_type]);*/

            updateFrontRooms(rooms_experience_modal, 'hotel_ticket_form', 'hotel_ticket_form-attached', navigate_experience);
          }
        });

        /**
         * Update ui rooms
         * @param $modal
         * @param formId
         * @param formId2
         * @param navigate
         */
        function updateFrontRooms($modal, formId, formId2, navigate) {

          var $form              = $('#' + formId);
          var $form2             = $('#' + formId2);
          var $rooms_occupancy   = $modal.find('.rooms-occupancy');
          var $room_next         = $rooms_occupancy.find('.room-next');
          var $room_prev         = $rooms_occupancy.find('.room-prev');
          var $rooms_number      = $modal.find('#rooms_numbers');
          var rooms_num          = isNumeric(parseInt($rooms_number.text())) ? parseInt($rooms_number.text()) : 0;
          var rooms_title        = "";
          var $children_selector = $modal.find('.children-age-selector');
          var $child_next        = $children_selector.find('.child-next');
          var $child_prev        = $children_selector.find('.child-prev');
          var navigate_children  = navigate_experience_children;

          if (formId == 'hotel_form') {
            navigate_children = navigate_hotel_children;
          }

          //$rooms_number.html(rooms_num);

          if (rooms_num > 0) {
            rooms_title += rooms_num + ' ';
            rooms_title += Drupal.formatPlural(rooms_num, Drupal.t('Room'), Drupal.t('Rooms'));

            $modal.find('#rooms_list').text(Drupal.t('Room') + ' ' + (navigate + 1) + '/' + rooms_num);

            if ($rooms_occupancy.css('display') == 'none') {
              TweenLite.to($rooms_occupancy, 0.25, {autoAlpha: 1, display: 'block'});
            }

            $form.find('input[name="room"]').val(rooms_title);
            $form2.find('input[name="room"]').val(rooms_title);

            for (var i = 0; i < rooms_num; i++) {
              if (!$form.find('input[name="room_' + i + '_adult"]').length) {
                $form.find('#rooms_occupation').append('<input type="hidden" name="room_' + i + '_adult" value="1">');
                $form2.find('#rooms_occupation-attached').append('<input type="hidden" name="room_' + i + '_adult" value="1">');
                /*totalOccupation[formId].adult++;
                 totalOccupation[formId2].adult++;*/
              }
              if (!$form.find('input[name="room_' + i + '_child"]').length) {
                $form.find('#rooms_occupation').append('<input type="hidden" name="room_' + i + '_child" value="0">');
                $form2.find('#rooms_occupation-attached').append('<input type="hidden" name="room_' + i + '_child" value="0">');
              }

              var adult_num = $form.find('input[name="room_' + i + '_adult"]').val();
              var child_num = $form.find('input[name="room_' + i + '_child"]').val();

              $modal.find('#room_occupancy_adult').text(adult_num);
              $modal.find('#room_occupancy_child').text(child_num);

              //console.log("NUM CHILD FOR ROOM " + i, child_num);

              for (var j = 0; j < child_num; j++) {

                //console.log("ROOM " + i + ", CHILD " + j, $form.find('input[name="room_' + i + '_childage_' + j + '"]').length);

                if (!$form.find('input[name="room_' + i + '_childage_' + j + '"]').length) {
                  $form.find('#rooms_occupation').append('<input type="hidden" name="room_' + i + '_childage_' + j + '" value="7">');
                  //console.log("FORM2 OCCUPATION", $form2.find('#rooms_occupation-attached'));
                  $form2.find('#rooms_occupation-attached').append('<input type="hidden" name="room_' + i + '_childage_' + j + '" value="7">');
                }
              }
            }

            //console.log("TOTAL OCCUPATION", totalOccupation);

            $form.find('input[name=total_occupation_adult]').val(totalOccupation[formId].adult);
            $form2.find('input[name=total_occupation_adult]').val(totalOccupation[formId2].adult);
            $form.find('input[name=total_occupation_child]').val(totalOccupation[formId].child);
            $form2.find('input[name=total_occupation_child]').val(totalOccupation[formId2].child);

            /**
             * Children
             */

            var nav_child_num = parseInt($form.find('input[name="room_' + navigate + '_child"]').val());

            refreshAge($modal, formId, formId2, navigate);

            if (isNumeric(nav_child_num) && nav_child_num) {
              if ($children_selector.css('display') == 'none') {
                TweenLite.to($children_selector, 0.25, {autoAlpha: 1, display: 'block'});
              }

              $children_selector.find('.child-number').text(Drupal.t('Child') + ' ' + (navigate_children[navigate] + 1) + '/' + nav_child_num);

            }
            else {
              if ($children_selector.css('display') != 'none') {
                TweenLite.to($children_selector, 0.25, {autoAlpha: 0, display: 'none'});
              }
            }

            //console.log("SHOW HIDE", nav_child_num, $children_selector, (navigate_children[navigate] + 1), navigate_children[navigate]);

            if (nav_child_num > (navigate_children[navigate] + 1)) {
              $child_next.removeClass('disabled')
            }
            else {
              if (!$child_next.hasClass('disabled')) {
                $child_next.addClass('disabled');
              }
            }

            if (navigate_children[navigate] > 0) {
              $child_prev.removeClass('disabled')
            }
            else {
              if (!$child_prev.hasClass('disabled')) {
                $child_prev.addClass('disabled');
              }
            }

            /**
             * Rooms
             */
            if (rooms_num > (navigate + 1)) {
              $room_next.removeClass('disabled');
            }
            else {
              if (!$room_next.hasClass('disabled')) {
                $room_next.addClass('disabled');
              }
            }

            if (navigate > 0) {
              $room_prev.removeClass('disabled');
            }
            else {
              if (!$room_prev.hasClass('disabled')) {
                $room_prev.addClass('disabled');
              }
            }

            var $guest_adult_number = $form.find('input[name="guest_adult"]');
            var guest_adult_number  = isNumeric(parseInt($guest_adult_number.val())) ? parseInt($guest_adult_number.val()) : 0;

            var $guest_child_number = $form.find('input[name="guest_child"]');
            var guest_child_number  = isNumeric(parseInt($guest_child_number.val())) ? parseInt($guest_child_number.val()) : 0;

            console.log("GUEST ADULT", guest_adult_number);
            console.log("GUEST CHILD", guest_child_number);

            if (totalOccupation[formId].adult < guest_adult_number) {
              $rooms_occupancy.find('.plus[data-guest=adult]').removeClass('disabled');
            }
            else {
              if (!$rooms_occupancy.find('.plus[data-guest=adult]').hasClass('disabled')) {
                $rooms_occupancy.find('.plus[data-guest=adult]').addClass('disabled');
              }
            }

            console.log("VERIF DISABLED", totalOccupation[formId].child, guest_child_number);

            if (totalOccupation[formId].child < guest_child_number) {
              $rooms_occupancy.find('.plus[data-guest=child]').removeClass('disabled');
            }
            else {
              if (!$rooms_occupancy.find('.plus[data-guest=child]').hasClass('disabled')) {
                $rooms_occupancy.find('.plus[data-guest=child]').addClass('disabled');
              }
            }

            console.log("NVIGATE IS ", navigate);

            var navigate_adult = $form.find('input[name="room_' + navigate + '_adult"]').val();
            var navigate_child = $form.find('input[name="room_' + navigate + '_child"]').val();

            if (navigate_adult > 0) {
              $rooms_occupancy.find('.minus[data-guest=adult]').removeClass('disabled');
            }
            else {
              if (!$rooms_occupancy.find('.minus[data-guest=adult]').hasClass('disabled')) {
                $rooms_occupancy.find('.minus[data-guest=adult]').addClass('disabled');
              }
            }

            if (navigate_child > 0) {
              $rooms_occupancy.find('.minus[data-guest=child]').removeClass('disabled');
            }
            else {
              if (!$rooms_occupancy.find('.minus[data-guest=child]').hasClass('disabled')) {
                $rooms_occupancy.find('.minus[data-guest=child]').addClass('disabled');
              }
            }

            $rooms_occupancy.find('#room_occupancy_adult').text(navigate_adult);
            $rooms_occupancy.find('#room_occupancy_child').text(navigate_child);
          }
          else {

            $form.find('input[name="room"]').val(rooms_title);
            $form2.find('input[name="room"]').val(rooms_title);

            if ($rooms_occupancy.css('display') != 'none') {
              TweenLite.to($rooms_occupancy, 0.25, {autoAlpha: 0, display: 'none'});
            }
          }

          var $guest_container = $('.select_rooms');
          $guest_container.text(rooms_title);
        }

        /**
         * Next Child
         */
        rooms_experience_modal.find('.child-next').click(function (e) {
          e.preventDefault();
          e.stopPropagation();

          var $form     = $('#hotel_ticket_form');
          var num_child = $form.find('input[name="room_' + navigate_experience + '_child"]').val();

          console.log("CHILD NEXT BEFORE", $form, $form.find('room_' + navigate_hotel + '_child'));

          if ((navigate_experience_children[navigate_experience] + 1) < num_child) {
            navigate_experience_children[navigate_experience]++;

            console.log("NAVIGATE CHILDREN", navigate_experience_children[navigate_experience]);

            //refreshAge(rooms_experience_modal, 'hotel_ticket_form', 'hotel_ticket_form-attached', navigate_experience);
          }

          updateFrontRooms(rooms_experience_modal, 'hotel_ticket_form', 'hotel_ticket_form-attached', navigate_experience);
        });

        /**
         * Prev Child
         */
        rooms_experience_modal.find('.child-prev').click(function (e) {
          e.preventDefault();
          e.stopPropagation();

          if ((navigate_experience_children[navigate_experience] - 1) >= 0) {
            navigate_experience_children[navigate_experience]--;

            //refreshAge(rooms_experience_modal, 'hotel_ticket_form', 'hotel_ticket_form-attached', navigate_experience);
          }

          updateFrontRooms(rooms_experience_modal, 'hotel_ticket_form', 'hotel_ticket_form-attached', navigate_experience);
        });

        /**
         * Next Child
         */
        rooms_hotel_modal.find('.child-next').click(function (e) {
          e.preventDefault();
          e.stopPropagation();

          var $form     = $('#hotel_form');
          var num_child = $form.find('input[name="room_' + navigate_hotel + '_child"]').val();

          console.log("CHILD NEXT BEFORE", $form, $form.find('room_' + navigate_hotel + '_child'));

          if ((navigate_hotel_children[navigate_hotel] + 1) < num_child) {
            navigate_hotel_children[navigate_hotel]++;
            console.log("CHILD NEXT", navigate_hotel_children[navigate_hotel]);

            //refreshAge(rooms_hotel_modal, 'hotel_form', 'hotel_form-attached', navigate_hotel);
          }

          updateFrontRooms(rooms_hotel_modal, 'hotel_form', 'hotel_form-attached', navigate_hotel);
        });

        /**
         * Prev Child
         */
        rooms_hotel_modal.find('.child-prev').click(function (e) {
          e.preventDefault();
          e.stopPropagation();

          if ((navigate_hotel_children[navigate_hotel] - 1) >= 0) {
            navigate_hotel_children[navigate_hotel]--;

            //refreshAge(rooms_hotel_modal, 'hotel_form', 'hotel_form-attached', navigate_hotel);
          }

          updateFrontRooms(rooms_hotel_modal, 'hotel_form', 'hotel_form-attached', navigate_hotel);
        });

        function refreshAge($modal, formId, formId2, navigate) {

          var $form             = $('#' + formId);
          var $form2            = $('#' + formId2);
          var navigate_children = navigate_experience_children;

          if (formId == 'hotel_form') {
            navigate_children = navigate_hotel_children;
          }

          var $childage  = $form.find('input[name="room_' + navigate + '_childage_' + navigate_children[navigate] + '"]');
          var $childage2 = $form2.find('input[name="room_' + navigate + '_childage_' + navigate_children[navigate] + '"]');

          console.log("ON REFRESH", $form, $childage);

          if ($childage.length) {
            var age     = $childage.val();
            var $slider = $modal.find(".slider-child-age");

            $slider.slider({
              value: age
            });

            $slider.find('.ui-slider-handle').attr('data-range', age);
            $slider.find('.child-age-label').css('opacity', 0.5);
            $slider.find('#child-age-label-' + age).css('opacity', 1);
          }
        }

        //HOTEL

        //DATE
        $('#date-hotel, #date-hotel-attached').focus(function () {
          calendar_hotel_modal.modal('show');
        });

        $("#checkin-hotel, #checkin-hotel-attached").datepicker({
          minDate   : "+1d",
          dateFormat: "d M yy",
          altFormat : "dd mm yy",
          altField  : $("[id^='date_hotel_checkin']"),
          onSelect  : function (selectedDate) {

            h_datein = selectedDate;

            if (h_dateout != '') {
              selectedDate += ' - ' + h_dateout;
            }

            $('#date-hotel, #date-hotel-attached').val(selectedDate);

            var date = $(this).datepicker('getDate');
            if (date) {
              date.setDate(date.getDate() + 1);
              $('#checkout-hotel, #checkout-hotel-attached').datepicker("option", "minDate", date);
            }

            $('#tabs_hotel > li.disabled, #tabs_hotel-attached > li.disabled').removeClass('disabled');
            $('#tabs_hotel a[href="#check-out"], #tabs_hotel-attached a[href="#check-out-attached"]').tab('show');

            calendar_hotel_modal.find('.ui-datepicker-close.btn-ok').css('display', 'inline-block');
          }
        });

        $("#checkin-hotel, #checkin-hotel-attached").datepicker('setDate', null);

        $('#checkout-hotel, #checkout-hotel-attached').datepicker({
          minDate   : "+1d",
          dateFormat: "d M yy",
          altFormat : "dd mm yy",
          altField  : $("[id^='date_hotel_checkout']"),

          onSelect: function (selectedDate) {

            h_dateout = selectedDate;

            if (h_datein != '') {
              selectedDate = h_datein + ' - ' + h_dateout;
            }

            $('#date-hotel, #date-hotel-attached').val(selectedDate);

            var date = $(this).datepicker('getDate');

            if (date) {
              date.setDate(date.getDate() - 1);
              $('#checkin-hotel, #checkin-hotel-attached').datepicker("option", "maxDate", date);
            }
          }
        });

        $('#checkout-hotel, #checkout-hotel-attached').datepicker('setDate', null);

        //GUESTS
        $('#guest-hotel,#guest-hotel-attached').focus(function () {
          $('#hotel_front_guests_modal').modal('show');
        });

        /**
         * Substract Guest
         */
        guests_hotel_modal.find('.minus').click(function (e) {

          e.preventDefault();
          e.stopPropagation();

          var guest_type    = $(this).data('guest');
          var $guest_number = guests_hotel_modal.find('#' + guest_type + '_number');
          var guest_number  = isNumeric(parseInt($guest_number.text())) ? parseInt($guest_number.text()) : 0;

          if (guest_type == 'adult' && guest_number == MIN_ADULT_GUEST) {

            var $child_number = guests_hotel_modal.find('#child_number');
            var child_number  = isNumeric(parseInt($child_number.text())) ? parseInt($child_number.text()) : 0;

            if (child_number > 0) {

              $modalInfo.find('.messageTxt').text(Drupal.t(messages['errorMinAdultGuest']));
              $modalInfo.modal("show");
              return false;
            }
          }

          if (guest_number > 0) {
            guest_number--;
            rooms_hotel_total--;
            $guest_number.text(guest_number);

            updateFrontHotelGuests(guests_hotel_modal, 'hotel_form', 'hotel_form-attached');
            updateFrontRooms(rooms_hotel_modal, 'hotel_form', 'hotel_form-attached', navigate_hotel);
          }
        });

        /**
         * Add guest
         */
        guests_hotel_modal.find('.plus').click(function (e) {

          e.preventDefault();
          e.stopPropagation();

          var guest_type    = $(this).data('guest');
          var $guest_number = guests_hotel_modal.find('#' + guest_type + '_number');
          var guest_number  = isNumeric(parseInt($guest_number.text())) ? parseInt($guest_number.text()) : 0;

          if (guest_type == 'child') {
            var $adult_number = guests_hotel_modal.find('#adult_number');
            var adult_number  = isNumeric(parseInt($adult_number.text())) ? parseInt($adult_number.text()) : 0;

            if (!adult_number) {
              adult_number = MIN_ADULT_GUEST;
              rooms_hotel_total++;
              $adult_number.text(adult_number);
            }
          }

          if (rooms_hotel_total >= MAX_HOTEL_GUESTS) {

            var args = {
              '@nbGuests': MAX_HOTEL_GUESTS
            };

            $modalInfo.find('.messageTxt').text(Drupal.t(messages['errorQtyGuests'], args));
            $modalInfo.modal("show");
            return false;
          }

          guest_number++;
          rooms_hotel_total++;
          $guest_number.text(guest_number);

          updateFrontHotelGuests(guests_hotel_modal, 'hotel_form', 'hotel_form-attached');
          updateFrontRooms(rooms_hotel_modal, 'hotel_form', 'hotel_form-attached', navigate_hotel);
        });

        //ROOMS
        $('#rooms-hotel,#rooms-hotel-attached').focus(function () {
          $('#hotel_front_rooms_modal').modal('show');
        });

        /**
         * Substract room
         */
        $select_hotel_rooms.find('.minus').click(function (e) {

          e.preventDefault();
          e.stopPropagation();

          var $rooms_number = rooms_hotel_modal.find('#rooms_numbers');
          var room_number   = isNumeric(parseInt($rooms_number.text())) ? parseInt($rooms_number.text()) : 0;

          var rooms_title = '';

          if (room_number > 0) {

            var lastRoomNb        = room_number - 1;
            var $form             = $('#hotel_form');
            var $form2            = $('#hotel_form-attached');
            var $adult_num_input  = $form.find('input[name="room_' + lastRoomNb + '_adult"]');
            var $child_num_input  = $form.find('input[name="room_' + lastRoomNb + '_child"]');
            var $adult_num_input2 = $form.find('input[name="room_' + lastRoomNb + '_adult"]');
            var $child_num_input2 = $form.find('input[name="room_' + lastRoomNb + '_child"]');
            var adult_num         = $adult_num_input.val();
            var child_num         = $child_num_input.val();

            totalOccupation['hotel_form']['adult'] -= adult_num;
            totalOccupation['hotel_form']['child'] -= child_num;
            totalOccupation['hotel_form-attached']['adult'] -= adult_num;
            totalOccupation['hotel_form-attached']['child'] -= child_num;

            $adult_num_input.remove();
            $adult_num_input2.remove();
            $child_num_input.remove();
            $child_num_input2.remove();

            room_number--;
            $rooms_number.text(room_number);

            if (navigate_hotel > (room_number - 1)) {
              navigate_hotel = (room_number - 1);
            }

            $form.find('input[name="nb_rooms"]').val(room_number);
            $form2.find('input[name="nb_rooms"]').val(room_number);

            updateFrontRooms(rooms_hotel_modal, 'hotel_form', 'hotel_form-attached', navigate_hotel);
          }
        });

        /**
         * Add room
         */
        $select_hotel_rooms.find('.plus').click(function (e) {

          e.preventDefault();
          e.stopPropagation();

          var $rooms_number = rooms_hotel_modal.find('#rooms_numbers');
          var room_number   = isNumeric(parseInt($rooms_number.text())) ? parseInt($rooms_number.text()) : 0;
          var $form         = $('#hotel_form');
          var $form2        = $('#hotel_form-attached');

          if (room_number < MAX_ROOMS) {
            room_number++;
            $rooms_number.text(room_number);
            totalOccupation['hotel_form'].adult++;
            totalOccupation['hotel_form-attached'].adult++;

            /*$form.find('input[name=total_occupation_adult]').val(totalOccupation['hotel_form'].adult);
             $form2.find('input[name=total_occupation_adult]').val(totalOccupation['hotel_form-attached'].adult);*/

            $form.find('input[name="nb_rooms"]').val(room_number);
            $form2.find('input[name="nb_rooms"]').val(room_number);

            updateFrontRooms(rooms_hotel_modal, 'hotel_form', 'hotel_form-attached', navigate_hotel);
          }
          else {

            var args = {
              '@maxRooms': MAX_ROOMS
            };

            $modalInfo.find('.messageTxt').text(Drupal.t(messages['errorSearchMaxRooms'], args));
            $modalInfo.modal("show");
            return false;
          }
        });

        /**
         * Next room
         */
        $rooms_hotel_occupancy.find('.room-next').click(function (e) {
          e.preventDefault();
          e.stopPropagation();

          var $rooms_number = rooms_hotel_modal.find('#rooms_numbers');
          var room_number   = isNumeric(parseInt($rooms_number.text())) ? parseInt($rooms_number.text()) : 0;

          if ((navigate_hotel + 1) < room_number) {
            navigate_hotel++;
            navigate_hotel_children[navigate_hotel] = 0;
          }

          updateFrontRooms(rooms_hotel_modal, 'hotel_form', 'hotel_form-attached', navigate_hotel);
        });

        /**
         * Prev room
         */
        $rooms_hotel_occupancy.find('.room-prev').click(function (e) {
          e.preventDefault();
          e.stopPropagation();

          var $rooms_number = rooms_hotel_modal.find('#rooms_numbers');
          var room_number   = isNumeric(parseInt($rooms_number.text())) ? parseInt($rooms_number.text()) : 0;

          if ((navigate_hotel - 1) >= 0) {
            navigate_hotel--;
            navigate_hotel_children[navigate_hotel] = 0;
          }

          updateFrontRooms(rooms_hotel_modal, 'hotel_form', 'hotel_form-attached', navigate_hotel);
        });

        /**
         * Substract Guest from Selected Room
         */
        $rooms_hotel_occupancy.find('.minus').click(function (e) {

          /*e.preventDefault();
           e.stopPropagation();

           var guest_type = $(this).data('guest');

           var $form                 = $('#hotel_form');
           var $form2                = $('#hotel_form-attached');
           var $guest_type_num_input = $form.find('input[name="room_' + navigate_hotel + '_' + guest_type + '"]');
           var guest_type_num        = $guest_type_num_input.val();

           if (guest_type_num > 0) {
           guest_type_num--;
           totalOccupation['hotel_form'][guest_type]--;

           $guest_type_num_input.val(guest_type_num);

           rooms_hotel_modal.find('#room_occupancy_' + guest_type).text(guest_type_num);
           $form.find('input[name=total_occupation_' + guest_type + ']').val(totalOccupation['hotel_form'][guest_type]);
           $form2.find('input[name=total_occupation_' + guest_type + ']').val(totalOccupation['hotel_form'][guest_type]);

           updateFrontRooms(rooms_hotel_modal, 'hotel_form', 'hotel_form-attached', navigate_hotel);
           }*/

          e.preventDefault();
          e.stopPropagation();

          var guest_type = $(this).data('guest');

          var $form                  = $('#hotel_form');
          var $form2                 = $('#hotel_form-attached');
          var $guest_type_num_input  = $form.find('input[name="room_' + navigate_experience + '_' + guest_type + '"]');
          var $guest_type_num_input2 = $form2.find('input[name="room_' + navigate_experience + '_' + guest_type + '"]');
          var guest_type_num         = $guest_type_num_input.val();
          var guest_type_num2        = $guest_type_num_input2.val();

          if (guest_type == 'adult' && guest_type_num == MIN_ADULT_GUEST) {
            $modalInfo.find('.messageTxt').text(Drupal.t(messages['errorMinAdultRoomGuest']));
            $modalInfo.modal("show");
            return false;
          }

          if (guest_type_num > 0) {
            guest_type_num--;
            totalOccupation['hotel_ticket_form'][guest_type]--;
            totalOccupation['hotel_ticket_form-attached'][guest_type]--;

            $guest_type_num_input.val(guest_type_num);
            $guest_type_num_input2.val(guest_type_num2);

            if (guest_type == 'child') {

              var $childAgeInputs = $('input[name^="room_' + navigate_hotel + '_childage_"]');

              if (navigate_hotel_children[navigate_hotel] >= guest_type_num) {
                navigate_hotel_children[navigate_hotel]--;
              }

              console.log('NAV EXP', navigate_hotel, $childAgeInputs.length, $childAgeInputs);
              if ($childAgeInputs.length) {
                $form.find('input[name^="room_' + navigate_hotel + '_childage_"]').last().remove();
                $form2.find('input[name^="room_' + navigate_hotel + '_childage_"]').last().remove();
                console.log('CHILDAGE REM', $('input[name^="room_' + navigate_hotel + '_childage_"]').length, $('input[name^="room_' + navigate_experience + '_childage_"]'));
              }
            }

            rooms_experience_modal.find('#room_occupancy_' + guest_type).text(guest_type_num);
            /*$form.find('input[name=total_occupation_' + guest_type + ']').val(totalOccupation['hotel_ticket_form'][guest_type]);
             $form2.find('input[name=total_occupation_' + guest_type + ']').val(totalOccupation['hotel_ticket_form'][guest_type]);*/

            updateFrontRooms(rooms_hotel_modal, 'hotel_form', 'hotel_form-attached', navigate_hotel);
          }

        });

        /**
         * Add Guest from Selected Room
         */
        $rooms_hotel_occupancy.find('.plus').click(function (e) {

          /*e.preventDefault();
           e.stopPropagation();

           var guest_type = $(this).data('guest');

           var $form                 = $('#hotel_form');
           var $form2                = $('#hotel_form-attached');
           var $guest_type_max_input = $form.find('input[name="guest_' + guest_type + '"]');
           var $guest_type_num_input = $form.find('input[name="room_' + navigate_hotel + '_' + guest_type + '"]');
           var guest_type_max        = $guest_type_max_input.val();
           var guest_type_num        = $guest_type_num_input.val();

           if (totalOccupation['hotel_form'][guest_type] < guest_type_max) {
           guest_type_num++;
           totalOccupation['hotel_form'][guest_type]++;

           $guest_type_num_input.val(guest_type_num);

           rooms_hotel_modal.find('#room_occupancy_' + guest_type).text(guest_type_num);
           $form.find('input[name=total_occupation_' + guest_type + ']').val(totalOccupation['hotel_form'][guest_type]);
           $form2.find('input[name=total_occupation_' + guest_type + ']').val(totalOccupation['hotel_form'][guest_type]);

           updateFrontRooms(rooms_hotel_modal, 'hotel_form', 'hotel_form-attached', navigate_hotel);
           }*/

          e.preventDefault();
          e.stopPropagation();

          var guest_type = $(this).data('guest');

          var form_id                = 'hotel_form';
          var form_id2               = 'hotel_form-attached';
          var $form                  = $('#' + form_id);
          var $form2                 = $('#' + form_id2);
          var $guest_type_max_input  = $form.find('input[name="guest_' + guest_type + '"]');
          var $guest_type_num_input  = $form.find('input[name="room_' + navigate_hotel + '_' + guest_type + '"]');
          var $guest_type_num_input2 = $form2.find('input[name="room_' + navigate_hotel + '_' + guest_type + '"]');
          var guest_type_max         = $guest_type_max_input.val();
          var guest_type_num         = $guest_type_num_input.val();

          console.log('Occupation', guest_type, totalOccupation['hotel_form'][guest_type], guest_type_num, guest_type_max, navigate_hotel);

          if (totalOccupation[form_id][guest_type] < guest_type_max) {
            guest_type_num++;

            console.log("ADD GUEST IN ROOM", guest_type, totalOccupation[form_id]);

            totalOccupation[form_id][guest_type]++;
            totalOccupation[form_id2][guest_type]++;

            if (guest_type == 'child' && !navigate_hotel_children.hasOwnProperty(navigate_hotel)) {
              navigate_hotel_children[navigate_hotel] = 0;
            }

            $guest_type_num_input.val(guest_type_num);
            $guest_type_num_input2.val(guest_type_num);

            //rooms_hotel_modal.find('#room_occupancy_' + guest_type).text(guest_type_num);

            updateFrontRooms(rooms_hotel_modal, form_id, form_id2, navigate_hotel);
          }
        });

        $('#hotel_ticket_form, #hotel_ticket_form-attached, #hotel_form, #hote_form-attached').submit(function (e) {

          if ($(this).attr('id') == 'hotel_ticket_form' || $(this).attr('id') == 'hotel_ticket_form-attached') {
            if ($(this).find('input[name=date_hotel_experience_checkin]').val() == "") {
              $modalInfo.find('.messageTxt').text(Drupal.t(messages['errorSearchCheckin']));
              $modalInfo.modal("show");
              event.preventDefault();
              return false;
            }
          }
          if ($(this).attr('id') == 'hotel_form' || $(this).attr('id') == 'hotel_form-attached') {
            if ($(this).find('input[name=date_hotel_checkin]').val() == "") {
              $modalInfo.find('.messageTxt').text(Drupal.t(messages['errorSearchCheckin']));
              $modalInfo.modal("show");
              event.preventDefault();
              return false;
            }
          }

          if ($(this).attr('id') == 'hotel_ticket_form' || $(this).attr('id') == 'hotel_ticket_form-attached') {
            if ($(this).find('input[name=date_hotel_experience_checkout]').val() == "") {
              $modalInfo.find('.messageTxt').text(Drupal.t(messages['errorSearchCheckout']));
              $modalInfo.modal("show");
              event.preventDefault();
              return false;
            }
          }
          if ($(this).attr('id') == 'hotel_form' || $(this).attr('id') == 'hotel_form-attached') {
            if ($(this).find('input[name=date_hotel_checkout]').val() == "") {
              $modalInfo.find('.messageTxt').text(Drupal.t(messages['errorSearchCheckout']));
              $modalInfo.modal("show");
              event.preventDefault();
              return false;
            }
          }

          if ($(this).attr('id') == 'hotel_ticket_form' || $(this).attr('id') == 'hotel_ticket_form-attached') {
            if ($(this).find('input[name=date_hotel_experience_checkout]').val() == "") {
              $modalInfo.find('.messageTxt').text(Drupal.t(messages['errorSearchCheckout']));
              $modalInfo.modal("show");
              event.preventDefault();
              return false;
            }
          }
          if ($(this).attr('id') == 'hotel_form' || $(this).attr('id') == 'hotel_form-attached') {
            if ($(this).find('input[name=date_hotel_checkout]').val() == "") {
              $modalInfo.find('.messageTxt').text(Drupal.t(messages['errorSearchCheckout']));
              $modalInfo.modal("show");
              event.preventDefault();
              return false;
            }
          }

          if ($(this).attr('id') == 'hotel_ticket_form' || $(this).attr('id') == 'hotel_ticket_form-attached') {
            if (!rooms_experience_total) {
              $modalInfo.find('.messageTxt').text(Drupal.t(messages['errorSearchNullGuests']));
              $modalInfo.modal("show");
              event.preventDefault();
              return false;
            }
          }
          if ($(this).attr('id') == 'hotel_form' || $(this).attr('id') == 'hotel_form-attached') {
            if (!rooms_hotel_total) {
              $modalInfo.find('.messageTxt').text(Drupal.t(messages['errorSearchNullGuests']));
              $modalInfo.modal("show");
              event.preventDefault();

              return false;
            }
          }

          console.log("NB_ROOMS", $(this).find('input[name="nb_rooms"]').val());

          if (!isNumeric(parseInt($(this).find('input[name="nb_rooms"]').val())) || !parseInt($(this).find('input[name="nb_rooms"]').val())) {
            $modalInfo.find('.messageTxt').text(Drupal.t(messages['errorSearchNullRooms']));
            $modalInfo.modal("show");
            event.preventDefault();
            return false;
          }
        });

        // TICKETS

        $('.single-filter').off('click').on('click', function (e) {

          var indice      = $(this).index();
          var checkbx     = $('#ticket_form .single-filter').eq(indice).find('input[type="checkbox"]');
          var checkbx2    = $('#ticket_form-attached .single-filter').eq(indice).find('input[type="checkbox"]');
          var checkBoxes  = $('#ticket_form').find('input[type="checkbox"]:checked');
          var checkBoxes2 = $('#ticket_form-attached').find('input[type="checkbox"]:checked');

          if (checkBoxes.length == 1 && checkbx.prop("checked")) {
            e.preventDefault();
          }
        });

        // MOBILE SEARCH EXPERIENCE TABS

        $('.experience-search a.scrollto').on('click', function (e) {

          var sniffer = Sniffer.getInstance();

          if (sniffer.IsMobile) {

            var $tab_content = $('#tab-content');

            $tab_content.removeClass('hide-on-mobile');
            $tab_content.removeClass('hide-tab-content');

            var href = $(this).attr('href');
            $('html, body').animate({
              scrollTop: $tab_content.offset().top - 50
            }, 'slow');
            e.preventDefault();
          }
        });

        $(document).on('click', '#close-content-tab', function () {
          $('#tab-content').addClass('hide-tab-content');
        });

        /**
         * Modal OK
         */
        $('.btn-ok').on('click', function (e) {

          e.preventDefault();

          var $curr_modal = $(this).closest('.modal');
          var $next_modal = $($(this).data('next'));

          $curr_modal.once('hidden.bs.modal', function (e) {
            $next_modal.modal('show');
          });

          $curr_modal.modal('hide');
        });
      }

    }
  }

})(jQuery);
;/**/
!function(t,i){"use strict";i.behaviors.slick={attach:function(i,s){var e=this;t(".slick:not(.unslick)",i).once("slick",function(){var i=t(this),n=t("> .slick__slider",i),l=t("> .slick__arrow",i),a=t.extend({},s.slick,n.data("slick"));e.beforeSlick(n,l,a),n.slick(e.globals(n,l,a)),e.afterSlick(n,a)})},beforeSlick:function(s,e,n){var l,a=this;a.randomize(s),s.on("init.slick",function(r,o){var c=o.options.responsive||null;if(c&&c.length>-1)for(l in c)c.hasOwnProperty(l)&&"unslick"!==c[l].settings&&(o.breakpointSettings[c[l].breakpoint]=t.extend({},i.settings.slick,a.globals(s,e,n),c[l].settings))}),s.on("setPosition.slick",function(t,i){a.setPosition(s,e,i)})},afterSlick:function(i,s){var e=this,n=i.slick("getSlick");i.parent().on("click.slick.load",".slick-down",function(i){i.preventDefault();var e=t(this);t("html, body").stop().animate({scrollTop:t(e.data("target")).offset().top-(e.data("offset")||0)},800,s.easing)}),s.mousewheel&&i.on("mousewheel.slick.load",function(t,s){return t.preventDefault(),0>s?i.slick("slickNext"):i.slick("slickPrev")}),i.trigger("afterSlick",[e,n,n.currentSlide])},randomize:function(t){t.parent().hasClass("slick--random")&&!t.hasClass("slick-initiliazed")&&t.children().sort(function(){return.5-Math.random()}).each(function(){t.append(this)})},setPosition:function(i,s,e){if(i.attr("id")===e.$slider.attr("id")){var n=e.options,l=i.parent().parent(".slick-wrapper").length?i.parent().parent(".slick-wrapper"):i.parent(".slick");return t(".slick-slide",l).removeClass("slick-current"),t("[data-slick-index='"+e.currentSlide+"']",l).addClass("slick-current"),n.centerPadding&&"0"!==n.centerPadding||e.$list.css("padding",""),e.slideCount<=n.slidesToShow||n.arrows===!1?s.addClass("element-hidden"):s.removeClass("element-hidden")}},globals:function(s,e,n){return{slide:n.slide,lazyLoad:n.lazyLoad,dotsClass:n.dotsClass,rtl:n.rtl,appendDots:".slick__arrow"===n.appendDots?e:n.appendDots||t(s),prevArrow:t(".slick-prev",e),nextArrow:t(".slick-next",e),appendArrows:e,customPaging:function(t,s){var e=t.$slides.eq(s).find("[data-thumb]")||null,l=i.t(e.attr("alt"))||"",a="<img alt='"+l+"' src='"+e.data("thumb")+"'>",r=e.length&&n.dotsClass.indexOf("thumbnail")>0?"<div class='slick-dots__thumbnail'>"+a+"</div>":"";return r+t.defaults.customPaging(t,s)}}}}}(jQuery,Drupal);;/**/
