(function ($) {
    Drupal.behaviors.dpr_itinerary_flow = {
        attach: function (context, settings) {
            /**
             * Select default the first day
             */
            var numberOfDays = 1;
            selectFirstDay(numberOfDays);
            /**
             * On click day slider
             */
            $('.day-slider-list').find('a').off('click').click(function (e) {
                e.preventDefault();
                e.stopPropagation();
                cancelTrip();
                ($('.day-slider-list').find('a').removeClass('selected') && $(this).addClass('selected'));

                numberOfDays = parseInt($(this).data('type').replace("DAY", ""));
                selectFirstDay(numberOfDays);
                /**
                 * On click park selection
                 */
                for(var i=1;i<numberOfDays;i++){
                    (function(i){
                        // add park selection buttons onclick action except the last one
                        var $park_container = 'park-selection-container-day' + i;
                        var next_day = i + 1;
                        var $next_park_container = 'park-selection-container-day' + next_day;
                        $('#' + $park_container).find('.park-list').off('click').click(function(e){
                            e.preventDefault();
                            e.stopPropagation();

                            $('#' + $park_container).find('.park-list')
                                    .removeClass('motiongate')
                                    .removeClass('bollywood')
                                    .removeClass('legoland')
                                    .removeClass('waterpark')
                                    .removeClass('day1')
                                    .removeClass('day2')
                                    .removeClass('day3')
                                    .removeClass('day4');
                            ($('#' + $park_container).find('.park-list').removeClass('selected') && $(this).addClass('selected'));
                            ($('#' + $park_container).find('.park-list').find('span').removeClass('selected') && $(this).find('span').addClass('selected'));
                            switch($(this).data('type')){
                                case 'motiongate':
                                    $('#' + $park_container).find('.park-list').removeClass('motiongate');
                                    $(this).addClass('motiongate').addClass('day' + i);
                                    break;
                                case 'bollywood':
                                    $('#' + $park_container).find('.park-list').removeClass('bollywood');
                                    $(this).addClass('bollywood').addClass('day' + i);
                                    break;
                                case 'legoland':
                                    $('#' + $park_container).find('.park-list').removeClass('legoland');
                                    $(this).addClass('legoland').addClass('day' + i);
                                    break;
                                case 'waterpark':
                                    $('#' + $park_container).find('.park-list').removeClass('waterpark');
                                    $(this).addClass('waterpark').addClass('day' + i);
                                    break;
                            }

                            $park = $('#' + $next_park_container, document).find('.container');
                            if ($('#' + $next_park_container).hasClass("closed")) {
                                TweenMax.set($park, {height: "auto", opacity: 1});
                                TweenLite.from($park, .5, {
                                    height    : 0,
                                    opacity   : 0,
                                    ease      : Power1.easeInOut,
                                    onComplete: function () {
                                        TweenLite.to($(window), .3, {
                                            scrollTo: {y: $('#' + $next_park_container).offset().top},
                                            ease    : Power4.easeInOut
                                        });
                                    }
                                });
                                $('#' + $next_park_container).removeClass("closed");
                            }
                        });
                        
                        if(i === (numberOfDays-1)){
                            //append "cancel" and "plan my trip" buttons to the last element
                            $('#' + $next_park_container).find('.park-list').off('click').click(function(e){
                                e.preventDefault();
                                e.stopPropagation();

                                $('#' + $next_park_container).find('.park-list')
                                    .removeClass('motiongate')
                                    .removeClass('bollywood')
                                    .removeClass('legoland')
                                    .removeClass('waterpark')
                                    .removeClass('day1')
                                    .removeClass('day2')
                                    .removeClass('day3')
                                    .removeClass('day4');
                                ($('#' + $next_park_container).find('.park-list').removeClass('selected') && $(this).addClass('selected'));
                                ($('#' + $next_park_container).find('.park-list').find('span').removeClass('selected') && $(this).find('span').addClass('selected'));
                                switch($(this).data('type')){
                                    case 'motiongate':
                                        $(this).addClass('motiongate').addClass('day' + next_day);
                                        break;
                                    case 'bollywood':
                                        $(this).addClass('bollywood').addClass('day' + next_day);
                                        break;
                                    case 'legoland':
                                        $(this).addClass('legoland').addClass('day' + next_day);
                                        break;
                                    case 'waterpark':
                                        $(this).addClass('waterpark').addClass('day' + next_day);
                                        break;
                                }

                                if($('.button-group').length) return;
                            
                                var $button_group = $('<div class="button-group">' + 
                                                        '<a href="#" class="action-btn" id="cancel_plan_btn">Cancel</a>' +
                                                        '<a class="action-btn active" id="plan_btn">Plan My Trip</a>' +
                                                        '</div>');
                                $button_group.appendTo($('#' + $next_park_container));

                                // listen click events
                                $('#cancel_plan_btn').on('click', function(e){
                                    e.preventDefault();
                                    e.stopPropagation();
                                    cancelTrip();
                                });

                                $('#plan_btn').on('click', function(e){
                                    e.preventDefault();
                                    e.stopPropagation();
                                    var $trip_plan = planTrip(numberOfDays);
                                    var $days = daysSelected(numberOfDays);
                                    $.ajax({
                                        type: 'POST',
                                        url: 'saveselected',
                                        data: {trip_plan: $trip_plan, days: $days},
                                        dataType: 'json',
                                        success: function(result){
                                            if(result.msg === "success"){
                                                window.location.href = Drupal.settings.planUrl + '/day1';
                                            }
                                        }
                                    });
                                });
                            });
                        }
                    })(i);
                }
            });
            /**
             * Cancel trip
             */
            function cancelTrip(){
                // scroll top of the page, close day planner, and delete button group
                for(var i=1; i<=4; i++){
                    var $park_selection_container = 'park-selection-container-day' + i;
                    if(!$('#' + $park_selection_container).hasClass('closed')){
                        $('#' + $park_selection_container).addClass('closed');
                    }
                    // remove selected and bubble classes
                    $('#' + $park_selection_container).find('.park-list')
                        .removeClass('motiongate')
                        .removeClass('bollywood')
                        .removeClass('legoland')
                        .removeClass('waterpark')
                        .removeClass('day1')
                        .removeClass('day2')
                        .removeClass('day3')
                        .removeClass('day4');
                    
                    $('#' + $park_selection_container).find('.park-list').find('span').removeClass('selected');
                    // unbind click events
                    $('#' + $park_selection_container).find('.park-list').prop('onclick',null).off('click');
                }
                // remove "cancel" and "plan my trip" buttons
                $('.button-group').remove();
                // remove selected day slider and select the first one
                ($('.day-slider-list').find('a').removeClass('selected') && $('.day-slider-list li').first().find('a').addClass('selected'));
                // scroll top
                $(document).scrollTop();
            }

            /**
             * Plan trip
             */
            function planTrip(numberOfDays){
                // get all the parks selected and associate them with the corresponding day
                var $days = {
                    1: 'DAY1',
                    2: 'DAY2',
                    3: 'DAY3',
                    4: 'DAY4'
                };
                var $trip_plan = '{';
                for(var i=1; i<=numberOfDays; i++){
                    var $park_selection_container = 'park-selection-container-day' + i;
                    var $park_selected = $('#' + $park_selection_container).find('.park-list.selected').data('type');
                    $trip_plan = $trip_plan + ('"' + $days[i] + '"' + ':' + '"' + $park_selected + '"' + ',');
                }
                return $trip_plan.replace(/,(?=[^,]*$)/, '') + '}';
            }

            /**
             * Days selected
             */
            function daysSelected(numberOfDays) {
                var $days = ['DAY1', 'DAY2', 'DAY3', 'DAY4'];
                var $days_selected = '[';
                for(var i=0; i<numberOfDays; i++){
                    $days_selected = $days_selected + ('"' + $days[i] +  '"' + ',');
                }
                return $days_selected.replace(/,(?=[^,]*$)/, '') + ']';
            }

            /**
             * Select the first day
             */
            function selectFirstDay(numberOfDays) {
                var $park = $('#park-selection-container-day1', document).find('.container');
                if ($('#park-selection-container-day1').hasClass("closed")) {
                    TweenMax.set($park, {height: "auto", opacity: 1});
                    TweenLite.from($park, .5, {
                        height    : 0,
                        opacity   : 0,
                        ease      : Power1.easeInOut,
                        onComplete: function () {
                            TweenLite.to($(window), .3, {
                                scrollTo: {y: $('#park-selection-container-day1').offset().top},
                                ease    : Power4.easeInOut
                            });

                            $('#park-selection-container-day1').find('.park-list').click(function(e){
                                e.preventDefault();
                                e.stopPropagation();

                                $('#park-selection-container-day1').find('.park-list')
                                    .removeClass('motiongate')
                                    .removeClass('bollywood')
                                    .removeClass('legoland')
                                    .removeClass('waterpark')
                                    .removeClass('day1')
                                    .removeClass('day2')
                                    .removeClass('day3')
                                    .removeClass('day4');
                                ($('#park-selection-container-day1').find('.park-list').removeClass('selected') && $(this).addClass('selected'));
                                ($('#park-selection-container-day1').find('.park-list').find('span').removeClass('selected') && $(this).find('span').addClass('selected'));
                                switch($(this).data('type')){
                                    case 'motiongate':
                                        $('#park-selection-container-day1').find('.park-list').removeClass('motiongate').removeClass('day1');
                                        $(this).addClass('motiongate').addClass('day1');
                                        break;
                                    case 'bollywood':
                                        $('#park-selection-container-day1').find('.park-list').removeClass('bollywood').removeClass('day1');
                                        $(this).addClass('bollywood').addClass('day1');
                                        break;
                                    case 'legoland':
                                        $('#park-selection-container-day1').find('.park-list').removeClass('legoland').removeClass('day1');
                                        $(this).addClass('legoland').addClass('day1');
                                        break;
                                    case 'waterpark':
                                        $('#park-selection-container-day1').find('.park-list').removeClass('waterpark').removeClass('day1');
                                        $(this).addClass('waterpark').addClass('day1');
                                        break;
                                }
                                if(numberOfDays === 1) {
                                    if($('.button-group').length) return;
                            
                                    var $button_group = $('<div class="button-group">' + 
                                                        '<a href="#" class="action-btn" id="cancel_plan_btn">Cancel</a>' +
                                                        '<a class="action-btn active" id="plan_btn">Plan My Trip</a>' +
                                                        '</div>');
                                    $button_group.appendTo($('#park-selection-container-day1'));
                                    $('#cancel_plan_btn').on('click', function(e){
                                        e.preventDefault();
                                        e.stopPropagation();
                                        cancelTrip();
                                    });

                                    $('#plan_btn').on('click', function(e){
                                        e.preventDefault();
                                        e.stopPropagation();
                                        var $trip_plan = planTrip(numberOfDays);
                                        var $days = daysSelected(numberOfDays);
                                        $.ajax({
                                            type: 'POST',
                                            url: 'saveselected',
                                            data: {trip_plan: $trip_plan, days: $days},
                                            dataType: 'json',
                                            success: function(result){
                                                if(result.msg === "success"){
                                                    window.location.href = Drupal.settings.planUrl + '/day1';
                                                }
                                            }
                                        });
                                    });
                                }
                            });
                        }
                    });
                    $('#park-selection-container-day1').removeClass("closed");
                }
            }
        }
    }
})(jQuery);