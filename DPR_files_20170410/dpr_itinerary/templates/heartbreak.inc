<?php if (isset($_SESSION['userData']) && $_SESSION['userData'] !=''): //logged user
    if (isset($node->field_wishlist_id['und'][0]['safe_value'])){
        $detail_wish_id = $node->field_wishlist_id['und'][0]['safe_value'];
    } elseif (isset ($r['wishlist_id']))  {
        $detail_wish_id = $r['wishlist_id'];                
    }
    if (isset( $_SESSION['wishlist_items_var'])) {
        if(!in_array($detail_wish_id, $_SESSION['wishlist_items_var'])) {  
            $class_2 = "icm-whishlist";
            $id_todo = "add_item_to_wish";
        }  else {
            $class_2 = "icm-whishlis_seleted";
            $id_todo = "remove_item_from_wish";
        }
    } else {
        $class_2 = "icm-whishlist";      
    }
?>
<a class="add_item_to_wish"  id="<?= $id_todo ?>" href="#" >
    <div class="heart"><i class="<?= $class_2 ?>"></i></div>
</a>
<?php else: //non logged user ?>
    <a class="toggler love" href="#" data-toggle="modal" data-target="#favModal">
        <i class="icon icm-whishlist"></i>
    </a>
    <!-- <a class="toggler love" href="#" data-toggle="modal" data-target="#favModal">❤♡</a> -->
<?php endif; ?>