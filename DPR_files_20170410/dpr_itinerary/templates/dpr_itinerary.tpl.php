<?php
    $userLogged = (isset($_SESSION['userData']) && $_SESSION['userData'] !='');
    global $language;
    global $base_url;
    $return = dpr_discover_get_itinerary_steps();
    $redirect_url = url(drupal_get_path_alias('plantrip') , array('absolute'=>true) );
    if(!$userLogged){
     $_SESSION['redirectLoginUrlWish'] = $redirect_url;
    }
    
?>
<section class="itinerary-content">
  <h1 class="itinerary-content-header"> <?= t("My trip") ?> </h1>
</section>
<section class="wrapper-page darker">
  <section class="content-page container">
    <div class="row masonry-container">
    <?php $i = 0; ?>
      <?php foreach ($return as $r): ?>
        <div class="masonry-item filterables itinerary-page col-xs-12 col-sm-12 col-md-12">
          <div class="steps-content row">
            <div class="col-sm-12 col-md-12 textual-content <?php $i % 2 == 0 ? print 'text-on-right' : print 'text-on-left'; ?> clearfix">
              <div class="inner">
                <div class="<?php $i % 2 == 0 ? print 'border-left-next' : print 'border-right-next'; ?>">
                  <h2><b><?php print $r['title'];?></b></h2>
                  <h2><?php print $r['subtitle'] ?></h2>
                </div>
                <hr>
                <div class="content">
                    <?php print $r['body_value']; ?>
                </div>
              </div>
            </div>
            <div class="col-sm-5 col-md-5 image-side <?php $i % 2 == 0 ? print 'group-right' : print 'group-left'; ?>">
              <div class="inner">
                <img src="<?= file_create_url( $r['image'] )  ?>">
              </div>
            </div>
          </div>
        </div>
        <?php $i++; ?>
        <?php endforeach; ?>
    </div>
  </section>
  <a href="<?php print $redirect_url.'/selectday'; ?>"><?= t("START NOW") ?></a>
</section>

<script language="javascript">
    (function ($) {
        $(function () {
            $('.full-container').addClass('no-background');
        });
    }(jQuery));
</script>
