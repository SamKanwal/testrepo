<div id="favModal" class="modal fade modal-pink modal-wishlist" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="icm-x"></i></span></button>
      </div>
      <div class="modal-body">
        <div class="restrictor">
          <h1 class="modal-title"><?= t('Save to wishlist') ?></h1>
          <p class="warning"><?= t('You need to be logged in to save items in your wishlist!') ?> </p>
          <a class="action-btn" href="<?=dpr_sso_generateLoginUrl()?>">Log in</a>
        </div>
        <hr>
        <div class="restrictor">
          <p><?= t('Don\'t have an account? Sign up!') ?></p>
          <a class="signup-btn action-btn white" data-dismiss="modal" href="#navbar">Sign up</a>
        </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->