<?php
    Global $user, $language;
    //$block_id = "32";
    //$block = block_block_view($block_id);
    //$block['content'] = i18n_string(array('blocks', 'block', $block_id, 'body'), $block['content']);
    //print render($block['content']);

    $plantrip = url(drupal_get_path_alias('plantrip') , array('absolute'=>true) );

    $_SESSION['plantripurl'] = $plantrip;
    $redirect_url = $plantrip.'/sendmessage';
    $userLogged = (isset($_SESSION['userData']) && $_SESSION['userData'] !='');
    if(!$userLogged){ 
     $_SESSION['redirectLoginUrlWish'] = $redirect_url;
    }

   
    if(isset($_SESSION['itinerary_plan'])){

        $submitData = array();

        $itineraryPlan = $_SESSION['itinerary_plan'];
        foreach($itineraryPlan->days as $day){
            $nodeIds = array();
            $dayData = new stdClass();
            $dayData->poi = array();
            $dayData->park = $day->park;
            $dayData->day = $day->day;
            //$daydecoded = json_decode(preg_replace('/\s+/', '',$day), true);/*
            //$day->submitData = array();
            foreach($day->plannedItems as $item){
                $nodeIds[] = $item;
            }
            
            $nodes = node_load_multiple($nodeIds);

    

            foreach($nodes as $node){

                try {
                    $wrapper = entity_metadata_wrapper('node', $node);
                    
                    $title = $node->title; 
                    $description = $wrapper->field_itinerary_description->value() == null ? t("No Description ") :  $wrapper->field_itinerary_description->value() ;
                    $pageUrl = url(drupal_get_path_alias('node/'.$node->nid) , array('absolute'=>true) );

                    $imageObject = $wrapper->field_image->value();
                    if (!empty($imageObject)) {
                        $imageUrl = file_create_url($imageObject['uri']);
                    }
                     
                    $data = new stdClass();
                    $data->name = $title;
                    $data->description = $description;
                    $data->pageUrl = $pageUrl;
                    $data->imageUrl = $imageUrl;
                    //$data = array('name'=>$title, 'description'=>$description, 'pageUrl'=>$pageUrl, 'imageUrl'=>$imageUrl);

                     $dayData->poi[] = $data;
                    } catch (Exception $e) {
                        watchdog('entity_metadata_wrapper', 'entity_metadata_wrapper error in %error_loc', array('%error_loc' => __FUNCTION__ . ' @ ' . __FILE__ . ' : ' . __LINE__), WATCHDOG_CRITICAL);
                        return;
                    }
            }
            $submitData[] = $dayData;
        }
        $_SESSION['itinerary_submit'] =  $submitData;
    }

?>
<section class="itinerary-content">
        <i class="icon icm-confirm"></i>
        <p class="sendmessage-title"><?= t('Your plan has been created successfully.') ?></p>
        <h2 style="display: <?php $userLogged ? print 'inline-block' : print 'none' ?>"><?= t('You will find your plan in your wishlist.') ?></h2>
        <div class="sendmessage-button-group">
            <a class="share-icon-left" href="#"><i class="icon icm-mail"><span><?= t('send via email') ?></span></i></a>
            <a class="share-icon-right" href="#"  rel="socialmedia"  data-content-url="<?php print $plantrip;?>" data-content-title="" data-content-default="">
                <i class="icon icm-share"><span><?= t('social share') ?></span></i>
            </a>
        </div>
</section>



<div id="sendItineraryModal" class="modal modal-pink fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content text-center">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h2 class="modal-title"><?= t("Send via email") ?></h2>
            </div>
            <div class="modal-body">   
                <div class="row">
                    <div class="col-md-1 col-sm-1 col-xs-1"></div>
                    <div class="col-md-10 col-sm-10 col-xs-10 textarea_wrap">
                        <p class="modal-desc" id="sendItineraryFeedBack"><span class="modal-desc-text pull-left"><?= t("Email") ?></span><span class="error pull-right"></span></p>
                        <input placeholder="<?php print t('Insert Email Address'); ?>" class="regex-control form-control email long-string input_emails"  ></input>   
                    </div>     
                    <div class="col-md-1 col-sm-1 col-xs-1"></div>
                </div>
                <div class="row">
                    <div class="col-md-1 col-sm-1"></div>
                    <div class="col-md-10 col-sm-10 modal-buttons">
                        <div class="col-md-6 col-md-push-6 col-sm-6 col-sm-push-6 cta_wrap">
                            <a class="action-btn send_itinerary_emails" href="#"><?php print t('Send'); ?></a>    
                        </div>  
                        <div class="col-md-6 col-md-pull-6 col-sm-6 col-sm-pull-6 cta_wrap">
                            <a class="action-btn white blank" href="#" data-dismiss="modal"><?php print t('Cancel'); ?></a>    
                        </div>
                    </div>     
                    <div class="col-md-1 col-sm-1"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="sendItineraryModalSuccess" class="modal modal-pink fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content text-center">
      <div class="small-modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><span class="icm-confirm confirm_send_icon"></span></h4>
        <h5 class="modal-subtitle" id="deleteItineraryFeedback"><?php print t('Your email has been sent'); ?></h5>
        </div>
        <div class="modal-body">
            <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4 modal-buttons">
                <div class="cta_wrap">
                    <a class="action-btn" href="#" data-dismiss="modal"><?php print t('Ok'); ?></a>
                </div>             
            </div>
            <div class="col-md-4"></div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="deleteItineraryModalError" class="modal modal-pink fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content text-center">
      <div class="small-modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><span class="icm-delete confirm_send_icon"></span></h4>
        <h5 class="modal-subtitle" id="deleteItineraryFeedback"><?php print t("Oops an error occured, try again later."); ?></h5>
        </div>
        <div class="modal-body">
            <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4 modal-buttons">
                <div class="cta_wrap">
                    <a class="action-btn delete_row_cta" href="#" data-dismiss="modal"><?php print t('Ok'); ?></a>
                </div>             
            </div>
            <div class="col-md-4"></div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="sendmessage-background"></div>

<script language="javascript">
    (function ($) {
        $(function () {
            $('.full-container').addClass('no-background');

            $(".send_itinerary_emails").on("click", function(e){
                e.preventDefault();
                var check = "";
                var mailSender = $(".textarea_wrap input").val();
                var checkMails = true;
                var val_chars = true;
                var validLength = true;
                var invalidMail = "";
                var mailRegex = new RegExp("[^@]+@[^@][^\\s]+([.]{1}[a-zA-Z]{2,}|[.]{1}[a-zA-Z]{2,}[.]{1}[a-zA-Z]{2,})");
                var val_charsRegex = new RegExp("^[a-zA-Z0-9.@_-]*$");
                var mailsender = mailSender.replace(/,+$/, '');
               
                  
                    checkMails = checkMails && mailRegex.test(mailSender);
                    val_chars = val_chars && val_charsRegex.test(mailSender);
                    if (mailSender.length > 49)  {
                        validLength = false;
                        invalidMail = mailSender;
                    }
                
                if(mailSender==""){                
                    $(".error").text(Drupal.t("Please enter recipient's email address"));
                    $(".textarea_wrap textarea").css("border-color", "red");
                }else if(!val_chars){                
                    $(".error").text(Drupal.t("Only letters (a-z), numbers (0-9), full stops (‘.’) and underscores (‘_’) are allowed"));
                    $(".textarea_wrap textarea").css("border-color", "red");
                }else if(!checkMails){                
                    $(".error").text(Drupal.t("Invalid email address"));
                    $(".textarea_wrap textarea").css("border-color", "red");
                }else if (!validLength) {
                    $(".error").text(Drupal.t("Your email is too long"));
                    $("#friend_email").css("border-color", "red");
                }else if(checkMails){
                    jsFunctions.toggleSpinner();
                    var dataItinerarySend = {"email" : mailSender};
                    $.ajax({
                        method: "POST",
                        url: urlSito+"sendItinerary",
                        data: dataItinerarySend,
                        success: function(data) {
                            var res = JSON.parse(data);                    
                            jsFunctions.toggleSpinner();
                            if(res.status ==='error'){
                                $("#deleteItineraryModalError").modal('show');
                                $("#sendItineraryModal").modal('hide');
                                $(".textarea_wrap textarea").val("");
                                $(".error").text("");
                                $("#friend_email").css("border-color", "none");
                            }else {
                                $("#sendItineraryModalSuccess").modal('show');
                                $("#sendItineraryModal").modal('hide');
                                $(".textarea_wrap textarea").val("");
                                $(".error").text("");
                                $("#friend_email").css("border-color", "none");
                            }
                        },
                        error: function(data){                   
                            jsFunctions.toggleSpinner();
                            $("#deleteItineraryModalError").modal('show');
                            $("#sendItineraryModal").modal('hide');
                            $(".textarea_wrap textarea").val("");
                            $(".error").text("");
                            $("#friend_email").css("border-color", "none");
                        }
                    });
                }
            
            }); 

            $(".icm-mail").on("click", function(e){
                e.preventDefault();
                $("#sendItineraryModal").modal('show');
            });
            

        });
        
    }(jQuery));
</script>

<?php module_load_include('php', 'dpr_social_share', '/templates/entry-point.tpl'); ?>