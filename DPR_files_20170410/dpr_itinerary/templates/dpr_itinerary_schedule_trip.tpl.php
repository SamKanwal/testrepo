<?php
global $language;
global $base_url;
global $base_path;
$uri = $base_path.drupal_get_path('module', 'dpr_itinerary');
//save the current park in session so that it can be fetched from mapplic module
/*if(!isset($_SESSION)){
    session_start();
}
*/
/*$park = "legoland";
$plan_url = "#";
$zones = dpr_get_zones($park);
$attractions = dpr_get_attractions($park);
$dining = dpr_get_dining($park);
$events = dpr_get_events($park);*/
$userLoggedIn = (isset($_SESSION['userData']) && $_SESSION['userData'] !='');


$redirect_url = url(drupal_get_path_alias('plantrip') , array('absolute'=>true) );

if(!$userLoggedIn) {
    $_SESSION['redirectLoginUrlWish'] = $redirect_url.'/scheduletrip/'.$day;
}

$trip_plan_array = [];
$trip_plan = preg_replace('/\s+/', '',$_SESSION['trip_plan']);

dpr_sso_retrieveWishlist();
if(isset($trip_plan) && !empty($trip_plan)){
    // retrieve items added under my plan from session
    $tripplan = [];
    if(isset($_SESSION['itinerary_plan'])){
        $itineraryPlan = $_SESSION['itinerary_plan'];
        if($itineraryPlan->days[$day]){
            //retrieve current day data from session
            $tripplan = $itineraryPlan->days[$day]->plannedItems;
        }
    }
    // get days and trip plan from session
    $days = json_decode(preg_replace('/\s+/', '',$_SESSION['days']), true);
    $trip_plan_array = json_decode($trip_plan, true);
    // get the park name
    $park = $trip_plan_array[$day];
    if(!$park){
        header("Location: ".$redirect_url);
        exit();
    }
    // set the current park in session
    $_SESSION['current_park'] = $park;
    $_SESSION['current_day'] = $day;

    // get current park's things-to-do
    $zones = dpr_get_zones($park);
    $attractions = dpr_get_attractions($park);
    $dining = dpr_get_dining($park);
    $events = dpr_get_events($park);

    $plan_url = "";
    $trip_planner_page = $redirect_url.'/selectday';
    $send_message_page = $redirect_url.'/sendmessage';
    $addToItinerary = $redirect_url.'/add';
    $removeFromItinerary = $redirect_url.'/remove';

    
    $plan_url = $redirect_url.'/scheduletrip/'.strtolower(dpr_itinerary_get_next_day($day));
    if(dpr_itinerary_get_next_day($day) =='FINISH'){
        $plan_url =  $send_message_page;
        $nextButtonText = t('FINISH'); 
    }else {
        $nextDay = strtolower(dpr_itinerary_get_next_day($day));
        $plan_url = $redirect_url.'/scheduletrip/'.$nextDay;
        $nextButtonText = t('PLAN DAY').' '. str_replace("day", "", $nextDay); 
    } 
    $today = str_replace("DAY", "DAY ", $day);
    $nextDay = str_replace("DAY", "DAY ", dpr_itinerary_get_next_day($day));
} else {
     header("Location: ".$redirect_url);
    exit();
}
?>
<section class="wrapper-page">
    
    <section class="outer_map_cont">
        <?php
        $block = module_invoke('mapplic', 'block_view', 'mapplic_map');
        print render($block['content']);
        $block = module_invoke('block', 'block_view', '3');
        print render($block['content']);
        ?>
  </section>

    <section class="content-page container schedule">
        <div class="trip-plan-row">
            <h1><?php print $today.' in '.$park ?></h1>
            <div class="row masonry-container row-left">
                <div class="masonry-item filterables col-xs-12 col-sm-12 col-md-12">
                <div class="row-title row text-on-left">
                    <div class="col-xs-12 col-sm-12 col-md-12  textual-content clearfix">
                        <div class="inner">
                            <h2 class="left-side-title"><?= t("MY PLAN") ?></h2>
                            <a class="toggle-place-btn <?php print $park ?>" id="toggle_place_btn" >
                                <div><?= t("Add Place") ?></div>
                                <i class="icon <?php strtolower($language->language) == 'ar' ? print 'icm-arrow-left' : print 'icm-arrow-right' ?>"></i>
                            </a>
                            <hr>
                            <!-- Add things-to-do user selected -->
                            <div class="row masonry-container" id="liked-container">
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
            <div class="row masonry-container row-right">
                <div class="masonry-item filterables col-xs-12 col-sm-12 col-md-12">
                <div class="row-title row text-on-right">
                    <div class="col-xs-12 col-sm-12 col-md-12 textual-content clearfix">
                        <div class="inner">
                            <a class="toggle-plan-btn" id="toggle_plan_btn" >
                                <i class="icon icm-arrow-left"></i>
                                <div><?= t("My Plan") ?></div>   
                            </a>
                            <h2 class="right-side-title"><?= t("ADD PLACE") ?></h2>
                            <hr>

                            <!-- Tabs for things-to-do -->
                            <div class="discover-nav-container row">
                                <a data-type="zones" class="things-to-do">
                                    <div class="block-nav col-xs-3 col-sm-3 zactive">
                                        <i class="icon icm-zones"></i>
                                        <div><?= t("ZONES") ?></div>
                                    </div>
                                </a>
                                <a data-type="attractions" class="things-to-do">
                                    <div class="block-nav col-xs-3 col-sm-3">
                                        <i class="icon icm-attractions"></i>
                                        <div><?= t("ATTRACTIONS") ?></div>
                                    </div>
                                </a>
                                <a data-type="dining" class="things-to-do">
                                    <div class="block-nav col-xs-3 col-sm-3">
                                        <i class="icon icm-dining"></i>
                                        <div><?= t("DINING") ?></div>
                                    </div>
                                </a>
                                <a data-type="events" class="things-to-do">
                                    <div class="block-nav col-xs-3 col-sm-3">
                                        <i class="icon icm-events"></i>
                                        <div><?= t("EVENTS") ?></div>
                                    </div>
                                </a>
                            </div>

                            <!-- There needs to be search bar here -->
                            <div class="inner-addon right-addon">
                                <i class="icon icm-search"></i>
                                <input class="search" id="search-bar" type="text" onclick="focus()" onKeyPress="search()" onKeyUp="search()">
                            </div>
                            
                            <div class="row masonry-container" id="zones-container">
                                <?php $i = 0; ?>
                                    <?php foreach ($zones as $r): ?>
                                        <?php $i++; $detail = $base_url.'/'.$language->language.'/'.drupal_get_path_alias('node/'.$r['nid']); ?>
                                        <div class="masonry-item filterables col-xs-12 col-sm-12 col-md-12 <?php in_array($r['nid'], $tripplan) ? print 'added_to_plan' : print '' ?>" id="id-<?php print $r["nid"] ?>" data-title="<?php print $r["title"]?>">
                                            <div class="result-container" >
                                                <div class="col-xs-12 col-sm-12 col-md-12 result-text <?php print $park ?> clearfix">
                                                     <div class="col-xs-3 col-sm-3 col-md-3 image-side">
                                                        <img src="<?php print $r['image']; ?>" class="<?php strtolower($language->language) == 'ar' ? print 'rtl-border' : print 'ltr-border' ?>">
                                                    </div>
                                                    <div>
                                                        <div class="result-title">
                                                            <h3 onclick = "(function(){ var dtl_url = '<?php print $detail ?>'; if(dtl_url!==''){ window.location.href = dtl_url;} })()" ><?php print $r["title"]; ?></h3></a>
                                                        </div>
                                                        <div class="content">
                                                            <p><?php print $r['itinerary_desc']; ?></p>
                                                        </div>
                                                    </div>
                                                    <a class="action-btn active add" data-item-id="id-<?php print $r["nid"] ?>" data-item-container="zones-container"><?= t("ADD TO MY PLAN") ?></a>
                                                </div>
                                            </div>
                                        </div>                
                                    <?php endforeach; ?>
                            </div>
                            
                           <div class="row masonry-container" style="display:none" id="attractions-container">
                                <?php $i = 0; ?>
                                    <?php foreach ($attractions as $r): ?>
                                        <?php $i++; $detail = $base_url.'/'.$language->language.'/'.drupal_get_path_alias('node/'.$r['nid']); ?>
                                         <div class="masonry-item filterables col-xs-12 col-sm-12 col-md-12 <?php in_array($r['nid'], $tripplan) ? print 'added_to_plan' : print '' ?>" id="id-<?php print $r["nid"] ?>" data-title="<?php print $r["title"]?>">
                                            <div class="result-container" data-wish="<?php print $r['wishlist_id'] ?>">
                                                <div class="col-xs-12 col-sm-12 col-md-12 result-text <?php print $park ?> clearfix">
                                                     <div class="col-xs-3 col-sm-3 col-md-3 image-side">
                                                        <img src="<?php print $r['image']; ?>" class="<?php strtolower($language->language) == 'ar' ? print 'rtl-border' : print 'ltr-border' ?>">
                                                      </div>
                                                    <div>
                                                        <div class="result-title">
                                                            <h3 onclick = "(function(){ var dtl_url = '<?php print $detail ?>'; if(dtl_url!==''){ window.location.href = dtl_url}  })()" ><?php print $r["title"]; ?></h3>
                                                            <h2><b><?php print $r["attraction_type"]; ?></b></h2>
                                                            <?php include 'heartbreak.inc' ?>
                                                        </div>
                                                        <div class="content">
                                                            <p><?php print $r['itinerary_desc']; ?></p>
                                                        </div>
                                                    </div>
                                                    <a class="action-btn active add" data-item-id="id-<?php print $r["nid"] ?>" data-item-container="attractions-container"><?= t("ADD TO MY PLAN") ?></a>
                                                </div>
                                            </div>
                                        </div>           
                                    <?php endforeach; ?>
                            </div>

                            <div class="row masonry-container" style="display:none" id="dining-container">
                                <?php $i = 0; ?>
                                    <?php foreach ($dining as $r): ?>
                                        <?php $i++; $detail = $base_url.'/'.$language->language.'/'.drupal_get_path_alias('node/'.$r['nid']); ?>
                                         <div class="masonry-item filterables col-xs-12 col-sm-12 col-md-12 <?php in_array($r['nid'], $tripplan) ? print 'added_to_plan' : print '' ?>" id="id-<?php print $r["nid"] ?>" data-title="<?php print $r["title"]?>">
                                            <div class="result-container" data-wish="<?php print $r['wishlist_id'] ?>">
                                                <div class="col-xs-12 col-sm-12 col-md-12 result-text <?php print $park ?> clearfix">
                                                     <div class="col-xs-3 col-sm-3 col-md-3 image-side">
                                                        <img src="<?php print $r['image']; ?>" class="<?php strtolower($language->language) == 'ar' ? print 'rtl-border' : print 'ltr-border' ?>">
                                                    </div>
                                                    <div>
                                                        <div class="result-title">
                                                            <h3 onclick = "(function(){ var dtl_url = '<?php print $detail ?>'; if(dtl_url!==''){ window.location.href = dtl_url}  })()" ><?php print $r["title"]; ?></h3>
                                                            <?php include 'heartbreak.inc' ?>
                                                        </div>
                                                        <div class="content">
                                                            <p><?php print $r['itinerary_desc']; ?></p>
                                                        </div>
                                                    </div>
                                                    <a class="action-btn active add" data-item-id="id-<?php print $r["nid"] ?>" data-item-container="dining-container"><?= t("ADD TO MY PLAN") ?></a>
                                                </div>
                                            </div>
                                        </div>           
                                    <?php endforeach; ?>
                            </div>

                            <div class="row masonry-container" style="display:none" id="events-container">
                                <?php $i = 0; ?>
                                    <?php foreach ($events as $r): ?>
                                        <?php $i++; $detail = $base_url.'/'.$language->language.'/'.drupal_get_path_alias('node/'.$r['nid']); ?>
                                         <div class="masonry-item filterables col-xs-12 col-sm-12 col-md-12 <?php in_array($r['nid'], $tripplan) ? print 'added_to_plan' : print '' ?>" id="id-<?php print $r["nid"] ?>" data-title="<?php print $r["title"]?>">
                                            <div class="result-container" data-wish="<?php print $r['wishlist_id'] ?>">
                                                <div class="col-xs-12 col-sm-12 col-md-12 result-text <?php print $park ?> clearfix">
                                                    <div class="col-xs-3 col-sm-3 col-md-3 image-side">
                                                        <img src="<?php print $r['image']; ?>" class="<?php strtolower($language->language) == 'ar' ? print 'rtl-border' : print 'ltr-border' ?>">
                                                    </div>
                                                    <div>
                                                        <div class="result-title">
                                                            <h3 onclick = "(function(){ var dtl_url = '<?php print $detail ?>'; if(dtl_url!==''){ window.location.href = dtl_url}  })()" ><?php print $r["title"]; ?></h3></a>
                                                            <?php include 'heartbreak.inc' ?>
                                                        </div>
                                                        <div class="content">
                                                            <p><?php print $r['itinerary_desc']; ?></p>
                                                        </div>
                                                    </div>
                                                    <a class="action-btn active add" data-item-id="id-<?php print $r["nid"] ?>" data-item-container="events-container"><?= t("ADD TO MY PLAN") ?></a>
                                                </div>
                                            </div>
                                        </div>           
                                    <?php endforeach; ?>
                            </div>
                            <div class="loader" style="opacity: 0">
                                <img src="<?php echo $uri; ?>/assets/loading.svg" alt="<?php print t('Now Loading'); ?>">
                                <p><?php print t('loading...'); ?></p>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </section>
    <?php require(drupal_get_path('module', 'dpr_itinerary') . '/templates/modal_favorite.inc'); ?>
</section>

<div id="sticky-anchor"></div>
<div class="button-group stick">
    <a href="<?php print $trip_planner_page ?>" class="action-btn cancel" id="cancel_plan_btn"><?= t("CANCEL") ?></a>
    <a href="<?php print $plan_url ?>" class="action-btn active nextday" id="plan_next_day_btn"><?php print $nextButtonText ?></a>
</div>

<script language="javascript">
    (function ($) {
        $(function () {
            $('.full-container').addClass('no-background');
            
            $(document).on("mapplic.teo.loaded", function(){
                $('.mapplic-pin').hide();
                $('#liked-container').find('.action-btn.active').each(function(){
                    pinItemOnMap($(this));
                });
            });

            $(document).ready(function() {
                $(window).scroll(sticky_relocate);
                sticky_relocate();
            });   

            $('#finish_plan_btn').on('click', function(e){
                e.preventDefault();
                e.stopPropagation();
                window.location.href = "<?php print $send_message_page ?>"; 
               
            });

            $(".added_to_plan").each(function() {
                // add items that are added to plan under my plan section of the ui
                move_item_to_myplan($(this).find('.action-btn.active'));
            });

            $('#plan_next_day_btn').on('click', function(e){
                e.preventDefault();
                e.stopPropagation();
                window.location.href = "<?php print $plan_url ?>";
                
            });

            addEffects();
            if($('.added_to_plan').length === 0 && $('#liked-container').children().length === 0){
                emptyResult("show", "noplan", $('#liked-container'), "No Plans Added");
            }
            $('.things-to-do').on('click', function(e){
                hide_things_to_do();
                emptyResult("remove", "noresult", $("#zones-container"));
                $('#search-bar').val("");
                $('.inner-addon').find('.icon').removeClass('icm-x').addClass('icm-search');
                showThingToDo();
                switch($(this).data('type')){
                    case "zones":
                        var $zones_length = '<?php echo sizeof($zones) ?>';
                        $('.things-to-do').find('.block-nav').removeClass('zactive') && $(this).find('.block-nav').addClass('zactive');
                        if(parseInt($zones_length) <= 0){
                            emptyResult("show", "noresult", $("#zones-container"), "No zones available");
                        } else {
                            $('.filterables').find('.result-container').removeClass('animated');
                            $(this).find('.filterables').removeClass('hidden');
                            document.getElementById('zones-container').style.display = 'block';
                            fadeIn();
                        }
                        break;
                    case "attractions":
                        var $attractions_length = '<?php echo sizeof($attractions) ?>';
                        $('.things-to-do').find('.block-nav').removeClass('zactive') && $(this).find('.block-nav').addClass('zactive');
                        if(parseInt($attractions_length) <= 0){
                            emptyResult("show", "noresult", $("#attractions-container"), "No attractions available");
                        } else {
                            $('.filterables').find('.result-container').removeClass('animated');
                            document.getElementById('attractions-container').style.display = 'block';
                            $(this).find('.filterables').removeClass('hidden');
                            fadeIn();
                        } 
                        break;
                    case "dining":
                        var $dining_length = '<?php echo sizeof($dining) ?>';
                        $('.things-to-do').find('.block-nav').removeClass('zactive') && $(this).find('.block-nav').addClass('zactive');
                        if(parseInt($dining_length) <= 0){
                            emptyResult("show", "noresult", $("#dining-container"), "No dining available");
                        } else {
                            $('.filterables').find('.result-container').removeClass('animated');
                            document.getElementById('dining-container').style.display = 'block';
                            $(this).find('.filterables').removeClass('hidden');
                            fadeIn();
                        }
                        break;
                    case "events":
                        var $events_length = '<?php echo sizeof($events) ?>';
                        $('.things-to-do').find('.block-nav').removeClass('zactive') && $(this).find('.block-nav').addClass('zactive');
                        if(parseInt($events_length) <= 0){
                            emptyResult("show", "noresult", $("#events-container"), "No events available");
                        } else {
                            $('.filterables').find('.result-container').removeClass('animated');
                            document.getElementById('events-container').style.display = 'block';
                            $(this).find('.filterables').removeClass('hidden');
                            fadeIn();
                        }
                        break;
                }
            });

            $('.action-btn.active').on('click', function(e){
                var wish;
                var self = $(this);
                if(self.hasClass('add')){
                    // add item to wishlist if user logged is
                    var wishlistID = self.closest('div[class^="result-container"]').data('wish');
                    addItemWishList(wishlistID);
                    // add item to plan
                    addItemToPlan(self.data('item-id').replace('id-',''));
                    // move item under my plan in ui
                    move_item_to_myplan(self);
                    // remove nothing added under my plan
                    emptyResult("remove", "noplan");
                } else if(self.hasClass('remove')) {
                    // remove item from plan
                    removeItemFromPlan(self.data('item-id').replace('id-',''));
                    // remove item from my plan in ui
                    remove_item_from_myplan(self);

                    //remove item from wishlist
                    wish = self.closest('.result-container').data('wish');
                    if(wish){
                        removeItemWishList(wish);
                    }

                    // show nothing added under my plan if all plans are removed from
                    if($('#liked-container').children().length === 0){
                        emptyResult("show", "noplan", $('#liked-container'), "No Plans Added");
                    }
                }
            });

            $('#toggle_place_btn').on('click', function(e){
                e.preventDefault();
                e.stopPropagation();

                $('.row-right').show('slide', {direction: 'right'}, 500);
                $('.row-left').hide();

                sticky_relocate();
            });

            $('#toggle_plan_btn').on('click', function(e){
                e.preventDefault();
                e.stopPropagation();

                $('.row-right').hide('slide', {direction: 'right'}, 500, function() { $('.row-left').show(); sticky_relocate();});
            });
            // wish list call
            $(".result-container").each(function(){
                var wishlistID = $(this).attr("data-wish");
                var self = $(this);
                self.find(".add_item_to_wish").click({scope:self, wishlistID: wishlistID}, addRemoveItemToWishList);  
                //self.find("#remove_item_from_wish").click({scope:self, wishlistID: wishlistID}, removeItemFromWishList);  
            });
        });
    }(jQuery));

    /**
     * search functionality
     */
    search = function(){
        (function ($) {
            $(function () {
                var $title = $('#search-bar').val().replace(/\s/g,'').toLowerCase();
                $title !== "" ? addCleanSearchBar() : removeCleanSearchbar();
                var $selected_place = $('.things-to-do').find('.block-nav.zactive').parent().data('type');
                emptyResult("remove", "noresult", $("#" + $selected_place + "-container"));
                $("#" + $selected_place + "-container").each(function(){
                    var $search_content = $(this).find('.filterables');
                    
                    $search_content.each(function(){
                        if($(this).hasClass('liked')) return;

                        var data = $(this).data();
                        var isHidden = false;
                        // filter title
                        if(data.title === "") isHidden = true;

                        if (!!data.title && $title!="na") {
                            if ($title!=="") {
                                if (data.title.replace(/\s/g,'').toLowerCase().indexOf($title)===-1) {  
                                    isHidden = true; 
                                }else{
                                    isHidden = false;
                                }
                            }
                        }

                        isHidden ?  ($(this).removeClass("hidden").addClass("hidden"))
                                :  ($(this).removeClass("hidden"));
                    });
                    if($(this).find('.filterables:visible').length === 0){
                        emptyResult("show", "noresult", $("#" + $selected_place + "-container"), "No Results");
                    }
                });
            });
        }(jQuery));
    };

    addEffects = function(){
        (function ($) {
            // handle scrolling
            $.fn.scrollEnd = function(callback, timeout) {          
                $(this).scroll(function(){
                    var $this = $(this);
                    if ($this.data('scrollTimeout')) {
                    clearTimeout($this.data('scrollTimeout'));
                    }
                    $this.data('scrollTimeout', setTimeout(callback,timeout));
                });
            };

            $(window).scrollEnd(function(){
                 $('.loader').css('opacity', '0');
            }, 500);

            $(function () {
                var $things_to_do;
                var CurrentScroll = 0;
                $(window).on('scroll.seffect', function () {
                    var NextScroll = $(this).scrollTop();
                    if (NextScroll > CurrentScroll){
                        // scroll down
                        // fade in components
                        fadeIn();
                        // show loading
                        var selected_place = $('.things-to-do').find('.block-nav.zactive').parent().data('type');
                        var not_animated = $("#" + selected_place + "-container").find('.filterables:not(.liked)').find('.result-container').not('.animated');
                        if(not_animated.length !== 0){
                            // when everything loaded, do not show loading svg
                            $('.loader').css('opacity', '1');
                        }
                    }
                    else {
                        // scroll up
                        $('.loader').css('opacity', '0');
                    }
                    CurrentScroll = NextScroll;  //Updates current scroll position
                }).trigger('seffect');

                $things_to_do = $('.result-container');
                $things_to_do.on('mouseenter', function () {
                    TweenMax.to($(this), 1, {boxShadow: '0 10px 10px #ccc', scale:1.05, zIndex:10, transformOrigin:"center 100%", ease: Expo.easeOut});
                }).on('mouseleave', function () {
                    TweenMax.to($(this), 1, {boxShadow: 'none', scale:1, zIndex:0, ease: Expo.easeOut});
                });
            });
        }(jQuery));
    };

    /**
     * hide all things-to-dos 
     */
    hide_things_to_do = function(){
        var $things_to_do_list = ["zones", "attractions", "dining", "events"];
        for(var i=0; i<$things_to_do_list.length; i++){
            document.getElementById($things_to_do_list[i] + '-container').style.display = 'none';
        }
    };

    fadeIn = function(){
        (function($){
            $(function(){
                var $things_to_do = $('.filterables:not(.liked)').find('.result-container:in-viewport').not('.animated');
                $things_to_do.addClass('animated');
                TweenMax.staggerFrom($things_to_do, 2, {opacity:0, y: 100, delay: .1, ease: Expo.easeOut}, .25);
            })
        }(jQuery));
    };

    emptyResult = function(s, $filter, $location, $text){
        (function ($) {
            $(function () {
                switch(s){
                    case "show":
                    $location.after(  
                        $("<div>").addClass("noresult_filter " + $filter + " text-center")
                                .css({"padding-bottom":"50px"})
                                .append(  $("<span>").addClass("icm-negative_feedback")
                                                        .css({"font-size":"5em",
                                                            "color": "#C80065"}),
                                            "</br>" + Drupal.t($text)
                                        )
                    );
                    break;
                    case "remove":
                        $(".noresult_filter." + $filter).remove();
                        break;
                        default:
                        break;
                }
            });
        }(jQuery));
    };

    move_item_to_myplan = function(scope){
        (function ($) {
            $(function () {
                var $item_id = scope.data('item-id');
                // remove selected things-to-do from ADD PLACE div and add to MY PLAN div
                pinItemOnMap(scope);
                // change button name as remove from my plan
                scope.text("REMOVE FROM MY PLAN");
                // add class name to differentiate then the rest when determining click
                scope.removeClass("add").addClass("remove");
                // add liked class to disable search under my plan container
                $('#' + $item_id).addClass('liked');
                // prepend to under my plan
                $('#' + $item_id).prependTo('#liked-container');
                // change default background color of park to dark-bg
                $('#' + $item_id).find('.result-text').removeClass("<?php print $park ?>").addClass('under-myplan');
            });
        }(jQuery));
    };

    remove_item_from_myplan = function(scope) {
        (function ($) {
            $(function () {
                var $item_id = scope.data('item-id');
                // remove selected things-to-do from MY PLAN div and add to ADD PLACE div
                removeItemPinOnMap(scope);
                var $container_to_append = scope.data('item-container');
                // change button name as add to my plan
                scope.text("ADD TO MY PLAN");
                // remove class name that was appended when adding things-to-do under my plan
                scope.removeClass("remove").addClass("add");
                // remove class to enable search under add place container
                $('#' + $item_id).removeClass('liked');
                if($('#' + $item_id).hasClass('added_to_plan')){
                    $('#' + $item_id).removeClass('added_to_plan');
                }
                // prepend to under add place
                $('#' + $item_id).prependTo('#' + $container_to_append);
                // change dark-bg background color to default background color of park 
                $('#' + $item_id).find('.result-text').removeClass('under-myplan').addClass("<?php print $park ?>");
            });
        }(jQuery));
    };

    addItemWishList = function(wishlistID){
        (function ($) {
            $(function () {
                if('<?php print $userLoggedIn ?>' !== '') {
                    // Returns successful data submission message when the entered information is stored in database.
                    var dataWish = {"key" : wishlistID};
                    var self = $("[data-wish='"+wishlistID+"']");
                    var heart_icon = self.find('.icm-whishlist');
                    // AJAX Code To Submit Form.
                    $.ajax({
                        method: "POST",
                        url: urlSito+"addWishlistItem",
                        data: dataWish,
                        success: function(data) {
                            heart_icon.removeClass('icm-whishlist').addClass('icm-whishlis_seleted');
                        },
                        error: function(error){
                            console.error(error);
                            //$('#genericErrorApiFeedback').modal('show'); 
                        }
                    });
                }
            });
        }(jQuery)); 
    };

    removeItemWishList = function(wishlistID){
        (function ($) {
            $(function () {
                if('<?php print $userLoggedIn ?>' !== '') {
                    // Returns successful data submission message when the entered information is stored in database.
                    var dataWish = {"key" : wishlistID};
                    var self = $("[data-wish='"+wishlistID+"']");
                    var heart_icon_selected = self.find('.icm-whishlis_seleted');
                    // AJAX Code To Submit Form.
                    $.ajax({
                        method: "POST",
                        url: urlSito+"removeWishlistItem",
                        data: dataWish,
                        success: function(data) {
                            // wishlist change id as add_item_to_wish
                            heart_icon_selected.removeClass('icm-whishlis_seleted').addClass('icm-whishlist');
                        },
                        error: function(){
                            //$('#genericErrorApiFeedback').modal('show');
                        }
                    });
                }
            });
        }(jQuery));
    };

    addItemToPlan = function($nid){
        (function ($) {
            $(function () {
                jsFunctions.toggleSpinner();
                
                // AJAX Code To Submit Form.
                var $day = '<?= $_SESSION['current_day'] ?>';
                var $park = '<?= $_SESSION['current_park'] ?>';
                $.ajax({
                    type: 'POST',
                    url:  "<?php print $addToItinerary ?>",
                    data: {day: $day, park: $park, nid: $nid},
                    dataType: 'json',
                    success: function(result){
                        if(result.msg === "success"){
                            //window.location.href = "<?php print $send_message_page ?>";
                        }
                        jsFunctions.toggleSpinner();
                    },
                    error: function (result){
                        jsFunctions.toggleSpinner();
                    }
                });
            });
        }(jQuery)); 
    };

    removeItemFromPlan = function($nid){
        (function ($) {
            $(function () {
                    // Returns successful data submission message when the entered information is stored in database.
                    // AJAX Code To Submit Form.
                    var $day = '<?= $_SESSION['current_day'] ?>';
                    jsFunctions.toggleSpinner();

                $.ajax({
                    type: 'POST',
                    url:  "<?php print $removeFromItinerary ?>",
                    data: {day: $day, nid: $nid},
                    dataType: 'json',
                    success: function(result){
                        if(result.msg === "success"){
                            //window.location.href = "<?php print $send_message_page ?>";
                        }
                        jsFunctions.toggleSpinner();
                    },
                    error: function (result){
                        jsFunctions.toggleSpinner();
                    }
                });
            });
        }(jQuery));
    };


    sticky_relocate = function() {
        (function ($) {
            $(function () {
                var window_top = $(window).scrollTop() + $(window).height(),
                    div_top = $('#sticky-anchor').offset().top;
                
                if (window_top > div_top) {
                    if($('.button-group').hasClass('stick')) {
                        $('.button-group').removeClass('stick');
                        $('#sticky-anchor').append($('.button-group'));
                    }
                    $('#sticky-anchor').height($('.button-group').outerHeight());
                } else if ($(window).scrollTop() === 0) {
                    // scroll at top, hide
                    $('.stick').css("display", "none");
                } else {
                    if(!$('.stick').is(':visible')){
                        // if not visible show
                        $('.stick').show();
                    }
                    if(!$('.button-group').hasClass('stick')) {
                        $('.button-group').addClass('stick');
                        $('.block-system').append($('.button-group'));
                    }
                    $('#sticky-anchor').height(0);
                }
            });
        }(jQuery)); 
    };

     addRemoveItemToWishList = function(e){
        (function ($) {
            $(function () {
                e.preventDefault();
                var self = e.data.scope;
                var heart_icon = self.find('.icm-whishlist');
                var heart_icon_selected = self.find('.icm-whishlis_seleted');
                var wishlistID = e.data.wishlistID;
                // Returns successful data submission message when the entered information is stored in database.
                var dataWish = {"key" : wishlistID};
                if(self.find('.icm-whishlist').length > 0){
                    $.ajax({
                        method: "POST",
                        url: urlSito+"addWishlistItem",
                        data: dataWish,
                        success: function(data) {
                            // add select class to heart icon
                            heart_icon.removeClass('icm-whishlist').addClass('icm-whishlis_seleted');
                        },
                        error: function(){
                            //$('#genericErrorApiFeedback').modal('show'); 
                        }
                    });
                } else {
                    $.ajax({
                        method: "POST",
                        url: urlSito+"removeWishlistItem",
                        data: dataWish,
                        success: function(data) {
                            // wishlist change id as add_item_to_wish
                            heart_icon_selected.removeClass('icm-whishlis_seleted').addClass('icm-whishlist');
                        },
                        error: function(){
                            //$('#genericErrorApiFeedback').modal('show'); 
                        }
                });
                }
                // AJAX Code To Submit Form.
  
            });
        }(jQuery));
    };

    addCleanSearchBar = function(){
        if(jQuery('.inner-addon').find('.icon').hasClass('icm-search')) {
            // when there is input typed in, remove search icon and add cross icon instead
            jQuery('.inner-addon').find('.icon').removeClass('icm-search').addClass('icm-x');
            jQuery('.inner-addon').on('click', function(){
                // add cross icon an action to clean input typed in
                jQuery('#search-bar').val('');
                jQuery(this).off();
                removeCleanSearchbar();
            });
        }
    };

    removeCleanSearchbar = function() {
        if(jQuery('.inner-addon').find('.icon').hasClass('icm-x')) {
            // if everything is deleted by user on search bar, show search icon instead of x
            jQuery('.inner-addon').find('.icon').removeClass('icm-x').addClass('icm-search');
            emptyResult("remove", "noresult", jQuery("#zones-container"));
            showThingToDo();
        }
    };

    showThingToDo = function() {
        // make visible hidden items on screen
        var selected_place = jQuery('.things-to-do').find('.block-nav.zactive').parent().data('type');
        jQuery("#" + selected_place + "-container").find('.hidden').each(function(){
            jQuery(this).removeClass("hidden");
        });
    };

    pinItemOnMap = function(s) {
        var $item_id = s.data('item-id');
        var mappRefId = 'map_ref' + $item_id.replace('id-','');
        jQuery('.mapplic-pin').each(function ()
        {
            
            if(jQuery(this).data('location') === mappRefId){
                jQuery(this).removeClass("hidden-pin");
                jQuery(this).show();
            }
        });
    };

    removeItemPinOnMap = function(s) {
        var $item_id = s.data('item-id');
        var mappRefId = 'map_ref' + $item_id.replace('id-','');
        jQuery('.mapplic-pin').each(function ()
        {
            if(jQuery(this).data('location') === mappRefId){
                jQuery(this).addClass("hidden-pin");
                jQuery(this).hide();
            }
        });
    };

</script>