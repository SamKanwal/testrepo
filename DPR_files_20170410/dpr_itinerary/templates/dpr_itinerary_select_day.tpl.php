<?php
        
    if(!isset($_SESSION)){
        session_start();
    }
    $redirect_url = url(drupal_get_path_alias('plantrip') , array('absolute'=>true) );
    $redirect_url = $redirect_url.'/selectday';
    $userLogged = (isset($_SESSION['userData']) && $_SESSION['userData'] !='');
    if(!$userLogged){ 
     $_SESSION['redirectLoginUrlWish'] = $redirect_url;
    }
?>
<div class="day-select-header">
    <h1><?= t("How long will you stay?") ?></h1>

</div>

<div class="day-slider-header">
    <?php
    // include day slider
    module_load_include('php', 'dpr_itinerary', '/templates/dpr_day_slider.tpl');
    //include park selection
    module_load_include('php', 'dpr_itinerary', '/templates/dpr_park_selection.tpl');
    ?>
</div>

<script language="javascript">
    (function ($) {
        $(function () {
            /**
             * Handle query string
             */
            if(getUrlParameter('parks')){
                var $parksArrayFromUrl = getUrlParameter('parks').split(' ');
                var $validData = true;
                for(var i = 0; i < $parksArrayFromUrl.length; i++){
                    var found = false;
                    $('.park-list').each(function(){
                        if($(this).data('type') === $parksArrayFromUrl[i]){
                            found = true;
                         }
                    });
                    $validData = $validData && found;
                }
                if($validData){
                    $('.park-list').each(function(){
                        if(jQuery.inArray( $(this).data('type'), $parksArrayFromUrl) === -1){
                            $(this).hide();
                        }
                    });
                }
                
                var $expandScreen;
                var $button_group = $('<div class="button-group">' + 
                                        '<a href="#" class="action-btn" id="cancel_plan_btn">Cancel</a>' +
                                        '<a class="action-btn active" id="plan_btn">Plan My Trip</a>' +
                                        '</div>');
                $button_group.appendTo($('#park-selection-container-' + $expandScreen));



                $('#cancel_plan_btn').on('click', function(e){
                    e.preventDefault();
                    e.stopPropagation();
                    cancelTrip();
                });

                $('#plan_btn').on('click', function(e){
                    e.preventDefault();
                    e.stopPropagation();
                    var $trip_plan = planTrip(numberOfDays);
                    var $days = daysSelected(numberOfDays);
                    $.ajax({
                        type: 'POST',
                        url: 'saveselected',
                        data: {trip_plan: $trip_plan, days: $days},
                        dataType: 'json',
                        success: function(result){
                            if(result.msg === "success"){
                                window.location.href = Drupal.settings.planUrl + '/day1';
                            }
                        }
                    });
                });
            }
        }); 
    }(jQuery));

    /**
     * Get query string
     */
    getUrlParameter = function(name) {
        name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
        var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
        var results = regex.exec(location.search);
        return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
    };

    /**
     * Select the park in the url query 
     */
    selectPark = function(day, parkname){
        (function ($) {
            $(function () {
                $('#park-selection-container-' + day).find('.park-list').each(function(){
                    if(parkname === $(this).data('type')) {
                        $(this).addClass("selected " + parkname);
                    }
                })
            }); 
        }(jQuery));
    };

    /**
     * Cancel trip
     */
    cancelTrip = function(){
        (function ($) {
            $(function () {
                // scroll top of the page, close day planner, and delete button group
                for(var i=1; i<=4; i++){
                    var $park_selection_container = 'park-selection-container-day' + i;
                    if(!$('#' + $park_selection_container).hasClass('closed')){
                        $('#' + $park_selection_container).addClass('closed');
                    }
                    // remove selected and bubble classes
                    $('#' + $park_selection_container).find('.park-list')
                        .removeClass('motiongate')
                        .removeClass('bollywood')
                        .removeClass('legoland')
                        .removeClass('waterpark')
                        .removeClass('selected');
                    
                    $('#' + $park_selection_container).find('.park-list').find('span').removeClass('selected');
                    // unbind click events
                    $('#' + $park_selection_container).find('.park-list').prop('onclick',null).off('click');
                }
                // remove "cancel" and "plan my trip" buttons
                $('.button-group').remove();
                // remove selected day slider and select the first one
                ($('.day-slider-list').find('a').removeClass('selected') && $('.day-slider-list li').first().find('a').addClass('selected'));
                // scroll top
                $(document).scrollTop();
            }); 
        }(jQuery));
    };

    /**
     * Plan trip
     */
    planTrip = function(numberOfDays){
        // get all the parks selected and associate them with the corresponding day
        var $days = {
            1: 'DAY1',
            2: 'DAY2',
            3: 'DAY3',
            4: 'DAY4'
        };
        var $trip_plan = '{';
        for(var i=1; i<=numberOfDays; i++){
            var $park_selection_container = 'park-selection-container-day' + i;
            var $park_selected = $('#' + $park_selection_container).find('.park-list.selected').data('type');
            $trip_plan = $trip_plan + ('"' + $days[i] + '"' + ':' + '"' + $park_selected + '"' + ',');
        }
        return $trip_plan.replace(/,(?=[^,]*$)/, '') + '}';
    };

    /**
     * Days selected
     */
    daysSelected = function(numberOfDays) {
        var $days = ['DAY1', 'DAY2', 'DAY3', 'DAY4'];
        var $days_selected = '[';
        for(var i=0; i<numberOfDays; i++){
            $days_selected = $days_selected + ('"' + $days[i] +  '"' + ',');
        }
        return $days_selected.replace(/,(?=[^,]*$)/, '') + ']';
    };
</script>