    <div id="park-selection-container-day1" class="closed">
        <div class="container">
            <h2><?= t("Choose a park for each day") ?></h2>
            <div id="park-selection-day1"><?= t("DAY 1") ?></div>
             <div class="park-list-container">
             <div class="park-list" data-type="motiongate"><span class="icm-motiongate"></span><span class="park-name"><?= t("Motiongate") ?></span></div>
             <div class="park-list" data-type="bollywood"><span class="icm-bollywood"></span><span  class="park-name"><?= t("Bollywood") ?></span></div>
             <div class="park-list" data-type="legoland"><span  class="icm-legoland_kingdom"></span><span class="park-name"><?= t("Legoland") ?></span></div>
             <div class="park-list" data-type="waterpark"><span  class="icm-legoland_waterpark"></span><span class="park-name"><?= t("Water Park") ?></span></div>
            </div>
        </div>
    </div>
    <div id="park-selection-container-day2" class="closed">
        <div class="container">
            <div id="park-selection-day2"><?= t("DAY 2") ?></div>
            <div class="park-list-container">
              <div class="park-list" data-type="motiongate"><span class="icm-motiongate"></span><span class="park-name"><?= t("Motiongate") ?></span></div>
             <div class="park-list" data-type="bollywood"><span class="icm-bollywood"></span><span  class="park-name"><?= t("Bollywood") ?></span></div>
             <div class="park-list" data-type="legoland"><span  class="icm-legoland_kingdom"></span><span class="park-name"><?= t("Legoland") ?></span></div>
             <div class="park-list" data-type="waterpark"><span  class="icm-legoland_waterpark"></span><span class="park-name"><?= t("Water Park") ?></span></div>
            </div>
        </div>
    </div>
    <div id="park-selection-container-day3" class="closed">
        <div class="container">
            <div id="park-selection-day3"><?= t("DAY 3") ?></div>
            <div class="park-list-container">
             <div class="park-list" data-type="motiongate"><span class="icm-motiongate"></span><span class="park-name"><?= t("Motiongate") ?></span></div>
             <div class="park-list" data-type="bollywood"><span class="icm-bollywood"></span><span  class="park-name"><?= t("Bollywood") ?></span></div>
             <div class="park-list" data-type="legoland"><span  class="icm-legoland_kingdom"></span><span class="park-name"><?= t("Legoland") ?></span></div>
             <div class="park-list" data-type="waterpark"><span  class="icm-legoland_waterpark"></span><span class="park-name"><?= t("Water Park") ?></span></div>
            </div>
        </div>
    </div>
    <div id="park-selection-container-day4" class="closed">
        <div class="container">
            <div id="park-selection-day4"><?= t("DAY 4") ?></div>
            <div class="park-list-container">
             <div class="park-list" data-type="motiongate"><span class="icm-motiongate"></span><span class="park-name"><?= t("Motiongate") ?></span></div>
             <div class="park-list" data-type="bollywood"><span class="icm-bollywood"></span><span  class="park-name"><?= t("Bollywood") ?></span></div>
             <div class="park-list" data-type="legoland"><span  class="icm-legoland_kingdom"></span><span class="park-name"><?= t("Legoland") ?></span></div>
             <div class="park-list" data-type="waterpark"><span  class="icm-legoland_waterpark"></span><span class="park-name"><?= t("Water Park") ?></span></div>
            </div>
        </div>
    </div>