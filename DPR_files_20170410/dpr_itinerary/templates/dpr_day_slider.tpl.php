<div class="day-slider-frame">
    <div class="day-slider container">
    <ul class="day-slider-list">
      <li>
        <a class="selected" href="#" data-type="1DAY" <?php if($duration=="1DAY"){ ?>class="selected"<?php } ?>>
          <div>
            <div class="day-slider-txt">
              <div><?= t("1<br>Day") ?></div>
            </div>
          </div>
        </a>
      </li>
      <li>
        <a href="#" data-type="2DAY" <?php if($duration=="2DAY"){ ?>class="selected"<?php } ?>>
          <div>
            <div class="day-slider-txt">
              <div><?= t("2<br>Day") ?></div>
            </div>
          </div>
        </a>
      </li>
      <li>
        <a href="#" data-type="3DAY" <?php if($duration=="3DAY"){ ?>class="selected"<?php } ?>>
          <div>
            <div class="day-slider-txt">
              <div><?= t("3<br>Day") ?></div>
            </div>
          </div>
        </a>
      </li>
      <li>
        <a href="#" data-type="4DAY" <?php if($duration=="4DAY"){ ?>class="selected"<?php } ?>>
          <div>
            <div class="day-slider-txt">
              <div><?= t("4<br>Day") ?></div>
            </div>
          </div>
        </a>
      </li>
    </ul>

    </div>
</div>